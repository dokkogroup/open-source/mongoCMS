<?php
#MU::VERSION::2
if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']=='localhost') ini_set('display_errors', true);

if(!empty($_GET['file'])){
	$file = '../mongoCMS/'.preg_replace('/(\.)+\//','/', $_GET['file']);
	$file = preg_replace("/\/(\/)+/","/", $file);
	$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
	switch ($ext) {
		case 'js': header('Content-Type: application/javascript; charset=utf-8'); break;
		case 'css': header('Content-Type: text/css; charset=utf-8'); break;
		case 'jpg':
		case 'jpeg': header('Content-Type: image/jpeg'); break;
		case 'png': header('Content-Type: image/png'); break;
		case 'gif': header('Content-Type: image/gif'); break;
		case 'bmp': header('Content-Type: image/x-ms-bmp'); break;
		default: break;
	}
	echo file_get_contents($file);
	exit;
}

// REMEMBER: YOU SHOULD NOT SET ANY VARIABLE OVER HERE, TO AVOID ADDING VARS
// TO THE INCLUDED FILES CONTEXT!

if(empty($_GET['controller'])) { echo "fcontroller"; exit; }

if($_GET['controller']=='allow.html') { echo "1"; exit; }

if($_GET['controller']=='robots.txt') {
	header("Content-Type:text/plain; charset=utf8");
	echo "User-agent: *    # everyone\n";
	echo "Disallow: /      # avoid index of everything";
	exit;
}

if(!file_exists('../mongoCMS/controllers/'.basename($_GET['controller']))){
	echo 'missing '.'../mongoCMS/controllers/'.basename($_GET['controller']).' file!';
	exit;
}
include '../mongoCMS/controllers/'.basename($_GET['controller']);