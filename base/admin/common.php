<?php
#MU::VERSION::1
if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']=='localhost') ini_set('display_errors', true);

date_default_timezone_set('America/Argentina/Buenos_Aires');
define('APP_NAME','YOUR-APP-NAME');
define('SESSION_VAR',APP_NAME);
// define('ENABLE_EVENT_LOGS', true);
// define('FORCE_DOKKO_LOGIN',false);
// define('DOKKO_LOGIN_APP_NAME',APP_NAME);
// define('ALLOW_PASSWORD_RECOVER',true);
// define('ALLOW_SWITCH_USER',true);
// define('ALLOW_USER_REGISTRATION',true);
// define('ON_USER_LOGGED_IN','onUserLoggedIn');
// define('ON_USER_REGISTER','onUserRegister');
// define('ON_USER_REGISTERED','onUserRegistered');
// define('ON_USER_ROLE_DISPLAY','onUserRoleDisplay');
// define('MY_PROFILE_METADATA','mongoCMS-metadata/myProfile.json');
// define('USER_REGISTRATION_PROFILE','regular');
// define('REGISTER_TYC_URL','https://www.mysite.com/terms-and-conditions');
// define('GMAPS_KEY','XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
// define('RECAPTCHA_KEY','XXXXXXXXXXXXXXXXX-XXXXXXXXXXXXXXXXXXXXXX');
// define('RECAPTCHA_SECRET','XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
// define('RECAPTCHA_SECRET','XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
// define('EMAIL_VALIDATION_URL','https://api.email-validator.net/api/verify?EmailAddress=%s&APIKey=ev-YOUR-API-KEY');
// define('FORCE_EMAIL_VALIDATION',false);
// define('LOGIN_LOGO','img/logo-login.png');
// define('PANEL_LOGO','img/logo-panel.png');
// define('MOBILE_LOGO','img/logo-mobile.png');
// define('HIDE_COPYRIGHT',true);
// define('ENABLE_FAVICON',true);
// const ROLES_THAT_CAN_FORCE_PASSWORD_RECOVERY = [ 'admin' ];
// const ROLES_THAT_CAN_SWITCH_USER = [ 'admin', 'superadmin' ];
// const MULTILANG_CONTENT = ['en'=>'English','es'=>'Spanish','pt'=>'Portuguese'];
// const SESSION_DURATION = [ 'default' => 3600*8, 'admin' => 900 ];

require_once '../libs/Smarty.class.php';
require_once '../denko/dk.denko.php';
require_once '../MONGODB.php';
require_once '../mongoCMS/mongoCMS/MongoCMS.php';
require_once '../mongoCMS/controllers/common.php';

MongoCMS::setDatabase($mongoDB);
// L10N::setLang('es','main','../mongoCMS/L10N');
MongoCMS::loadMetadata('../mongoCMS-metadata.json');

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
