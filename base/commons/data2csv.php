<?php
#MU::AUTO_UPDATE_ZONE::BEGIN
#MU::VERSION::1

/*
* Data Example:
* [
* { "key1": "value", "kye2": "value", "key3": "value" },
* { "key1": "value", "kye2": "value", "key3": "value" },
* { "key1": "value", "kye2": "value", "key3": "value" }
* ]
*
* The csv output: (keys will be used for first row):
* 1. key1;key2;key3
* 2. value;value;value
* 3. value;value;value
* 4. value;value;value
*/

function dataToCsv ($data, $csvFilePath = false, $boolOutputFile = false) {
    // See if the string contains something
    if (empty($data)) die("Data is empty!");

    // If passed a string, turn it into an array
    if (is_array($data) === false) {
        $data = json_decode($data, true);
    }
    // If a path is included, open that file for handling. Otherwise, use a temp file (for echoing CSV string)
    if ($csvFilePath !== false) {
        $f = fopen($csvFilePath,'w+');
        if ($f === false) {
            die("Couldn't create the file to store the CSV, or the path is invalid. Make sure you're including the full path, INCLUDING the name of the output file (e.g. '../save/path/csvOutput.csv')");
        }
        $boolEchoCsv = false;
    } else {
        $boolEchoCsv = !$boolOutputFile;
        $strTempFile = 'templates_c/csvOutput' . date("U") . ".csv";
        $f = fopen($strTempFile,"w+");
    }

    $firstLineKeys = false;
    foreach ($data as $line) {
        if (empty($firstLineKeys)) {
            $firstLineKeys = array_keys($line);
            fputcsv($f, $firstLineKeys, ";");
            $firstLineKeys = array_flip($firstLineKeys);
        }
        // Using array_merge is important to maintain the order of keys acording to the first element
        fputcsv($f, array_merge($firstLineKeys, $line), ";");
    }
    fclose($f);

    // Take the file and put it to a string/file for output (if no save path was included in function arguments)
    if ($boolOutputFile === true) {
        if ($csvFilePath !== false) $file = $csvFilePath;
        else $file = $strTempFile;

        // Output the file to the browser (for open/save)
        if (file_exists($file)) {
            header('Content-Type: text/csv');
            //header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Length: ' . filesize($file));
            readfile($file);
        }
    } elseif ($boolEchoCsv === true) {
        if (($handle = fopen($strTempFile, "r")) !== FALSE) {
            while (($data = fgetcsv($handle)) !== FALSE) {
                echo implode(",",$data);
                echo "<br />";
            }
            fclose($handle);
        }
    }
    // Delete the temp file
    if(!empty($strTempFile)) unlink($strTempFile);
}

#MU::AUTO_UPDATE_ZONE::END