<?php

function isMd5($md5){
	return preg_match('/^[a-z0-9]{32}$/ui', $md5);
}

function dokkoLogin($appName=null){
	if($appName==null) $appName=dklGuessName();
	if(empty($_GET['k'])) exit;
	if(!isMd5($_GET['k'])) exit;
	$url='https://login.dokkogroup.com.ar/getLogin?k='.$_GET['k'].'&a='.$appName;
	$login=file_get_contents($url);
	if($login=='') exit;
	return json_decode($login,true);
}

function dklGuessName(){
	$appName = Denko::getHost();
	if($appName=='localhost'){
		return APP_DEVELOPMENT_NAME;
	}
	$appName = str_replace(array('.dojo','.dokkogroup.com.ar'),'',$appName);
	$appName = str_replace('.dokko6.com.ar','-dokko6',$appName);
	return $appName;
}

function dklLoginRedirect($mode,$appName=null){
	if($appName==null) $appName=dklGuessName();
	echo "<script type='text/javascript'>window.location='https://login.dokkogroup.com.ar/login?a=$appName&$mode';</script>";
	exit;
}
