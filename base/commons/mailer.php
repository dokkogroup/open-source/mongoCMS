<?php

function sendEMail($to, $subject, $message){
	require_once __DIR__.'/../mongoCMS/controllers/common.php';
	require_once __DIR__.'/PHPMailerAutoload.php';

	$SMTP_HOST = MongoCMS\getConfig('SMTP_HOST');
	$SMTP_USER = MongoCMS\getConfig('SMTP_USER');
	$SMTP_PORT = MongoCMS\getConfig('SMTP_PORT');
	$SMTP_PASS = MongoCMS\getConfig('SMTP_PASS');
	$SMTP_FROM = MongoCMS\getConfig('SMTP_FROM');
	$SMTP_FROMNAME = MongoCMS\getConfig('SMTP_FROMNAME');

	if(empty($SMTP_HOST) || empty($SMTP_FROM) || empty($SMTP_FROMNAME)){
		throw new Exception(_t('Error trying to send the email: %s',_t('SMTP is not configured')), 1);
	}

	$configs=array();
	$configs['TO']=$to;
	//$configs['CC']=$cc;
	$configs['message']=$message;

	$mail = new PHPMailer();

	$mail->isSMTP();                              // Set mailer to use SMTP
	// $mail->SMTPDebug = 4;
	$mail->CharSet = 'UTF-8';
	$mail->Host = $SMTP_HOST;
	$mail->Port = $SMTP_PORT;
	$mail->SMTPAuth = true;                       // Enable SMTP authentication
	$mail->Username = $SMTP_USER;                 // SMTP username
	$mail->Password = $SMTP_PASS;                 // SMTP password
	$mail->SMTPSecure = 'tls';                    // Enable encryption, 'ssl' also accepted

	$mail->From = $SMTP_FROM;
	$mail->FromName = $SMTP_FROMNAME;
	$mail->addAddress($to);
	/*
	if(!empty($cc)){
		$mail->addReplyTo($cc);
		$mail->addCC($cc);
	}
	*/

	$mail->isHTML(true);                             // Set email format to non-HTML

	$mail->Subject = $subject;
	$mail->Body    = $message;

	if(!$mail->send()) {
	    throw new Exception(_t('Error trying to send the email: %s', $mail->ErrorInfo), 1);
	}
	return true;
}
