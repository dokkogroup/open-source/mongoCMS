<?php
/* Mega Updater for MongoDB - mongoUpdateFunctions
 * Versión 0.1
 * Propiedad de DokkoGrpup
 *
 * En este archivo se encuentran las funciones
 * de actualización específicas de cada proyecto.
 *
 * By. FBricker
 */
// TIPOS DE CONFIGURACION:
// 1 - Numeric
// 2 - Single-line Text
// 3 - Multiline Text
// 4 - Hidden
// 5 - Password

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Funciones de actualizacion

function updateV1(){
	global $mongoDB;
	report('Installing megaUpdater',R_ACTION);
	createConfiguration('ip_access_filter', 2, '',true);
	$mongoDB->configurations->createIndex( [ 'name' => 1 ], [ 'unique' => 1 ] );
	return true;
}

// function updateV2(){
// 	global $mongoDB;
// 	createConfiguration('ip_access_filter', 2, '', true);
// 	createConfiguration('musesVersion', 2, '2.1 (html5)', true);
// 	createConfiguration('releaseDate', 2, '16-sep-2016 21:05, GMT-3', true);
// 	createConfiguration('musesVersion-flash', 2, '1.2', true);
// 	createConfiguration('releaseDate-flash', 2, '2014-11-23 21:32 GMT-3', true);
// 	createConfiguration('changeLog', 3, '', true);
// 	createConfiguration('SMTP_HOST', 2, '', true);
// 	createConfiguration('SMTP_PORT', 1, '587', true);
// 	createConfiguration('SMTP_USER', 2, '', true);
// 	createConfiguration('SMTP_PASS', 5, '', true);
// 	createConfiguration('SMTP_FROM', 2, '', true);
// 	createConfiguration('SMTP_FROMNAME', 2, '', true);

// 	$mongoDB->stations->createIndex( [ 'categories' => 1 ] );
// 	$mongoDB->stations->createIndex( [ 'checked.status' => 1 ] );
// 	$mongoDB->stations->createIndex( [ 'checked.timestamp' => 1 ] );
// 	$mongoDB->stations->createIndex( [ 'name' => 'text', 'description' => 'text' ] );
// 	$mongoDB->users->createIndex( [ 'email' => 1 ], [ 'unique' => 1 ] );
// 	$mongoDB->mCMSEventLogs->createIndex( [ 'type' => 'text', 'description' => 'text' ] );
// 	$mongoDB->legacyStations->createIndex( [ 'url' => 1 ], [ 'unique' => 1 ] );
// 	$mongoDB->legacyStations->createIndex( [ 'station' => 1 ] );
// 	$mongoDB->legacyStations->createIndex( [ 'reportsCount' => 1 ] );
// 	$mongoDB->categories->createIndex( [ 'aliases' => 1 ] );
// 	$mongoDB->categories->createIndex( [ 'enabled' => 1 ] );

// 	$mongoDB->appUsers->createIndex(
// 		[ 'email' => 1 ],
// 		[ 'unique' => 1,
// 		  'partialFilterExpression' => [ 'email' => [ '$type' => 'string' ]
// 		]
// 	);
// }