<?php
/* Mega Updater for MongoDB
 * Version 0.1
 * Propiedad de DokkoGrpup
 *
 * By. FBricker
 */
#MU::VERSION::4

require_once 'mongoUpdateFunctions.php';
require_once '../MONGODB.php';

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Constantes

define("MU_VERSION","0.1");
define("MU_NAME","Dokko megaUpdater");

define("R_INFO",1);
define("R_ERROR",2);
define("R_OK",3);
define("R_ACTION",4);
define("R_ECHO",5);
define("R_IMPORTANT",6);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Funciones globales

function formatOutput($text,$html,$color){
	if(!$html){
		return $text;
	}
	return '<font color="'.$color.'"><b>'.$text.'</b></font>';
}

function reportSplit(){
	if(isset($_SERVER['HTTP_HOST'])){
		report("<hr>");
	}else{
		report("------------------------------------------------------------");
	}
}

function report($text,$tipo=R_ECHO){
	if(isset($_SERVER['HTTP_HOST'])){
		$eol=" <br>\n";
		$html=true;
	}else{
		$eol="\n";
		$html=false;
	}
	switch ($tipo) {
		case R_INFO:
			$sol=formatOutput('Info: ',$html,'black');
			break;
		case R_ERROR:
			$sol=formatOutput('Error: ',$html,'red');
			break;
		case R_OK:
			$sol=formatOutput('Ok: ',$html,'green');
			break;
		case R_ACTION:
			$sol=formatOutput(' -> ',$html,'black');
			break;
		case R_IMPORTANT:
			$sol='';
			$text=formatOutput($text,$html,'black').' *';
			break;
		default:
			$sol='';
			break;
	}
	echo $sol.$text.$eol;
}


function getDbVersion(){
	global $mongoDB;
	$version = $mongoDB->mongoMegaUpdaterVersion->findOne();
	if(empty($version)) {
		$mongoDB->mongoMegaUpdaterVersion->insertOne(['version'=>0]);
		return 0;
	};
	return $version['version'];
}

function setDbVersion($version){
	global $mongoDB;
	$mongoDB->mongoMegaUpdaterVersion->updateOne([],['$set'=>['version'=>$version]]);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function initField($cName, $fieldName, $defaultValue){
	global $mongoDB;
	report("Initializing field '$fieldName' on collection $cName");
	$mongoDB->$cName->updateMany(
		[ $fieldName => [ '$exists' => false ] ],
		[ '$set' => [$fieldName => $defaultValue ] ]
	);
	return true;
}

// TIPOS DE CONFIGURACION:
// 1 - Numeric
// 2 - Single-line Text
// 3 - Multiline Text
// 4 - Hidden
// 5 - Password
function createConfiguration($name, $type, $defaultValue, $private){
	global $mongoDB;
	$current = $mongoDB->configurations->findOne(['name'=>$name]);
	if(!empty($current)){
		report("Updating configuration ".$name);
		$mongoDB->configurations->updateOne($current,['$set' => [ 'type' => $type, 'private'=>$private ]]);
		return true;
	}
	report("Creating configuration ".$name);
	$mongoDB->configurations->insertOne(['name'=>$name,'type'=>$type,'value'=>$defaultValue,'private'=>$private]);
	return true;
}

function deleteConfiguration($name){
	global $mongoDB;
	report("Deleting configuration ".$name);
	$mongoDB->configurations->deleteOne(['name'=>$name]);
	return true;
}

////////////////////////////////////////////////////////////////////////////////

function getFileVersion($file){
	$baseFolder = __DIR__.'/../';
	$fullName = $baseFolder.$file;
	if(!file_exists($fullName)) $fullName = $baseFolder.str_replace('admin/', 'web/', $file);
	if(!file_exists($fullName)){
		report("Missing file: ".$file." on project!",R_IMPORTANT);
		return -1;
	}
	$data = file_get_contents($fullName);
	$aux = explode('#MU::VERSION::', $data,2);
	if(count($aux)!=2) return 'N/A';
	$aux = explode("\n",$aux[1],2);
	$version = trim($aux[0]);
	if(is_numeric($version) && (int)$version == $version) return (int)$version;
	report("Invalid version number on file: $file",R_IMPORTANT);
	return -1;
}

////////////////////////////////////////////////////////////////////////////////

function updateFile($file){
	$baseFolder = __DIR__.'/../';
	$projectFile = $baseFolder.$file;
	if(!file_exists($projectFile)) $projectFile = $baseFolder.str_replace('admin/', 'web/', $file);

	$projectData = file_get_contents($projectFile);
	$mongoCMSData = file_get_contents($baseFolder.'mongoCMS/base/'.$file);

	if($file=='megaUpdater/mongoUpdate.php'){
		$newData = $mongoCMSData;
	}else{
		$projectParts1 = explode('#MU::AUTO_UPDATE_ZONE::BEGIN', $projectData,2);
		if(count($projectParts1)!=2) return false;
		$projectParts2 = explode('#MU::AUTO_UPDATE_ZONE::END', $projectParts1[1],2);
		if(count($projectParts2)!=2) return false;

		$mongoCMSParts1 = explode('#MU::AUTO_UPDATE_ZONE::BEGIN', $mongoCMSData,2);
		if(count($mongoCMSParts1)!=2) return false;
		$mongoCMSParts2 = explode('#MU::AUTO_UPDATE_ZONE::END', $mongoCMSParts1[1],2);
		if(count($mongoCMSParts2)!=2) return false;
		$newData = $projectParts1[0];
		$newData.='#MU::AUTO_UPDATE_ZONE::BEGIN';
		$newData.= $mongoCMSParts2[0];
		$newData.='#MU::AUTO_UPDATE_ZONE::END';
		$newData.= $projectParts2[1];
	}

	if(file_put_contents($projectFile, $newData)!=strlen($newData)){
		report('Automatic Update: Fail :(',R_IMPORTANT);
		return false;
	}
	report('Automatic Update: Success :)',R_IMPORTANT);
	return true;
}

////////////////////////////////////////////////////////////////////////////////

function checkMongoCMSBaseFiles(){
	$files = [
		'megaUpdater/mongoUpdate.php',
		'pull',
		'MONGODB.php',
		'commons/data2csv.php',
		'admin/mongocms-base.php',
		'admin/common.php',
		'admin/.htaccess'
	];
	$hasErrors = false;
	foreach($files as $file){
		$projectVersion = getFileVersion($file);
		$mongoCMSVersion = getFileVersion('mongoCMS/base/'.$file);
		if($projectVersion == -1 || $mongoCMSVersion == -1) {
			$hasErrors = true;
			continue;
		}
		if($projectVersion != $mongoCMSVersion) {
			report("Different versions on file: ".$file." (Project: $projectVersion - MongoCMS: $mongoCMSVersion)",R_IMPORTANT);
			if($projectVersion>$mongoCMSVersion){
				report('Project version is newer than MongoCMS version... strange :/', R_IMPORTANT);
				continue;
			}
			if(!updateFile($file)) {
				report('Automatic Update: Not available for this file.',R_IMPORTANT);
				$hasErrors = true;
			}
		}
	}
	if($hasErrors) exit;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Ejecucion de actualizacion

checkMongoCMSBaseFiles();

reportSplit();
report('Initiating '.MU_NAME.' v'.MU_VERSION,R_IMPORTANT);

if(isset($argv[1]) && $argv[1]=='--execute-version') {
	report("Will execute version ".$argv[2]."!",R_IMPORTANT);
	reportSplit();
	$version = $argv[2];
	$metodo='updateV'.$version;
	if(!function_exists($metodo)){
		report("Version not found!",R_IMPORTANT);
		exit(1);
	}
	if($metodo()){
		report("Upgrade to version ".$version." finished successfully",R_OK);
		exit(0);
	}
	reportSplit();
	report("We found some errors while executing version ".$version,R_ERROR);
	reportSplit();
	exit(1);
}

$version=getDbVersion();
report("Current database version: ".$version,R_IMPORTANT);

if($version=='') $version=0;
$version++;
$metodo='updateV'.$version;
if(!function_exists($metodo)){
	report("There are no pending updates",R_IMPORTANT);
	reportSplit();
	exit(0);
}

if(isset($argv[1]) && $argv[1]=='--confirm') {
	report("We have pending database updates!",R_IMPORTANT);
	reportSplit();
	do{
		echo "Do you want to run megaUpdater updates now? (y/n): ";
		$res = trim(fgets(STDIN));
	} while($res !='y' && $res!='n');
	if($res=='n') exit(0);
}

while(function_exists($metodo)){
	reportSplit();
	report("Running version upgrade ".$version,R_IMPORTANT);
	// transactions will be available on MongoDB 4.0
	// $mongoDB->startTransaction();
	$muTransactionEnabled=true;
	if($metodo()){
		report("Upgrade to version ".$version." finished successfully",R_OK);
		setDbVersion($version);
		// if($muTransactionEnabled) $mongoDB->commitTransaction();

	}else{
		reportSplit();
		report("We found some errors while upgrading to version ".$version,R_ERROR);
		reportSplit();
		// if($muTransactionEnabled) $mongoDB->abortTransaction();
		exit(1);
	}
	$version++;
	$metodo='updateV'.$version;
}
exit(0);
