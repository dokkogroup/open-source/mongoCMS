<?php
#MU::AUTO_UPDATE_ZONE::BEGIN
#MU::VERSION::1
require_once __DIR__.'/mongoCMS/MongoDB/functions.php';

///////////////////////////////////////////////////////////////////////////////

function __autoload($class){
	$class = str_replace('\\','/',$class);
	if(substr($class,0,8)=='MongoDB/'){
		$file = __DIR__.'/mongoCMS/'.$class.'.php';
		if(!file_exists($file)) return;
		require_once __DIR__.'/mongoCMS/'.$class.'.php';
	}
}
spl_autoload_register('__autoload');

if(!class_exists('MongoDB\BSON\ObjectID')){
	echo "Error: MongoDB driver not found!";
	if(class_exists('MongoClient')){
		echo "\n<br/>\nYou have Mongo driver (the old one... you need MongoDB driver instead)";
	}
	exit;
}

///////////////////////////////////////////////////////////////////////////////

function MongoID($id){
	if($id instanceof MongoDB\BSON\ObjectID) return $id;
	return new MongoDB\BSON\ObjectID($id);
}

///////////////////////////////////////////////////////////////////////////////

global $mongoDB;
#MU::AUTO_UPDATE_ZONE::END

if(file_exists(__DIR__.'/MONGODB.ini.local')){
	$mongoDB = parse_ini_file(__DIR__.'/MONGODB.ini.local');
}else{
	$mongoDB = parse_ini_file(__DIR__.'/MONGODB.ini');
}

$mongoDB = (new MongoDB\Client($mongoDB['uri']))->selectDatabase($mongoDB['dbName']);