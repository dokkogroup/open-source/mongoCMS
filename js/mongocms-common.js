function changeOrder(id,collection,pos,name){
	bootbox.prompt({
		title: "<b>Entre new position for \""+name+"\":</b><br/>This action cannot be undone!",
		value: pos,
		callback: function(result) {
			if (result == null || result == pos || result == '') return;
			window.location='order?cn='+collection+'&id='+id+'&set='+result;
		}
	});
}

function modalPopup(url, name, height, width){
	if(height==null) height="400px";
	if(width==null) width="100%";
	bootbox.dialog({
		title: name,
		message: '<iframe style="border:0;" src="'+url+'" height="'+height+'" width="'+width+'"></iframe>'
	});
	return false;
}

function parseGPS(coordinates){
    var c = coordinates.split(",");
    var lat = c[0].trim();
    var lon = c[1].trim();
    return {lat:parseFloat(lat),lon:parseFloat(lon)};
}

function printOnIFrame(a){
	var iframe = document.createElement('iframe');
	iframe.style.display = "none";
	iframe.src=a;
	document.body.appendChild(iframe);
	return false;
}