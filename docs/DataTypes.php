<?php
require_once '../../denko/dk.denko.php';

?><html>
<head>
	<title>DataTypes - Docs :: MongoCMS</title>
	<link rel="stylesheet" type="text/css" href="../css/menu.css">
</head>
<body style="padding: 10px;">
	<hr/>
	<h1>DataTypes - Docs :: MongoCMS</h1>
	<hr/>
	<div style="position:absolute;width:100px;height: 30px;top:20px;right:15px; text-align: right;">
		<a href="index.php">back to home</a>
	</div>
	<br/>
<?php

$data = json_decode(file_get_contents('DataTypes.json'),true);

if(empty($_GET['type'])){
	foreach( $data['types'] as $tName => $tData ) {
		echo '<a href="DataTypes.php?type='.$tName.'">'.$tName.'</a><br/>';
		echo $tData['description']."<br/><br/>\n";
	}
} else {
	echo $data['types'][$_GET['type']]['description'].'<br/><br/>';
	echo '<b>Available attributes:</b><br/>';
	if(!isset($data['types'][$_GET['type']]['attributes'])) $data['types'][$_GET['type']]['attributes'] = [];
	$attributes = array_merge($data['types'][$_GET['type']]['attributes'],$data['attributes']);
	$data['types'][$_GET['type']]['attributes'] = $attributes;
	unset($data['types'][$_GET['type']]['description']);
	Denko::print_r($data['types'][$_GET['type']]['attributes'],10,[],'mongoCMS_dump');
}
?>
</body>
</html>