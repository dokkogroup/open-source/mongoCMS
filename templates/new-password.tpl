{include file="header.tpl" title="Set new password"|_t section="login"}

<div class="container">
	<form method="post" onSubmit="return validateNewPass();">
		<div class="text-center">
			<img src="img{$cachePrefix}/logo-panel.png" />
			<h2>{'Set new password'|_t}</h2>
			<br/>
			<span style="font-size:18px; color:#666;"><b>{$user.name}</b><br/>{$user.email}</span>
			<br/><br/>
		</div>
		<label for="inputPassword">{'New password'|_t}</label>
		<input type="password" id="inputPassword1" class="form-control" placeholder="{'Password'|_t}" name="password1" minlength="6" required>
		<br/>
		<label for="inputPassword">{'Confirm your password'|_t}</label>
		<input type="password" id="inputPassword2" class="form-control" placeholder="{'Password'|_t}" name="password2" minlength="6" required>
		<br/>
		{if !empty($error)}<p class="text-danger bg-danger text-center"><b>{'Error'|_t}:</b> {$error}</p>{/if}
		<button class="btn btn-lg btn-primary btn-block" type="submit">{'Set new password'|_t}</button>
	</form>
</div> <!-- /container -->

<script>
	function validateNewPass(){
		if($('#inputPassword1').val() != $('#inputPassword2').val()){
			bootbox.alert('<strong>'+{'Error'|_jst}+':</strong> '+{'Passwords don\'t match'|_jst});
			return false;
		}
		return true;
	}
</script>
{include file="footer.tpl"}