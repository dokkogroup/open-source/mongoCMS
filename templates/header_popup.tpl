<html>
	<head>
		<title>{if $title!=''}{$title} :: {/if}{$smarty.const.APP_NAME}</title>
		<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Bootstrap core CSS -->
		<link href="css{$cachePrefix}/bootstrap.min.css" rel="stylesheet" />
		<link href="css{$cachePrefix}/dataTables.bootstrap.min.css" rel="stylesheet" />
		<link href="css{$cachePrefix}/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
		<link href="css{$cachePrefix}/bootstrap-select.min.css" rel="stylesheet">
		<link href="css{$cachePrefix}/menu.css" rel="stylesheet">
		<link href="css{$cachePrefix}/mongocms-customizations.css" rel="stylesheet">
		<script src="js{$cachePrefix}/jquery-3.5.1.min.js"></script>
		<script src="js{$cachePrefix}/fileinput.min.js"></script>
		<script src="js{$cachePrefix}/fileinput_locale_es.js"></script>
		<script src="js{$cachePrefix}/bootbox.min.js"></script>
		<script src="js{$cachePrefix}/jquery.dataTables.min.js"></script>
		<script src="js{$cachePrefix}/jquery.form.min.js"></script>
		<script src="js{$cachePrefix}/jquery.blockUI.js"></script>
		<script src="js{$cachePrefix}/dataTables.bootstrap.min.js"></script>
		<script src="js{$cachePrefix}/bootstrap-select.min.js"></script>
		<script src="js{$cachePrefix}/mongocms-common.js"></script>
		<script src="js{$cachePrefix}/mongocms-customizations.js"></script>
	</head>
	<body style="padding:0">