{$richFields = ""}
{$fileFields = "[null"}
{$onDisplay = $m.navigation.onFieldDisplay|default:null}
{foreach $viewFields as $fieldName=>$field }
	{$fieldName=$fieldPrefix|cat:$fieldName}
	{if empty($field)}{continue}{/if}
	{if $field.onDisplay|default:$onDisplay != null}
		{if !call_user_func_array($field.onDisplay|default:$onDisplay,
			[$fieldName,$field,$element,$cName,$m,'view'])}
			{continue}
		{/if}
	{/if}
	{$rField = MongoCMS::getFieldInfoByPath($m,$fieldName)}
	{if $rField == null}Missing field {$fieldName}!{/if}
	{$languages=MongoCMS::getFieldLanguages($m, $fieldName)}
	{$extraHTMLAttributes=($field['jsEvents']|default:'')|cat:' '|cat:($field['htmlAttributes']|default:'')}
	{if isset($field.autoComplete) && $field.autoComplete === true}
		{$extraHTMLAttributes=$extraHTMLAttributes|cat:' autocomplete="on"'}
	{elseif isset($field.autoComplete) && $field.autoComplete === false}
		{$extraHTMLAttributes=$extraHTMLAttributes|cat:' autocomplete="off"'}
	{/if}
	{$readonly="disabled readonly"}
	{$singleLangFieldName = $fieldName}
	{foreach $languages|default:[null] as $langCode => $langName}
		{$placeHolder=$field.hint|default:$field.label|default:$fieldName}
		{if !empty($langName)}
			{$fieldName=$singleLangFieldName|cat:'.'|cat:$langCode}
			{$placeHolder=$placeHolder|cat:' :: '|cat:$langName}
		{/if}
		{$fieldName=str_replace('.','*',$fieldName)}
		{$element[$fieldName] = MongoCMS::getAttrByPath($element,$fieldName)}
		{if !MongoCMS::evalDisplayCond($element, $field.displayCond|default:null)}
			{continue}
		{/if}

		{if !empty($field.format)}
		{$element[$fieldName] = $element[$fieldName]|MongoCMS_applyModifiers:$field.format:$element}
		{/if}
		{if !empty($field.type)}
		{$rField.type=$field.type}
		{/if}


		{if !empty($field.separator)}
		<div class="col-sm-12 mcms-field-separator {$field.class|default:''}">
		<h3 class="text-success">{$field.separator}</h3>
		</div>
		{/if}

		<div class="col-sm-{$field.cols} mcms-field-container {$field.class|default:''}">
		<label for="input{$fieldName}" >{$field.label|default:$fieldName}{if !empty($langName)} :: {$langName}{/if}</label>

		{if isset($field.template) && $field.template!=""}
			{include file="formFields/"|cat:$field.template}
		{else}
			{if $rField.type == 'file'}
				<input style="{$field.style|default:''}" type="file" id="input{$fieldName}" class="form-control" name="{$fieldName}" {$extraHTMLAttributes}/>
				<input type="hidden" id="input{$fieldName}_delete" name="{$fieldName}_delete" value="0" />
				{capture assign="fileFields"}{$fileFields},{ "id" : "input{$fieldName}", "options" : {$field.inputOptions|default:null|json_encode}, "currentValue" : {MongoCMS::getFile($element[$fieldName]|default:null)|json_encode} , "previewMode" : "{$field.previewMode|default:'file'}" }{/capture}
			{/if}
			{if $rField.type == 'string'}
				<input style="{$field.style|default:''}" type="text" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {$extraHTMLAttributes} {$readonly}/>
			{/if}
			{if $rField.type == 'none' }
				<span style="height:auto;background:none;{$field.style|default:''}" disabled class="form-control">{$element[$fieldName]}</span>
			{/if}
			{if $rField.type == 'array'}
				{foreach $element[$fieldName] as $key => $subElem}
				{include file="viewElementFields.tpl" viewFields=$field.fields fieldPrefix=$fieldName|cat:'.'|cat:$key|cat:'.'}
				{/foreach}
			{/if}
			{if $rField.type == 'object'}
				<span style="height:auto;background:none;{$field.style|default:''}" disabled class="form-control" {$extraHTMLAttributes}>{Denko::print_r($element[$fieldName],8,[],'mongoCMS_dump')}</span>
			{/if}
			{if $rField.type == 'password'}
				<input style="{$field.style|default:''}" autocomplete="new-password" type="password" id="input{$fieldName}" class="form-control" placeholder="* * * * * * * * * * * * *" name="{$fieldName}" {$extraHTMLAttributes} {$readonly}/>
			{/if}
			{if $rField.type == 'email'}
				<input style="{$field.style|default:''}" type="email" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {$extraHTMLAttributes} {$readonly}/>
			{/if}
			{if $rField.type == 'int' || $rField.type == 'float'}
				<input style="{$field.style|default:''}" type="number" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$readonly}/>
			{/if}
			{if $rField.type == 'richtext' || $rField.type == 'text' }
				<textarea style="height:{$field.height|default:200px};{$field.style|default:''}" id="input{$fieldName}" class="form-control" disabled="disabled" placeholder="{$placeHolder}" name="{$fieldName}" {$extraHTMLAttributes}>{$element[$fieldName]|escape:html}</textArea>
			{/if}
			{if $rField.type == 'richtext'}
				{capture assign="richFields"}input{$fieldName}:{$field.height|default:'200px'},{$richFields}{/capture}
			{/if}
			{if $rField.type == 'multi'}
				{include file="formFields/multiView.tpl"}
			{/if}
			{if $rField.type == 'bool'}
				{$yesString = "Yes"|_t}
				{$noString = "No"|_t}
				<select class="form-control" style="{$field.style|default:''}"  id="input{$fieldName}" name="{$fieldName}" {$extraHTMLAttributes} {$readonly}>
					<option value="true" {if $element[$fieldName]==true}selected{/if}>{$field.trueTxt|default:$yesString}</option>
					<option value="false" {if $element[$fieldName]==false}selected{/if}>{$field.falseTxt|default:$noString}</option>
				</select>
			{/if}
			{if $rField.type == 'date'}
				<input style="{$field.style|default:''}" type="date" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$readonly}/>
			{/if}
			{if $rField.type == 'time'}
				<input style="{$field.style|default:''}" type="time" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$readonly}/>
			{/if}
			{if $rField.type == 'datetime'}
				<input style="{$field.style|default:''}" type="datetime-local" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$readonly}/>
			{/if}
			{if $rField.type == 'table'}
				{include file="formFields/tableView.tpl"}
			{/if}
			{if $rField.type == 'editableTable'}
				{include file="formFields/tableEdit.tpl"}
			{/if}
			{if $rField.type == 'singleReference'}
				{include file="formFields/singleReferenceView.tpl"}
			{/if}
			{if $rField.type == 'multiReference'}
				{include file="formFields/multiReferenceView.tpl"}
			{/if}
			{if $rField.type == 'sortableReferences'}
				{include file="formFields/sortableReferences.tpl"}
			{/if}
			{if $rField.type == 'stringArray'}
				{include file="formFields/stringArray.tpl"}
			{/if}
			{if $rField.type|substr:0:3 == 'gps'}
				{include file="formFields/gps.tpl"}
				{$includeLocationPicker=true}
			{/if}
            {if $rField.type == 'files'}
                {include file="formFields/files.tpl"}
            {/if}
		{/if}
		</div>
	{/foreach}
{/foreach}

{if $fileFields!='[null'}
{include file="fileinput-themes.tpl"}
<script>
	var fileFields = {$fileFields|cat:"]"};
	fileFields.forEach(function(e){
		if(e==null) return;
		if(e.options==null) e.options = {};
		if(e.currentValue!=null) {
			var file = e.currentValue;
			var showZoom = false;
			var preview = '<span class="file-other-icon"><i class="glyphicon glyphicon-file" style="margin-top:20px;margin-left:10px;"></i></span>';
			var url = 'uploads/'+file._id.$oid+'/'+(file.filename||e.previewMode||"file");
			switch(e.previewMode){
				case 'image':
					preview = url;
					e.options.initialPreviewAsData = true;
					showZoom = true;
					break;
				case 'pdf':
					preview = '<object style="width:100%;height:100%;" data="'+url+'" type="application/pdf"></object>';
					//e.options.initialPreviewAsData = true;
					showZoom = true;
					break;
				case 'mp4':
					preview = '<video style="width:auto;height:100%;"  controls ><source src="'+url+'" ></video>';
					showZoom = true;
					break;
				default: break;
			}

			if(e.options.showDownload === undefined) e.options.showDownload = true;

			e.options.initialPreview = [ preview ];
			e.options.initialPreviewConfig = [{
				caption: file.filename,
				showDrag: false,
				showDownload: e.options.showDownload,
				showRemove: false,
				showZoom: showZoom,
				size: file.length,
				downloadUrl: url
			}];
			e.options.showUpload = false;
			e.options.showClose = false;
			e.options.showBrowse = false;
		}
		if(e.options.showUpload==undefined) e.options.showUpload=false;
		$("[id='"+e.id+"']").fileinput(e.options);
	});
	$('.file-caption').attr('disabled', true);
	$('.btn-file').attr('disabled', true).attr('onclick', 'return false;');
</script>
{/if}

{if $richFields!=''}
<script src="//cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
<script>
	var richFields = "{$richFields}".split(",");
	richFields.forEach(function(e){
		if(e.trim()=="") return;
		var data = e.split(':');
		var options = {
			height: data[1],
			toolbarStartupExpanded: false,
			toolbarCanCollapse: true,
			disableNativeSpellChecker: false
		};
		CKEDITOR.replace( data[0] , options );
	});
</script>
{/if}
{if $includeLocationPicker|default:false}
<script type="text/javascript" src='//maps.google.com/maps/api/js?sensor=false&libraries=places&key={constant('GMAPS_KEY')}'></script>
<script src="js{$cachePrefix}/locationpicker.jquery.min.js"></script>
<script>locationPickers.forEach(displayLocationPicker);</script>
{/if}