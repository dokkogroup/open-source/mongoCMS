{include file="header.tpl" title="Create new account"|_t section="login"}
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="container">
	<form method="post" onSubmit="return validateNewPass();">
		<div class="text-center">
			<img src="img{$cachePrefix}/logo-panel.png" />
			<h2>{'Create new account'|_t}</h2>
		</div>
		<br/>
		{if isset($smarty.get.mailSent)}
			<p class="text-warning bg-warning text-center">
				<b>{'A message has been sent to your email address with instructions to active your account'|_t}</b>
			</p>
		{else}
			<label for="inputName">{'Your name'|_t}</label>
			<input type="text" id="inputName" class="form-control" placeholder="{'Your name'|_t}" name="name" required autofocus>
			<br/>
			<label for="inputEmail">{'EMail'|_t}</label>
			<input type="email" id="inputEmail" class="form-control" placeholder="{'EMail'|_t}" name="email" required autofocus>
			<br/>
			<label for="inputPassword">{'Password'|_t}</label>
			<input type="password" id="inputPassword1" class="form-control" placeholder="{'Password'|_t}" name="password1" minlength="6" required>
			<br/>
			<label for="inputPassword">{'Confirm your password'|_t}</label>
			<input type="password" id="inputPassword2" class="form-control" placeholder="{'Password'|_t}" name="password2" minlength="6" required>
			<br/>
			{if defined('REGISTER_TYC_URL')}
			<input type="checkbox" style="zoom:1.2" class="form-check-input" id="acceptTermsAndConditions" name="acceptTermsAndConditions" required />
			<label for="inputPassword">{'I agree to the <a href="%s" target="_new">Terms and Conditions</a>.'|_t:constant('REGISTER_TYC_URL')}</label>
			<br/>
			<br/>
			{/if}
			{if !empty($error)}<p class="text-danger bg-danger text-center"><b>{'Error'|_t}:</b> {$error}</p>{/if}
			{if defined('RECAPTCHA_KEY')}
			<div style="min-height:78px;width: 304px;margin:0 auto;" class="g-recaptcha" data-sitekey="{constant('RECAPTCHA_KEY')}"></div>
			<br/>
			{/if}
			<button class="btn btn-lg btn-primary btn-block" id="smtBtn" type="submit">{'Create new account'|_t}</button>
		{/if}
	</form>
</div> <!-- /container -->
<script>
	function validateNewPass(){
		if($('#inputPassword1').val() != $('#inputPassword2').val()){
			bootbox.alert('<strong>'+{'Error'|_jst}+':</strong> '+{'Passwords don\'t match'|_jst});
			return false;
		}
		$('#smtBtn').prop('disabled',true);
		return true;
	}
</script>
{include file="footer.tpl"}