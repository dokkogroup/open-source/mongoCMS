{include file="header-printer.tpl" title=$m.ui.printingTitle|default:$m.ui.pluralLabel section=$cName}

<div class="col-xs-12 table-responsive">

	<h1>{$m.ui.printingTitle|default:$m.ui.pluralLabel|capitalize}</h1>

	<table id="elementsTable" class="table table-bordered" >
	<thead>
		<tr>
			{foreach $m.ui.printingFields|default:$m.ui.listingFields as $label=>$lField}
				{if empty($lField)}{continue}{/if}
				<th style="position:relative;">
					<img style="opacity:0.2;width:100%;height:100%;position:absolute;top:0;left:0;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mPc8h8AAm0BtR3QMUIAAAAASUVORK5CYII=">
					{$label}
				</th>
			{/foreach}
		</tr>
	</thead>
	<tbody>
		{foreach $elemArr as $key => $element }
		{capture assign="elementDescription"}{MongoCMS::getDescriptiveText($element,$cName)}{/capture}
		<tr class="normal" id="row-{$element._id}">
			{foreach $m.ui.printingFields|default:$m.ui.listingFields as $lField}
				{if empty($lField)}{continue}{/if}
				<td style="{$lField.style|default:''}">
				{if isset($lField.options.setPosition) && $lField.options.setPosition}
					<span style="cursor:pointer" onclick="changeOrder('{$element._id}','{$cName}',{$element.order|default:0},'{$elementDescription}');">
				{/if}

				{MongoCMS::getTextFromDescriptionFields($element,$lField.fields,$cName, $key)}

				{if isset($lField.options.setPosition) && $lField.options.setPosition}
					</span>
				{/if}
				{if isset($lField.options.positionArrows) && $lField.options.positionArrows}
				<br/>
				<a href="order?cn={$cName}&amp;id={$element._id}&amp;dec">↑</a>
				<a href="order?cn={$cName}&amp;id={$element._id}&amp;inc">↓</a>
				{/if}
				</td>
			{/foreach}
		</tr>
		{/foreach}
	</tbody>
	</table>
	<br/>
	<br/>
</div>

{include file="footer-printer.tpl"}