{if $section|default:""==$cName}{$class=$class|cat:" active"}{/if}
<li class="{$class}">
	<a  href="{MongoCMS::getListingURL($cName)}" title="{$metadata.ui.pluralLabel}">
	{if $metadata.ui.icon|default:'' != ''}
		{if strpos($metadata.ui.icon,'glyphicon')!==false}
			<span class="glyphicon {$metadata.ui.icon}"></span>
		{elseif strpos($metadata.ui.icon,'.png')!==false}
			<img class="image-icon" src="./img/{$metadata.ui.icon}" />
		{else}
			<span class="material-icons">{$metadata.ui.icon}</span>
		{/if}
	{/if}
	<span class="nav-sidebar-item-label">{$metadata.ui.pluralLabel}</span>
	</a>
</li>