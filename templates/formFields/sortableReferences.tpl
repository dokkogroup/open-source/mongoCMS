{if strpos($readonly,'readonly')===false}
<select class="selectpicker form-control" data-live-search="true" id="input{$fieldName}" {$extraHTMLAttributes} {$readonly} >
	<option value=""></option>
	{$options = MongoCMS::getCollection($rField.collection)->find($field.filter|default:[],[ 'sort' => $field.sort|default:[] ])}
	{foreach $options as $option}
	<option value="{$option._id}">
		{MongoCMS::getDescriptiveText($option,$rField.collection)}
	</option>
	{/foreach}
</select>
<input type="hidden" id="hidden{$fieldName}" name="{$fieldName}" />
{/if}
<table class="table table-striped table-bordered" border=1  style="margin-top:20px" >
	<tbody id="table{$fieldName}">
		{foreach $element[$fieldName] as $value}
		<tr><td value="{(string)$value.id}">
			{MongoCMS::getDescriptiveText($value.id,$rField.collection)}
			{if strpos($readonly,'readonly')===false}
			<button type="button" class="btn btn-default btn-sm pull-right" onclick="removeSortableReferenceValue('{$fieldName}',this)"><span class="glyphicon glyphicon-trash"></span></button>
			{/if}
		</td></tr>
		{/foreach}
	</tbody>
</table>

{if strpos($readonly,'readonly')===false}
<script>

$('#input{$fieldName}').change(function(){
	addSortableReferenceValue('{$fieldName}', document.getElementById('input{$fieldName}'));
});

$(function() {
	$("#table{$fieldName}" ).sortable({
		containerSelector:'tbody',
		itemSelector:'tr',
		placeholder: '<tr style="background-color:#DDD;"><td id="placeholder">&nbsp;</td></tr>',
		onDragStart:function ($item, _super, event) {
			$item.hide();
		},
		onDrop:function ($item, _super, event) {
			$item.show();
			saveSortableReferenceValue("{$fieldName}");
		}
	});
});
{if !defined('sortableReferencesInitialized')}
{define('sortableReferencesInitialized',true)}
function saveSortableReferenceValue(name){
	var res = [];
	$("#table"+name+" td").each(function(pos,elem){
		res.push({
			id:$(elem).attr('value')
		});
	});
	$("#hidden"+name).val(JSON.stringify(res));
}

function removeSortableReferenceValue(name, elem){
	$(elem).parent().parent().remove();
	saveSortableReferenceValue(name);
}

function addSortableReferenceValue(name, select){
	if(select.value=='') return;
	var optionText = $("#input"+name+" option:selected").text();
	var markup = "<tr><td value="+select.value+">"+optionText;
	markup+='<button type="button" class="btn btn-default btn-sm pull-right" onclick="removeSortableReferenceValue(\''+name+'\',this)"><span class="glyphicon glyphicon-trash"></span></button>';
	markup+="</td></tr>";
	$("#table"+name).append(markup);
	select.value='';
	saveSortableReferenceValue(name);
}
{/if}
saveSortableReferenceValue("{$fieldName}");
</script>
{/if}