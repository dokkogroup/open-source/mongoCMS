{if !$rField.multiple|default:false}
<select class="selectpicker show-tick form-control" data-live-search="true" style="{$field.style|default:''}"  id="input{$fieldName}" name="{$fieldName}" {$extraHTMLAttributes} {$readonly}>
{foreach MongoCMS::getFieldOptions($cName,$fieldName,$field.options|default:null) as $option => $optionName}
	<option value="{$option}" {if $element[$fieldName]==$option}selected{/if}>{$optionName}</option>
{/foreach}
</select>
{else}
<select class="selectpicker show-tick form-control" data-live-search="true" multiple style="{$field.style|default:''}"  id="input{$fieldName}[]" name="{$fieldName}[]" {$readonly}>
{foreach MongoCMS::getFieldOptions($cName,$fieldName,$field.options|default:null) as $option => $optionName}
	<option value="{$option}" {if in_array($option,(array)($element[$fieldName]|default:[])) || (!empty($rField.defaultAll) && $action|lower=='new') }selected{/if}>{$optionName}</option>
{/foreach}
</select>
{/if}