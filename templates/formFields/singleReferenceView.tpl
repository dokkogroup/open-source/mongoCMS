{$customField=$rField.customField|default:'_id'}
	{if (!empty($rField.customField))}
		{$value = MongoCMS::getByCustomField($rField.collection,$rField.customField,$element[$fieldName],$rField.customFieldType)}
	{else}
		{$value = MongoCMS::getByID($rField.collection,$element[$fieldName])}
	{/if}
<select class="form-control" style="{$field.style|default:''}"  id="input{$fieldName}" name="{$fieldName}" {$extraHTMLAttributes} {$readonly}>
	{if $value!=null}
	<option value="{$value.$customField}" selected>
		{MongoCMS::getDescriptiveText($value,$rField.collection)}
	</option>
	{else}
	<option value="null" selected>-none-</option>
	{/if}
</select>
