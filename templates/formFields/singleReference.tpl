<select class="selectpicker show-tick form-control" data-live-search="true" style="{$field.style|default:''}"  id="input{$fieldName}" name="{$fieldName}" {$extraHTMLAttributes} {$readonly}>
{$customField=$rField.customField|default:'_id'}
{if strpos($readonly,'readonly')===false}
	{if empty($required)}<option value="null">-none-</option>{/if}
	{$fo = MongoCMS::getReferenceSelectorFilters($m,$field)}
	{MongoCMS::executeOnFilterCallback($field.onFilter|default:'', $fo.filters, $fo.options)}
	{$options = MongoCMS::getCollection($rField.collection)->find($fo.filters,$fo.options)}
	{foreach $options as $option}
	<option value="{$option.$customField}" {if $element[$fieldName]==$option.$customField}selected{/if}>
		{MongoCMS::getDescriptiveText($option,$rField.collection)}
	</option>
	{/foreach}
{else}
	{if (!empty($rField.customField))}
		{$value = MongoCMS::getByCustomField($rField.collection,$rField.customField,$element[$fieldName],$rField.customFieldType)}
	{else}
		{$value = MongoCMS::getByID($rField.collection,$element[$fieldName])}
	{/if}
	{if $value!=null}
	<option value="{$value.$customField}" selected>
		{MongoCMS::getDescriptiveText($value,$rField.collection,$customField)}
	</option>
	{else}
	<option value="null" selected>-none-</option>
	{/if}
{/if}
</select>
