{if !$rField.multiple|default:false}
	<select class="form-control" style="{$field.style|default:''}"  id="input{$fieldName}" name="{$fieldName}" {$extraHTMLAttributes} {$readonly}>
		{foreach MongoCMS::getFieldOptions($cName,$fieldName,$field.options|default:null) as $option => $optionName}
			{if $element[$fieldName]==$option}
			<option value="{$option}" selected>{$optionName}</option>
			{/if}
		{/foreach}
	</select>
{else}
	<select class="form-control"  multiple style="{$field.style|default:''}"  id="input{$fieldName}[]" name="{$fieldName}[]" {$readonly}>
	{foreach MongoCMS::getFieldOptions($cName,$fieldName,$field.options|default:null) as $option => $optionName}
		{if in_array($option,(array)($element[$fieldName]|default:[]))}
		<option value="{$option}" >{$optionName}</option>
		{/if}
	{/foreach}
	</select>
{/if}