<select class=" form-control" multiple id="input{$fieldName}" name="{$fieldName}[]" {$readonly}>
	{foreach $element[$fieldName] as $value}
	{$desc=MongoCMS::getDescriptiveText($value,$rField.collection)}
	{if empty($desc)}{continue}{/if}
	<option value="{$value}">
		{MongoCMS::getDescriptiveText($value,$rField.collection)}
	</option>
	{/foreach}
</select>