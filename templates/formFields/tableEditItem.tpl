		<tr class="normal row-item" id="row-{$element._id}-{$key}">
			{foreach $field.tableFields as $colLabel => $subFields}
				
                {if empty($lField)}{continue}{/if}
                
				<td style="{$subFields.style|default:''}" >
					{if is_string($subFields.fields|default:null)}
						{$subFields['fields'] = [ $subFields['fields'] => null ]}
					{/if}
					{foreach $subFields.fields|default:[] as $subFieldName => $field}
						
                        {$fieldName = $fieldPath|cat:'*'|cat:$key|cat:'*'|cat:$subFieldName}
						{$fieldRName = $fieldPath|cat:'*?*'|cat:$subFieldName}
						{$placeHolder=$field.hint|default:$colLabel|default:$fieldName}
						{if !empty($langName)}
							{$fieldName=$singleLangFieldName|cat:'.'|cat:$langCode}
							{$placeHolder=$placeHolder|cat:' :: '|cat:$langName}
						{/if}
						{$element[$fieldName] = MongoCMS::getAttrByPath($element,$fieldName)}
						{$rField = MongoCMS::getFieldInfoByPath($m,$fieldRName)}
						{$required=(isset($rField.nullable) && $rField.nullable===true)?"":"required"}
						{if $rField.type == 'string'}
							<input style="{$field.style|default:''}" type="text" id="input{$fieldName}" class="form-control row-item-input" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {$extraHTMLAttributes} {$required} {$readonly}/>
						{/if}
						{if $rField.type == 'int' || $rField.type == 'float'}
							<input style="{$field.style|default:''}" type="number" id="input{$fieldName}" class="form-control row-item-input" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$required} {$readonly}/>
						{/if}
						{if $rField.type == 'file'}
							{$fileinfo=MongoCMS::getFile($element[$fieldName]|default:null)}
							{if (!empty($fileinfo))}
								<p><a href="uploads/{$fileinfo['_id']}/{$fileinfo['filename']}">{$fileinfo['filename']}</a></p>
							{/if}
							<input style="{$field.style|default:''}" type="file" id="input{$fieldName}" accept="{$field.inputOptions.allowedFileTypes|default:[]|implode:'|'|replace:'pdf':'application/pdf'|replace:'image':'image/*'}" class="form-control" name="{$fieldName}" {if empty($element[$fieldName])}{$extraHTMLAttributes} {$required}{/if} {$readonly}/>
							<input type="hidden" id="input{$fieldName}_delete" name="{$fieldName}_delete" value="0" />
							{capture assign="fileFields"}{$fileFields},{ "id" : "input{$fieldName}", "options" : {$field.inputOptions|default:null|json_encode}, "currentValue" : {MongoCMS::getFile($element[$fieldName]|default:null)|json_encode} , "previewMode" : "{$field.previewMode|default:'file'}" }{/capture}
						{/if}
						{if $rField.type == 'multi'}
							{include file="formFields/multi.tpl"}
						{/if}
						{if $rField.type == 'singleReference'}
							{include file="formFields/singleReference.tpl"}
						{/if}
						
					{/foreach}
				</td>
			{/foreach}
			        <td style="text-align:center;padding-top:15px;">
                        <span role="button" class="glyphicon glyphicon-trash row-item-delete"  onClick="deleteRow('{$element._id}','{$key}','{$fieldPath}');"></span>
					</td>

		</tr>
