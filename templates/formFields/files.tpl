{include file="fileinput-themes.tpl"}
<input style="{$field.style|default:''}" type="file" id="input{$fieldName}"
       accept="{$field.inputOptions.allowedFileTypes|default:[]|implode:'|'|replace:'pdf':'application/pdf'|replace:'image':'image/*'}"
       class="form-control"
       name="{$fieldName}_container[]" {if empty($element[$fieldName])}{$extraHTMLAttributes} {/if} {$readonly}
       multiple="multiple"/>
<input type="hidden" id="input{$fieldName}_real" name="{$fieldName}" value="0"/>

<script>
(function(){
    var getPreviewMode = function (contentType){
        var mode=contentType.split('\/');
        switch (mode[0]){
            case 'image':
                return 'image';
            case 'video':
            case 'application':
                return mode[1];
            default:
                return '';
        }
    }

    var options = {
        overwriteInitial : false,
        showUpload : true,
        showClose : false,
        uploadAsync : true,
        uploadUrl : "?action=uploadFile",
        append : true,
        fileActionSettings : {
            dragIcon : '<i class="glyphicon glyphicon-move"></i>',
            dragClass :'text-info'
        },
        previewClass: 'fileinput-preview'
    };

    var overrideOptions = {$field.inputOptions|default:'{}'|json_encode};

    for (var attrname in overrideOptions) { options[attrname] = overrideOptions[attrname]; }

    var e = {
        id: "input{$fieldName}",
        options: options,
        currentValue : {MongoCMS::getFiles($element[$fieldName]|default:null)|json_encode},
        previewMode : "{$field.previewMode|default:'pdf'}"
    };
    var previews =[];
    var previewsConfig =[];

    if (e.currentValue != null) {
        var data=e.currentValue;
        data.forEach(function (file,index) {
            if (file != null) {
                var showZoom = false;
                var initialPreviewAsData = false;
                var preview = '<span class="file-other-icon"><i class="glyphicon glyphicon-file" style="margin-top:20px;margin-left:10px;"></i></span>';

                var url = 'uploads/'+file._id.$oid+'/'+(file.filename||e.previewMode||"file");

                var previewMode=getPreviewMode(file.contentType);
                switch (previewMode) {
                    case 'image':
                        preview = url;
                        initialPreviewAsData = true;
                        showZoom = true;
                        break;
                    case 'pdf':
                        preview = '<object style="width:100%;height:100%;" data="'+url+'" type="application/pdf"></object>';
                        showZoom = true;
                        break;
                    case 'mp4':
                        preview = '<video style="width:auto;height:100%;"  controls ><source src="'+url+'" ></video>';
                        showZoom = true;
                        break;
                    default:
                        break;
                }

                var readonly = {if strpos($readonly,'readonly')===false}false{else}true{/if}; //
                previews.push(preview);
                if(readonly) e.options.showRemove = false;
                if(readonly) e.options.showDrag = false;
                if(e.options.showDownload === undefined) e.options.showDownload = true;
                if(e.options.showRemove === undefined) e.options.showRemove = !readonly;
                if(e.options.showDrag === undefined) e.options.showDrag = !readonly;

                var config = {
                    caption: file.filename,
                    showZoom: showZoom,
                    filetype: file.contentType,
                    size: file.length,
                    showDrag: e.options.showDrag,
                    showRemove:  e.options.showRemove,
                    showDownload: e.options.showDownload,
                    url: "allow.html",
                    previewAsData: initialPreviewAsData,
                    key:index,
                    downloadUrl: url,
                    frameAttr: { 'mongoid':file._id.$oid, 'inputid':e.id }
                };
                previewsConfig.push(config);
            }
        });
    }
    e.options.initialPreview =  previews ;
    e.options.initialPreviewConfig =  previewsConfig ;
    {if L10N::$currentLang == 'es'}
        e.options.language='es';
    {/if}

    $("[id='" + e.id + "']").fileinput(e.options);

    $("[id='" + e.id + "']").on('filepreajax', function(event, previewId, index) {
        $('#'+previewId).attr('temp-id',previewId);
    });

    $("[id='" + e.id + "']").on("fileuploaded", function (event, data,previewId, index) {
        $('[temp-id='+ previewId +']').attr({ 'mongoid':data.response.id, 'inputid':e.id });
        return false;
    });

    $('BUTTON.kv-file-zoom').attr('disabled',null);

    $('#mCMSForm').on("submit", function (e){
        if ($('#input{$fieldName}').fileinput("getFileStack").length > 0){
            $('#errorBox').html('<strong>' + {'Error'|_jst}+': </strong>' + {'There are files without uploading. Please upload or delete them'|_jst} );
            $('#errorBox').show();
            return false;
        }
        var mongo_ids=[];
        $('.kv-preview-thumb[inputid=input{$fieldName}]').each(function (index,e){
            mongo_ids.push($(e).attr('mongoid'));
        });
        $('#input{$fieldName}_real').val(JSON.stringify(mongo_ids));
    });
})();
</script>

