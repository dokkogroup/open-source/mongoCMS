{if strpos($readonly,'readonly')===false}
{$values = []}{foreach $element[$fieldName] as $value}{$values[]=(string)$value}{/foreach}
<select class="selectpicker form-control" data-live-search="true"  multiple id="input{$fieldName}" name="{$fieldName}[]" {$extraHTMLAttributes} {$readonly}>
	{$fo = MongoCMS::getReferenceSelectorFilters($m,$field)}
	{MongoCMS::executeOnFilterCallback($field.onFilter|default:'', $fo.filters, $fo.options)}
	{$options = MongoCMS::getCollection($rField.collection)->find($fo.filters, $fo.options)}
	{foreach $options as $option}
	<option value="{$option._id}" {if in_array((string)$option._id,$values) || (!empty($rField.defaultAll) && $action|lower=='new') }selected{/if}>
		{MongoCMS::getDescriptiveText($option,$rField.collection)}
	</option>
	{/foreach}
	<option id="hidden_{$fieldName}" value="" class="hidden"></option>
</select>
<script>
	$('#input{$fieldName}').change(function (){
		var count = $('#input{$fieldName}').children('option:selected').length;
		if(count==0) {
			$('#hidden_{$fieldName}').selected(true);
			setTimeout(function(){
				$('button[data-id="input{$fieldName}"]').addClass('bs-placeholder');
			},1);
		}
		if(count>1) $('#hidden_{$fieldName}').selected(false);
	});
</script>
{else}
<select class="form-control" multiple id="input{$fieldName}" name="{$fieldName}[]" {$readonly}>
	{foreach $element[$fieldName] as $value}
	<option value="{$value}">
		{MongoCMS::getDescriptiveText($value,$rField.collection)}
	</option>
	{/foreach}
</select>
{/if}