{if !empty($field.displayLocationInput)}
<input class="form-control" style="margin-bottom:-3px;padding-top:4px;border-bottom:none;" type="text" id="locationName_{$fieldName|replace:'*':'_'}" onkeydown="if(event.which == 13) event.preventDefault();" />
{/if}
{if $rField.type=='gpsPair'}
	<input type="hidden" name="{$fieldName}" value="{$element[$fieldName]|default:$rField.default|default:'0,0'|escape:html}"/>
{else}
	{if $rField.type=='gpsLat'}
		{$fieldNameLat=$fieldName}
		{$fieldNameLon = $rField.lon|replace:'.':'*'}
	{elseif $rField.type=='gpsLon'}
		{$fieldNameLon=$fieldName}
		{$fieldNameLat = $rField.lat|replace:'.':'*'}
	{elseif $rField.type=='gps'}
		{$fieldNameLat = $fieldName|cat:'*'|cat:$rField.lat|replace:'.':'*'}
		{$fieldNameLon = $fieldName|cat:'*'|cat:$rField.lon|replace:'.':'*'}
	{/if}
	{$element[$fieldNameLat] = MongoCMS::getAttrByPath($element,$fieldNameLat)}
	{$element[$fieldNameLon] = MongoCMS::getAttrByPath($element,$fieldNameLon)}
	{$rFieldLat = MongoCMS::getFieldInfoByPath($m,$fieldNameLat)}
	{$rFieldLon = MongoCMS::getFieldInfoByPath($m,$fieldNameLon)}
	<input type="hidden" name="{$fieldNameLat}" value="{$element[$fieldNameLat]|default:$rFieldLat.default|default:0|escape:html}"/>
	<input type="hidden" name="{$fieldNameLon}" value="{$element[$fieldNameLon]|default:$rFieldLon.default|default:0|escape:html}"/>
{/if}
<div id="gps_{$fieldName}" style="height: 300px;border:solid #ccc 1px;"></div>
<script>
var gpsFields = {};
{if $rField.type=='gpsPair'}
	gpsFields = parseGPS($('input[name=\'{$fieldName}\']').val());
{else}
	gpsFields.lat = parseFloat($('input[name=\'{$fieldNameLat}\']').val());
	gpsFields.lon = parseFloat($('input[name=\'{$fieldNameLon}\']').val());
{/if}

function displayLocationPicker(picker){
	$('div[id=\''+picker.elementId+'\']').locationpicker(picker);
	if(picker.markerContent!=''){
		var infoWindow = new google.maps.InfoWindow();
		var map = $('div[id=\''+picker.elementId+'\']').locationpicker("map");
		google.maps.event.addListener(map.marker, "click", function(e){
			infoWindow.setContent(picker.markerContent);
			infoWindow.open(map, map.marker);
		});
	}
}

if(typeof locationPickers == 'undefined'){
	locationPickers = [];
}

locationPickers.push({
		elementId: 'gps_{$fieldName}',
		location: {
			latitude: gpsFields.lat,
			longitude: gpsFields.lon
		},
		radius: 0,
		mapOptions: {
			markerDraggable:{if strpos($readonly,'readonly')===false}true{else}false{/if}
		},
		zoom: {$field.zoom|default:12},
		onchanged: function(currentLocation, radius, isMarkerDropped) {
			{if $rField.type=='gpsPair'}
				$('input[name=\'{$fieldName}\']').val(currentLocation.latitude+", "+currentLocation.longitude);
			{else}
				$('input[name=\'{$fieldNameLat}\']').val(currentLocation.latitude);
				$('input[name=\'{$fieldNameLon}\']').val(currentLocation.longitude);
			{/if}
		},
		{if !empty($field.displayLocationInput)}
		inputBinding: {
			locationNameInput: $('#locationName_{$fieldName|replace:'*':'_'}')
		},
		{/if}
		enableAutocomplete: true,
		markerContent : '{MongoCMS::getTextFromDescriptionFields($element,$field.markerContent|default:[],$cName)}'
	});
</script>