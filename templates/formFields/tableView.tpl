<table id="elementsTable" class="table table-striped table-bordered" {$extraHTMLAttributes}>
	<thead>
		<tr>
			{foreach $field.tableFields as $label=>$lField}
				{if empty($lField)}{continue}{/if}
				<th>{$label}</th>
			{/foreach}
		</tr>
	</thead>
	<tbody>
		{$elements = (array)$element[$fieldName]}
		{foreach $elements as $key => $val }
		<tr class="normal" id="row-{$element._id}-{$key}">
			{foreach $field.tableFields as $lField}
				{if empty($lField)}{continue}{/if}
				<td style="{$lField.style|default:''}">
				{MongoCMS::getTextFromDescriptionFields($val,$lField.fields,$cName,$key)}
				</td>
			{/foreach}
		</tr>
		{/foreach}
	</tbody>
</table>