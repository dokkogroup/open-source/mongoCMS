{if $element.type==4}
	<textarea id="input{$fieldName}" class="form-control" placeholder="Configuration value" name="{$fieldName}" rows="10" {$readonly}>{$element.value|escape:html}</textarea>
	<script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
	<script>
		var options = {
			height: '200px',
			disableNativeSpellChecker: false
		};
		options.toolbarGroups = [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
			{ name: 'editing', groups: [ 'find', 'selection' ] },
			{ name: 'links' },
			{ name: 'insert', options : ['image'] },
			// { name: 'forms' },
			{ name: 'tools' },
			{ name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
			//{ name: 'others' },
			'/',
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
			{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
			{ name: 'styles' },
			{ name: 'colors' },
		];
		options.removeButtons = 'Flash,Save,Preview,NewPage';
		CKEDITOR.replace( 'input{$fieldName}' , options );
	</script>
{elseif $element.type==3}
	<textarea id="input{$fieldName}" class="form-control" placeholder="Configuration value" name="{$fieldName}" rows="10" {$readonly}>{$element.value|escape:html}</textarea>
{elseif $element.type==1}
	<input type="number" min="0" step="1" id="inputValue" class="form-control" placeholder="Configuration value" name="{$fieldName}" rows="10" required {$readonly} value="{$element.value|escape:html}" />
{elseif $element.type==5}
	<input type="password" id="input{$fieldName}" class="form-control" placeholder="Configuration value" name="{$fieldName}" rows="10" {$readonly} value="{$element.value|escape:html}" />
{else}
	<input type="text" id="input{$fieldName}" class="form-control" placeholder="Configuration value" name="{$fieldName}" rows="10" {$readonly} value="{$element.value|escape:html}" />
{/if}