{$fieldPath = $fieldName}
<table id="elementsTable_{$fieldPath}" class="table table-striped table-bordered" {$extraHTMLAttributes}>
	<thead>
		<tr>
			{foreach $field.tableFields as $label=>$lField}
				{if empty($lField)}{continue}{/if}
				<th style="font-size:0.9em;">{$label}</th>
			{/foreach}
			<th style="width:20px;text-align:center;"><span  id="add-row_{$fieldPath}" class="btn btn-success btn-xs add-row glyphicon glyphicon-plus"> </span></th>
		</tr>
	</thead>
	<tbody>
		{$elements = (array)$element[$fieldPath]}

		{foreach $elements as $key => $val }
			{include file="formFields/tableEditItem.tpl"}
		{/foreach}
		{capture name="rowTemplate" assign="rowTemplateEdit"}
			{include file="formFields/tableEditItem.tpl" key="%_ROW_NUMBER_%"}
		{/capture}
		<input type="hidden" value="" name="to_delete_tableitems[]" id="to_delete_tableitems_{$fieldPath}"/>
	</tbody>

</table>

<script>
	var rowTemplateEdit=rowTemplateEdit || {};
	rowTemplateEdit['{$fieldPath}']={$rowTemplateEdit|json_encode};
	var tableRows=tableRows || {};
	tableRows['{$fieldPath}']=$('#elementsTable_{$fieldPath} tr.normal').length;

	function deleteRow(element,row,fieldPath){
		$('#elementsTable_'+fieldPath+' #row-'+element+'-'+row).remove();
		reIndexTable(element,fieldPath);
	}

	function reIndexTable(element,fieldPath){
		$('#elementsTable_'+fieldPath+' .row-item').each(function (index,item){
			$(item).find('.row-item-input').each(function(i,input){
				let new_input_name=$(input).attr('name').replace(/\*\d*\*/gm,'*'+index+'*');
				$(input).attr('name',new_input_name);
				$(input).attr('id','input'+new_input_name);
			});
			$(item).attr('id','row-'+element+'-'+index);
			$(item).find('.row-item-delete').attr('onclick',"deleteRow('"+element+"','"+index+"','"+fieldPath+"');");
		});
		$('#to_delete_tableitems_'+fieldPath).val('');
		let cant=$('#elementsTable_'+fieldPath+' tr.normal').length;
		if (cant < tableRows[fieldPath]){
			let to_delete='';
			for (let i=cant;i<tableRows[fieldPath];i++){
				to_delete+=fieldPath+'|'+i+',';
			}
			$('#to_delete_tableitems_'+fieldPath).val(to_delete);
		}
	}

	$(document).ready(function(){
			$("#add-row_{$fieldPath}").click(function(){
				let cant=$('#elementsTable_{$fieldPath} tr.normal').length;
				item = rowTemplateEdit['{$fieldPath}'].replace(/%_ROW_NUMBER_%/g, cant);
				$("#elementsTable_{$fieldPath} tbody").append(item);
				$('#elementsTable_{$fieldPath} .selectpicker').selectpicker('refresh');
				reIndexTable('{$element._id|default:new_id}','{$fieldPath}');
			});
	});      	
</script>