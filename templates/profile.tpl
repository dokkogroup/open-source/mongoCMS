{include file="header.tpl" title="My profile"|_t section="profile"}

<h1 class="mcms-main-title">{'My profile'|_t}</h1>
<br/>

<form method="post" enctype="multipart/form-data" id="mCMSForm" onSubmit="return onMCMSFormSubmit(this);">
	{$richFields = ""}
	{$fileFields = "[null"}
	{foreach $m.ui.fields as $fieldName=>$field }
		{if empty($field)}{continue}{/if}
		{if !isset($m.fields[$fieldName])}Missing field {$fieldName}!{/if}
		{assign var="rField" value=$m.fields[$fieldName]}
		{$required=(isset($rField.nullable) && $rField.nullable===true)?"":"required"}
		{$readonly=(!empty($rField.readonly))?"disabled readonly":"autofocus"}
		{$extraHTMLAttributes=($field['jsEvents']|default:'')|cat:' '|cat:($field['htmlAttributes']|default:'')}
		{if isset($field.autoComplete) && $field.autoComplete === true}
			{$extraHTMLAttributes=$extraHTMLAttributes|cat:' autocomplete="on"'}
		{elseif isset($field.autoComplete) && $field.autoComplete === false}
			{$extraHTMLAttributes=$extraHTMLAttributes|cat:' autocomplete="off"'}
		{/if}
		{if !isset($element[$fieldName])}{$element[$fieldName]=null}{/if}
		{if !MongoCMS::evalDisplayCond($element, $field.displayCond|default:null)}
			{continue}
		{/if}

		{if !empty($field.separator)}
		<div class="col-sm-12 mcms-field-separator {$field.class|default:''}">
		<h3 class="text-success">{$field.separator}</h3>
		</div>
		{/if}

		<div class="col-sm-{$field.cols} mcms-field-container {$field.class|default:''}">
		<label for="input{$fieldName}" >{$field.label|default:$fieldName}</label>

		{if isset($field.template) && $field.template!=""}
			{include file="formFields/"|cat:$field.template}
		{else}
			{if $rField.type == 'file'}
				<input style="{$field.style|default:''}" type="file" id="input{$fieldName}" class="form-control" name="{$fieldName}" {if empty($element[$fieldName])}{$extraHTMLAttributes} {$required}{/if} {$readonly}/>
				<input type="hidden" id="input{$fieldName}_delete" name="{$fieldName}_delete" value="0" />
				{capture assign="fileFields"}{$fileFields},{ "id" : "input{$fieldName}", "options" : {$field.inputOptions|default:null|json_encode}, "currentValue" : {MongoCMS::getFile($element[$fieldName]|default:null)|json_encode} , "previewMode" : "{$field.previewMode|default:'file'}" }{/capture}
			{/if}
			{if $rField.type == 'string'}
				<input style="{$field.style|default:''}" type="text" id="input{$fieldName}" class="form-control" placeholder="{$field.hint|default:$field.label|default:$fieldName}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {$extraHTMLAttributes} {$required} {$readonly}/>
			{/if}
			{if $rField.type == 'password'}
				<input style="{$field.style|default:''}" autocomplete="new-password" type="password" id="input{$fieldName}" class="form-control"
				{if empty($element[$fieldName])}
					placeholder="{$field.hint|default:$field.label|default:$fieldName}" {$extraHTMLAttributes} {$required}
				{else}
					placeholder="{'enter new password or leave empty to keep the same'|_t}"
				{/if}
				name="{$fieldName}" {$readonly}/>
			{/if}
			{if $rField.type == 'email'}
				<input style="{$field.style|default:''}" type="email" id="input{$fieldName}" class="form-control" placeholder="{$field.hint|default:$field.label|default:$fieldName}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {$extraHTMLAttributes} {$required} {$readonly}/>
			{/if}
			{if $rField.type == 'int'}
				<input style="{$field.style|default:''}" type="number" id="input{$fieldName}" class="form-control" placeholder="{$field.hint|default:$field.label|default:$fieldName}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$required} {$readonly}/>
			{/if}
			{if $rField.type == 'richtext' || $rField.type == 'text' }
				<textarea style="height:{$field.height|default:200px};{$field.style|default:''}" id="input{$fieldName}" class="form-control" placeholder="{$field.hint|default:$field.label|default:$fieldName}" name="{$fieldName}" {$extraHTMLAttributes} {$required} {$readonly}>{$element[$fieldName]|escape:html}</textArea>
			{/if}
			{if $rField.type == 'richtext'}
				{capture assign="richFields"}input{$fieldName}:{$field.height|default:'200px'},{$richFields}{/capture}
			{/if}
			{if $rField.type == 'bool'}
				{$yesString = "Yes"|_t}
				{$noString = "No"|_t}
				<select class="form-control" style="{$field.style|default:''}"  id="input{$fieldName}" name="{$fieldName}" {$readonly}>
					<option value="true" {if $element[$fieldName]==true}selected{/if}>{$field.trueTxt|default:$yesString}</option>
					<option value="false" {if $element[$fieldName]==false}selected{/if}>{$field.falseTxt|default:$noString}</option>
				</select>
			{/if}
			{if $rField.type == 'date'}
				<input style="{$field.style|default:''}" type="date" id="input{$fieldName}" class="form-control" placeholder="{$field.hint|default:$field.label|default:$fieldName}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$required} {$readonly}/>
			{/if}
			{if $rField.type == 'time'}
				<input style="{$field.style|default:''}" type="time" id="input{$fieldName}" class="form-control" placeholder="{$field.hint|default:$field.label|default:$fieldName}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$required} {$readonly}/>
			{/if}
			{if $rField.type == 'datetime'}
				<input style="{$field.style|default:''}" type="datetime-local" id="input{$fieldName}" class="form-control" placeholder="{$field.hint|default:$field.label|default:$fieldName}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$required} {$readonly}/>
			{/if}
			{if $rField.type == 'multi'}
				{include file="formFields/multi.tpl"}
			{/if}
			{if $rField.type == 'singleReference'}
				{include file="formFields/singleReference.tpl"}
			{/if}
			{if $rField.type == 'multiReference'}
				{include file="formFields/multiReference.tpl"}
			{/if}
		{/if}
		</div>
	{/foreach}
	<div class="col-sm-12 text-right">
		<div class="col-sm-12 text-center alert" id="errorBox" style="display:none;"></div>
		<button class="btn btn-lg btn-primary" type="submit" id="mCMSSaveBtn">{'Save'|_t}</button>
		<a class="btn btn-lg btn-danger" href="{$returnLocation}">{'Back'|_t}</a>
		<br/>
		<br/>
		<br/>
	</div>
</form>

{if $fileFields!='[null'}
<script>
	var fileFields = {$fileFields|cat:"]"};
	fileFields.forEach(function(e){
		if(e==null) return;
		if(e.options==null) e.options = {};
		if(e.currentValue!=null) {
			var file = e.currentValue;
			var showZoom = false;
			var preview = '<span class="file-other-icon"><i class="glyphicon glyphicon-file" style="margin-top:20px;margin-left:10px;"></i></span>';

			switch(e.previewMode){
				case 'image':
					preview = 'uploads/'+file._id.$oid+'/'+(file.filename||'image');
					e.options.initialPreviewAsData = true;
					showZoom = true;
					break;
				default: break;
			}

			e.options.initialPreview = [ preview ];
			e.options.initialPreviewConfig = [
				{ caption: file.filename, showDrag:false, showZoom: showZoom, size: file.length, url: "allow.html", key: 1 }]; 
			e.options.showUpload = false;
			e.options.showRemove = false;
			e.options.showClose = false;
		}
		if(e.options.showUpload==undefined) e.options.showUpload=false;
		$('#'+e.id).fileinput(e.options);
		$('#'+e.id).on("filepredelete", function(a) {
			$('#'+e.id+"_delete").val(1);
			return false;
		});
		$('#'+e.id+"_delete").val(0);
	});
</script>
{/if}

{if $richFields!=''}
<script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
<script>
	var richFields = "{$richFields}".split(",");
	richFields.forEach(function(e){
		if(e.trim()=="") return;
		var data = e.split(':');
		var options = { 
			height: data[1],
			disableNativeSpellChecker: false
		};
		options.toolbarGroups = [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
			{ name: 'editing', groups: [ 'find', 'selection' ] },
			{ name: 'links' },
			{ name: 'insert', options : ['image'] },
			// { name: 'forms' },
			{ name: 'tools' },
			{ name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
			//{ name: 'others' },
			'/',
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
			{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
			{ name: 'styles' },
			{ name: 'colors' },
		];
		options.removeButtons = 'Flash,Save,Preview,NewPage';
		CKEDITOR.replace( data[0] , options );
	});
</script>
{/if}

<script>
	$('#mCMSForm').ajaxForm(function(res) {
		$('#mCMSSaveBtn').prop('disabled', true);
		var message = '';
		try{
			if(res.status == 'ok'){
				window.location = res.redirect;
				return;
			}
			if(typeof res.message !== 'undefined') message = res.message;
			else message = {_jst('Response is not a valid JSON.')};
		}catch(e){
			message = {_jst('Response is not a valid JSON.')};
		}
		$('#mCMSSaveBtn').prop('disabled', false);

		$('#errorBox').addClass('alert-danger');
		$('#errorBox').removeClass('alert-success');
		$('#errorBox').html('<strong>'+{'Error'|_jst}+':</strong> '+message);
		$('#errorBox').show();
	});

	function onMCMSFormSubmit(form){
		$('#errorBox').hide();
		var onSubmit = function(form){
			{$m.ui.onSubmit|default:'return true;'}
		};
		if(!onSubmit(form)) return false;
		if ( typeof(CKEDITOR) !== 'undefined' ) {
			for ( instance in CKEDITOR.instances ){
				CKEDITOR.instances[instance].updateElement();
			}
		}
		return true;
	}

	if(window.location.hash.replace('#','') == 'updated'){
		window.location.hash = 'updated!';
		$('#errorBox').removeClass('alert-danger');
		$('#errorBox').addClass('alert-success');
		$('#errorBox').html({'Profile updated'|_jst});
		$('#errorBox').show();
		$('#mCMSSaveBtn').prop('disabled', false);
	}
</script>

{include file="footer.tpl"}