{include file="header.tpl" title=$action|cat:' %s'|_t:$m.ui.label section=$cName}

<h1 class="mcms-main-title">{$action|cat:' %s'|_t:$m.ui.label}</h1>
<br/>

{include file="viewElementFields.tpl" viewFields=$m.ui.viewFields fieldPrefix=""}

<div class="col-sm-12 text-right">
	{if MongoCMS::displayEditBtn($m, $element) }
	<a class="btn btn-lg btn-info"
		href="./{MongoCMS::getEditionURL($cName,$element._id)}">{'Edit'|_t}</a>
	{/if}
	<a class="btn btn-lg btn-danger" href="{$returnLocation}">{'Back'|_t}</a>
	<br/>
	<br/>
	<br/>
</div>
<script>
	var MongoCMSFormInfo = {
		'action' : '{$action|lower}',
		'collection' : '{$cName}',
		'id' : '{$element._id}'
	};
</script>
{include file="footer.tpl"}