<div style="width:100%; padding:0; margin:0; background-color: #FFFFFF; font-family: Arial; line-height: 1.2;">
<div style="width:100%; text-align: center;">
	<img src="{Denko::getBaseHREF()}/img/logo-panel.png" style="width:210px;height:61px;margin:10px auto;"/>
	<br/>
	<p style="font-size: 24px; color: rgb(42, 42, 42); ">
		{'Password Recover'|_t|upper}
	</p>
	<p style="font-size: 16px; color: rgb(85, 85, 85);">
		{'Hi %s, we\'re sending this email since you (or someone else) has requested a password reset for your %s user.<br/>If you didn\'t requested a password reset, please ignore this message.'|_t:$user['name']:APP_NAME}
	</p>
</div>
<div style="width:100%; margin:0; background-color:rgb(107,132,92); padding:20px 0;color:#FFFFFF;">
	<div style="width:80%; text-align: left; margin:0 auto;">
		<span style="font-size:18px;">{'To recover your password:'|_t}</span>
		<hr/>
		<br/>
		<span style="font-size:14px; line-height: 1.6; color: white;">
			{'Just click on the following link <a href="%1$s" style="color: #FF9;"><b>RESET PASSWORD</b></a>, and follow the steps to create a new password.<br/>If this link doesn\'t work, you can copy this link and paste it on your browser: <a href="%1$s" style="color: #FF9;">%1$s</a>'|_t:$recoveryURL}
		</span>
	</div>
</div>
<div style="width:80%; text-align: left; margin:20px auto;">
	{'Best regards,'|_t}<br/>
	<b>{APP_NAME}</b><br/>
	<br/>
</div>
<div style="width:100%; margin:0; background-color:#666; padding:10px 0;color:#FFFFFF; text-align:center">
{'PS: This is antomatic email. Please don\'t respond to this message.'|_t}
</div>
</div>