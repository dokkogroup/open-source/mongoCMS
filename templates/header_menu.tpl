<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand hidden-lg hidden-md hidden-sm" href=".">
                {if defined('MOBILE_LOGO')}
					<img class="logo-mobile-header" src="{$smarty.const.MOBILE_LOGO}" />
                {elseif defined('PANEL_LOGO')}
					<img class="logo-mobile-header" src="{$smarty.const.PANEL_LOGO}" />
				{else}
					<img class="logo-mobile-header" src="img{$cachePrefix}/logo-panel.png" />
				{/if}
			</a>
			<div id="nav-icon" class="visible-xs-block">
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="dropdown hidden-xs">
				<button class="dropdown-toggle user-info" type="button" id="user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					<img src="{MongoCMS::getSessionUser('email')|gravatar:35}" width="35" class="img-responsive img-circle user-avatar" alt="">
					<p>{MongoCMS::getSessionUser('name')}</p>
					<p><span>{MongoCMS::getSessionUser('displayRole')}</span></p>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu pull-right" aria-labelledby="user">
                    {if defined('MY_PROFILE_METADATA')}
                    <li><a href="./profile" title="{'My profile'|_t}"><span class="material-icons">person</span> {'My profile'|_t}</a></li>
                    {/if}
                    {include file="header_menu_customizations.tpl"}
                    <li><a href="login?logout" title="{'Logout'|_t}"><span class="material-icons">power_settings_new</span> {'Logout'|_t}</a></li>
				</ul>
			</div>
		</div>
	</div>
</nav>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		$('#nav-icon').click(function(){
			$(this).toggleClass('open');
			$('.sidebar').toggleClass('hidden-xs');
			$('.full-cover').toggleClass('hidden-xs');
			if($(this).hasClass('open')){
				$('body').css('overflow','hidden');
			}else{
				$('body').css('overflow','auto');
			}
		});
	});
</script>