async function deleteElement(id,desc){
	let warningMessage = "{'You can\'t undo this action!'|_t}";
	{if !empty($m.navigation.deleteWarning)}
	try{
		let customMessage = await $.ajax("./{MongoCMS::getDeletionWarningURL($cName,'%%ID%%')}".replace('%%ID%%',id));
		warningMessage = customMessage.text;
	}catch(e){ }
	/*{/if}/* */

	let execute = async function(id) {
		let deletionURL = './{MongoCMS::getDeletionURL($cName,"%%ID%%")}'.replace('%%ID%%',id);
		try{
			let res = await $.ajax({ url: deletionURL, context: document.body });
			if(res.status != 'ok') {
				bootbox.alert('<strong>'+{'Error'|_jst}+':</strong> '+res.message);
				return;
			}
			if (typeof res.redirect !== 'undefined' && res.redirect!=null && res.redirect!='' ) {
				window.location = res.redirect;
			} else if (typeof res.reload !== 'undefined' && res.reload==true ) {
				window.location.reload();
			} else {
				{if isset($m.ui.listingOptions.dataTables) }
				$('#elementsTable').dataTable().fnDeleteRow( document.getElementById( 'row-'+id ) );
				{else}
				$('#row-'+id).hide();
				/*{/if}/* */
			}
		}catch(e){
			bootbox.alert('<strong>'+{'Error'|_jst}+':</strong> '+ {_jst('Response is not a valid JSON.')});
		}
	};

	bootbox.confirm({
		message: "<b>{'Delete %s'|_t:$m.ui.label} \""+desc+"\"?</b><br/>"+warningMessage,
		callback: (res)=>{ if(res) execute(id) },
		buttons: {
			confirm: { label: '{'OK'|_t}' },
			cancel: { label: '{'Cancel'|_t}' }
    	}
	});
}

function restoreElement(id,desc){
	let execute = async function (id){
		var restaurationURL = './{MongoCMS::getRestaurationURL($cName,"%%ID%%")}';
		try{
			let res = await $.ajax({ url: restaurationURL.replace('%%ID%%',id), context: document.body });
			if(res.status != 'ok') {
				bootbox.alert('<strong>'+{'Error'|_jst}+':</strong> '+res.message);
				return;
			}
			if (typeof res.redirect !== 'undefined' && res.redirect!=null && res.redirect!='' ) {
				window.location = res.redirect;
			} else if (typeof res.reload !== 'undefined' && res.reload==true ) {
				window.location.reload();
			} else {
				{if isset($m.ui.listingOptions.dataTables) }
				$('#elementsTable').dataTable().fnDeleteRow( document.getElementById( 'row-'+id ) );
				{else}
				$('#row-'+id).hide();
				/*{/if}/* */
			}
		}catch(e){
			bootbox.alert('<strong>'+{'Error'|_jst}+':</strong> '+ {_jst('Response is not a valid JSON.')});
		}
	}

	bootbox.confirm({
		message: "<b>{'Restore %s'|_t:$m.ui.label} \""+desc+"\"?</b><br/>{'Please confirm you want to restore this element.'|_t}",
		callback: (res) => { if(res) execute(id) },
		buttons: {
			confirm: { label: '{'OK'|_t}' },
			cancel: { label: '{'Cancel'|_t}' }
    	}
	});
}


function switchUser(elem){
	let execute = async function(elem){
		try{
			let res = await $.ajax({ url: elem.href, context: document.body });
			if(res.status != 'ok') {
				bootbox.alert('<strong>'+{'Error'|_jst}+':</strong> '+res.message);
				return;
			}
			if (typeof res.redirect !== 'undefined' && res.redirect!=null && res.redirect!='' ) {
				window.location = res.redirect;
			} else if (typeof res.reload !== 'undefined' && res.reload==true ) {
				window.location.reload();
			} else {
				bootbox.alert({'Something went wrong...'|_jst});
			}
		}catch(e){
			bootbox.alert('<strong>'+{'Error'|_jst}+':</strong> '+ {_jst('Response is not a valid JSON.')});
		}
	}

	bootbox.confirm({
		message: "<b>{'Do you want to become this user?'|_t}",
		callback: res => { if(res) execute(elem) },
		buttons: {
			confirm: { label: '{'OK'|_t}' },
			cancel: { label: '{'Cancel'|_t}' }
		}
	});
	return false;
}

function sendActivationLink(elem){
	let execute = async function(elem) {
		try{
			let res = await $.ajax({ url: elem.href, context: document.body });
			if(res.status != 'ok'){
				bootbox.alert('<strong>'+{'Error'|_jst}+':</strong> '+res.message);
				return;
			}
			if (typeof res.redirect !== 'undefined' && res.redirect!=null && res.redirect!='' ) {
				window.location = res.redirect;
			} else if (typeof res.reload !== 'undefined' && res.reload==true ) {
				window.location.reload();
			} else {
				bootbox.alert({'Activation email sent successfully.'|_jst});
			}
		}catch(e){
			bootbox.alert('<strong>'+{'Error'|_jst}+':</strong> '+ {_jst('Response is not a valid JSON.')});
		}
	}
	bootbox.confirm({
		message: "<b>{'Do you want to send activation link to this user?'|_t}",
		callback: res => { if(res) execute(elem) },
		buttons: {
			confirm: { label: '{'OK'|_t}' },
			cancel: { label: '{'Cancel'|_t}' }
		}
	});
	return false;
}