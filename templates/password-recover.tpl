{include file="header.tpl" title="Password Recover"|_t section="login"}
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="container">
	<form method="post" onSubmit="$('#smtBtn').prop('disabled',true); return true;">
		<div class="text-center">
			<img src="img{$cachePrefix}/logo-panel.png" />
			<h2>{'Password Recover'|_t}</h2>
		</div>
		<label for="inputEmail" class="sr-only">{'EMail'|_t}</label>
		<input type="email" id="inputEmail" class="form-control" placeholder="{'EMail'|_t}" name="email" required autofocus>
		<br/>
		{if defined('RECAPTCHA_KEY')}
		<div style="min-height:78px;width: 304px;margin:0 auto;" class="g-recaptcha" data-sitekey="{RECAPTCHA_KEY}"></div>
		<br/>
		{/if}
		{if !empty($error)}<p class="text-danger bg-danger text-center"><b>{'Error'|_t}:</b> {$error}</p>{/if}
		<button class="btn btn-lg btn-primary btn-block" id="smtBtn" type="submit">{'Recover Password'|_t}</button>
	</form>
</div> <!-- /container -->

{include file="footer.tpl"}