<html>
	<head>
		<title>{if !empty($title)}{$title} :: {/if}{$smarty.const.APP_NAME}</title>
		<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		{if $smarty.const.ENABLE_FAVICON|default:false}
		<!-- Favicons -->
		<link rel="apple-touch-icon" sizes="57x57" href="img{$cachePrefix}/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="img{$cachePrefix}/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="img{$cachePrefix}/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="img{$cachePrefix}/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="img{$cachePrefix}/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="img{$cachePrefix}/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="img{$cachePrefix}/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="img{$cachePrefix}/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="img{$cachePrefix}/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="img{$cachePrefix}/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="img{$cachePrefix}/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="img{$cachePrefix}/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="img{$cachePrefix}/favicon/favicon-16x16.png">
		<link rel="manifest" href="img{$cachePrefix}/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="img{$cachePrefix}/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		{/if}
		<!-- Bootstrap core CSS -->
		<link href="css{$cachePrefix}/bootstrap.min.css" rel="stylesheet" />
		<link href="css{$cachePrefix}/dataTables.bootstrap.min.css" rel="stylesheet" />
		<link href="css{$cachePrefix}/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
		<link href="css{$cachePrefix}/bootstrap-select.min.css" rel="stylesheet">
		<link href="css{$cachePrefix}/menu.css" rel="stylesheet">
		<link href="css{$cachePrefix}/mongocms-customizations.css" rel="stylesheet">
		<script src="js{$cachePrefix}/jquery-3.5.1.min.js"></script>
        <script src="js{$cachePrefix}/sortable.min.js"></script>
        <script src="js{$cachePrefix}/fileinput.min.js"></script>
		<script src="js{$cachePrefix}/fileinput_locale_es.js"></script>
		<script src="js{$cachePrefix}/bootbox.min.js"></script>
		<script src="js{$cachePrefix}/jquery.dataTables.min.js"></script>
		<script src="js{$cachePrefix}/jquery.form.min.js"></script>
		<script src="js{$cachePrefix}/jquery.blockUI.js"></script>
		<script src="js{$cachePrefix}/jquery-sortable.js"></script>
		<script src="js{$cachePrefix}/dataTables.bootstrap.min.js"></script>
		<script src="js{$cachePrefix}/bootstrap-select.min.js"></script>
        {if L10N::$currentLang == 'es'}
            <script src="js{$cachePrefix}/bootstrap-select-es_ES.js"></script>
        {/if}
		<script src="js{$cachePrefix}/mongocms-common.js"></script>
		<script src="js{$cachePrefix}/mongocms-customizations.js"></script>
		{include file="header_customizations.tpl"}
	</head>
	<body style="height:100%" {if $section|default:''=='login'}class="login"{/if}>
			<div class="full-cover col-xs-12 hidden-xs hidden-sm hidden-md hidden-lg" onclick="$('#nav-icon').click();"></div>
			{if MongoCMS::getSessionUser()!=null}
				{if (empty($m.navigation.hideHeaderMenu))}
					{include file="header_menu.tpl"}
				{/if}
			{/if}	
			<div class="container-fluid">
			<div class="row">
			{if MongoCMS::getSessionUser()!=null}
				{if (empty($m.navigation.hideHeaderNavbar))}
					<div class="col-sm-3 col-md-2 col-xs-8 hidden-xs sidebar">
						{include file="header_navbar.tpl"}
					</div>
					<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				{/if}
			{else}
				<div class="col-sm-12">
			{/if}
			{* CONTENT STARTS HERE *}