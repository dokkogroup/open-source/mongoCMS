						{* CONTENT ENDS HERE *}
					</div> {* CLOSES col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main *}
				</div> {* CLOSES row *}
			</div> {* CLOSES container-fluid *}
	</body>
	<script src="js{$cachePrefix}/bootstrap.min.js"></script>
 	{if $ajaxBlockUI|default:true}
	<script>
		$(document).ajaxStart(
			function(){
				$.blockUI( { css: { left:'auto',right:'3px', width:'40px', bottom:'3px', top:'auto', padding:'10px'}, message: '<img src="img{$cachePrefix}/loading.gif" />' });
			}
		).ajaxStop($.unblockUI);
	</script>
	{/if}
	{dk_browser_update}
</html>