<a class="logo-header" style="background-image: url('{if defined('PANEL_LOGO')}{$smarty.const.PANEL_LOGO}{else}img/logo-panel.png{/if}')" "href="." ></a>
<ul class="nav nav-sidebar {if defined('HIDE_COPYRIGHT')}nav-sidebar-copyright-hidden{/if}">
	<li class="hidden-sm hidden-md hidden-lg user-info left">
		<a class="user-avatar" href="admin_users_abm.php?action=view" title="{#header_infoPersonal#}"><img src="{MongoCMS::getSessionUser('email')|gravatar:35}" width="35px" class="img-responsive img-circle user-avatar left" alt=""></a>
		<p><a href="admin_users_abm.php?action=view" title="{#header_infoPersonal#}">{MongoCMS::getSessionUser('name')}</a></p>
		<p><span>{MongoCMS::getSessionUser('displayRole')}</span></p>
	</li>
	<li class="hidden-sm hidden-md hidden-lg" style="border-bottom:dotted #666 1px;">&nbsp;</li>

	{if !empty(MongoCMS::$metadata['@groups'])}
		{$groups=MongoCMS::$metadata['@groups']}
	{else}
		{foreach MongoCMS::$metadata as $cName=>$item}
			{if !empty($item.disabled)}{continue}{/if}
			{$groups[]=$cName}
		{/foreach}
	{/if}
	{foreach $groups as $group}
		{if empty($group)}{continue}{/if}
		{if is_array($group)}
			{capture assign="items_group"}
				{$collapse_in=""}
				{$collapsed="collapsed"}
				{foreach $group.members as $cName}
					{$metadata=MongoCMS::$metadata[$cName]}
					{if $section|default:""==$cName}{$collapse_in="in"}{$collapsed=""}{/if}
					{include file="header_navbar_item.tpl" metadata=$metadata cName=$cName}
				{/foreach}
			{/capture}

			<li class="{$class} {$collapsed}" data-toggle="collapse"  data-target="#collapse_{$group.name}">
				{$group_icon=$group.icon|default:''}
				<a href="#">
					{if $group_icon != ''}
						{if strpos($group_icon,'glyphicon')!==false}
							<span class="glyphicon {$group_icon}"></span>
						{elseif strpos($group_icon,'.png')!==false}
							<img class="image-icon" src="./img/{$group_icon}" />
						{else}
							<span class="material-icons">{$group_icon}</span>
						{/if}
					{/if}
					{$group.label} <span class="btn-group-collapse"></span>
				</a>
			</li>
			<ul class="collapse {$collapse_in} nav nav-sidebar-group"  id="collapse_{$group.name}">
				{$items_group}
			</ul>
		{else}
			{$cName=$group}
			{$metadata=MongoCMS::$metadata[$cName]}
			{$class=""}
			{if !empty($metadata.navigation.navbarSeparator)}{$class=$class|cat:" separated"}{/if}
			{include file="header_navbar_item.tpl" metadata=$metadata cName=$cName}
		{/if}

	{/foreach}

	{if defined('MY_PROFILE_METADATA')}
	<li class="hidden-sm hidden-md hidden-lg" style="border-top:dotted #666 1px;">
		<a href="./profile" title="{'My profile'|_t}"><span class="material-icons">person</span> {'My profile'|_t}</a>
	</li>
	{/if}
    {include file="header_navbar_customizations.tpl"}
	<li class="hidden-sm hidden-md hidden-lg">
		<a href="login?logout" title="{'Logout'|_t}"><span class="material-icons">power_settings_new</span> {'Logout'|_t}</a>
	</li>
</ul>
<div class="copyright{if defined('HIDE_COPYRIGHT')} copyright-hidden{/if}">
	<p>MongoCMS</p>
	<p>&copy; {$smarty.now|date_format:"%Y"} - All Rights Reserved</p>
	<p style="font-size: 0.6em">Powered by <a href="http://www.dokkogroup.com.ar" title="Dokko Group" target="_blank">Dokko Group</a></p>
</div>