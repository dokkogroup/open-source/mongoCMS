<html>
	<head>
		<title>{$smarty.const.APP_NAME} - {'Unexpected Error'|_t}</title>
		<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Bootstrap core CSS -->
		<link href="css{$cachePrefix}/bootstrap.min.css" rel="stylesheet" />
		<link href="css{$cachePrefix}/dataTables.bootstrap.min.css" rel="stylesheet" />
		<link href="css{$cachePrefix}/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
		<link href="css{$cachePrefix}/bootstrap-select.min.css" rel="stylesheet">
		<link href="css{$cachePrefix}/menu.css" rel="stylesheet">
		<link href="css{$cachePrefix}/mongocms-customizations.css" rel="stylesheet">
		<script src="js{$cachePrefix}/jquery-3.5.1.min.js"></script>
		<script src="js{$cachePrefix}/fileinput.min.js"></script>
		<script src="js{$cachePrefix}/fileinput_locale_es.js"></script>
		<script src="js{$cachePrefix}/bootbox.min.js"></script>
		<script src="js{$cachePrefix}/jquery.dataTables.min.js"></script>
		<script src="js{$cachePrefix}/dataTables.bootstrap.min.js"></script>
		<script src="js{$cachePrefix}/bootstrap-select.min.js"></script>
		<script src="js{$cachePrefix}/mongocms-common.js"></script>
		<script src="js{$cachePrefix}/mongocms-customizations.js"></script>
	</head>
	<body style="height:100%">
		<div class="col-sm-12 text-center" style="padding-top:0;">
			<img src="img{$cachePrefix}/logo-panel.png" style="max-width: 450px"/>
			<h1>{'Unexpected Error'|_t}</h1>
			<span style="font-size:1.2em">
				<span class="label label-info">{'Error Description:'|_t}</span>
				{$ex->getMessage()}
				<br/>
				<span class="label label-info">{'Error Location:'|_t}</span>
				{$ex->getFile()}
				<b>({$ex->getLine()})</b>
			</span>
		</div>
		{if $showTrace}
			<div class="col-sm-12" style="padding:10px 40px;">
				<h2 class="label-warning" style="padding:0 10px;">{'Stack Trace'|_t}:</h2>
				<div style="padding:0 10px;">
				{foreach $ex->getTrace() as $k => $t}
					<span class="label label-danger">#{$k}:</span> {$t['file']} <b>({$t['line']})</b>
					<br/>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&raquo;&nbsp;{$t['class']|default:''}{$t['type']|default:''}<b>{$t['function']|default:''}</b>
					( {foreach $t['args'] as $pk => $arg}
						{if $pk > 0},{/if}
						{if is_array($arg)}
						<span style="color:#aa4;">[array]</span>
						{elseif is_object($arg)}
						<span style="color:#aaa;">[object]</span>
						{elseif is_numeric($arg)}
						<span style="color:#44a;">{$arg}</span>
						{else}
						"{(string)$arg|truncate:100}"
						{/if}
					{/foreach} )
					<br/>
					<br/>
				{/foreach}
				</div>
			</div>
		{/if}
	</body>
	<script src="js{$cachePrefix}/bootstrap.min.js"></script>
	{dk_browser_update}
</html>