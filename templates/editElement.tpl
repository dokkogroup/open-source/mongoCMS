{include file="header.tpl" title=$action|cat:" %s"|_t:$m.ui.label section=$cName}

<h1 class="mcms-main-title">{$action|cat:' %s'|_t:$m.ui.label}</h1>
<br/>

<form method="post" enctype="multipart/form-data" id="mCMSForm" onSubmit="return onMCMSFormSubmit(this);">
	{$richFields = "["}
	{$fileFields = "[null"}
	{$onDisplay = $m.navigation.onFieldDisplay|default:null}
	{foreach $m.ui.fields as $fieldName=>$field }
		{if empty($field)}{continue}{/if}
		{if $field.onDisplay|default:$onDisplay != null}
			{if !call_user_func_array($field.onDisplay|default:$onDisplay,
				[$fieldName,$field,$element,$cName,$m,'edit'])}
				{continue}
			{/if}
		{/if}
		{$rField = MongoCMS::getFieldInfoByPath($m,$fieldName)}
		{if $rField == null}Missing field {$fieldName}!{/if}
		{$languages=MongoCMS::getFieldLanguages($m, $fieldName)}
		{$required=(isset($rField.nullable) && $rField.nullable===true)?"":"required"}
		{$extraHTMLAttributes=($field['jsEvents']|default:'')|cat:' '|cat:($field['htmlAttributes']|default:'')}
		{if isset($field.autoComplete) && $field.autoComplete === true}
			{$extraHTMLAttributes=$extraHTMLAttributes|cat:' autocomplete="on"'}
		{elseif isset($field.autoComplete) && $field.autoComplete === false}
			{$extraHTMLAttributes=$extraHTMLAttributes|cat:' autocomplete="off"'}
		{/if}
		{if !empty($rField.readonly) && ($action!="New" || !empty($rField.default)) }
			{$readonly="disabled readonly"}
		{else}
			{$readonly="autofocus"}
		{/if}
		{$singleLangFieldName = $fieldName}
		{foreach $languages|default:[null] as $langCode => $langName}
			{$placeHolder=$field.hint|default:$field.label|default:$fieldName}
			{if !empty($langName)}
				{$fieldName=$singleLangFieldName|cat:'.'|cat:$langCode}
				{$placeHolder=$placeHolder|cat:' :: '|cat:$langName}
			{/if}
			{$fieldName=str_replace('.','*',$fieldName)}
			{$element[$fieldName] = MongoCMS::getAttrByPath($element,$fieldName)}
			{if !MongoCMS::evalDisplayCond($element, $field.displayCond|default:null)}
				{continue}
			{/if}

			{if $action=='New' && !empty($rField.default)}
				{$element[$fieldName] = $rField.default}
				{if !empty($rField.defaultFormula)}
					{$element[$fieldName] = $rField.default|MongoCMS_applyModifiers:$rField.defaultFormula:$element}
					{if !empty($rField.readonly)} {$readonly="readonly"} {/if}
				{/if}
			{/if}

			{if !empty($field.editFormat)}
			{$element[$fieldName] = $element[$fieldName]|MongoCMS_applyModifiers:$field.editFormat:$element}
			{/if}
			{if !empty($field.type)}
			{$rField.type=$field.type}
			{/if}

			{if !empty($field.separator)}
			<div class="col-sm-12 mcms-field-separator {$field.class|default:''}">
			<h3 class="text-success">{$field.separator}</h3>
			</div>
			{/if}

			<div class="col-sm-{$field.cols} mcms-field-container {$field.class|default:''}">
			<label for="input{$fieldName}" >{$field.label|default:$fieldName}{if !empty($langName)} :: {$langName}{/if}</label>

			{if isset($field.template) && $field.template!=""}
				{include file="formFields/"|cat:$field.template}
			{else}
				{if $rField.type == 'file'}
					<input style="{$field.style|default:''}" type="file" id="input{$fieldName}" accept="{$field.inputOptions.allowedFileTypes|default:[]|implode:'|'|replace:'pdf':'application/pdf'|replace:'image':'image/*'}" class="form-control" name="{$fieldName}" {if empty($element[$fieldName])}{$extraHTMLAttributes} {$required}{/if} {$readonly}/>
					<input type="hidden" id="input{$fieldName}_delete" name="{$fieldName}_delete" value="0" />
					{capture assign="fileFields"}{$fileFields},{ "id" : "input{$fieldName}", "options" : {$field.inputOptions|default:null|json_encode}, "currentValue" : {MongoCMS::getFile($element[$fieldName]|default:null)|json_encode} , "previewMode" : "{$field.previewMode|default:'file'}" }{/capture}
				{/if}
				{if $rField.type == 'string'}
					<input style="{$field.style|default:''}" type="text" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {$extraHTMLAttributes} {$required} {$readonly}/>
				{/if}
				{if $rField.type == 'password'}
					<input style="{$field.style|default:''}" autocomplete="new-password" type="password" id="input{$fieldName}" class="form-control"
					{if empty($element[$fieldName])}
						placeholder="{$placeHolder}" {$extraHTMLAttributes} {$required}
					{else}
						placeholder="{'enter new password or leave empty to keep the same'|_t}"
					{/if}
					name="{$fieldName}" {$readonly}/>
				{/if}
				{if $rField.type == 'email'}
					<input style="{$field.style|default:''}" type="email" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {$extraHTMLAttributes} {$required} {$readonly}/>
				{/if}
				{if $rField.type == 'int' || $rField.type == 'float'}
					<input style="{$field.style|default:''}" type="number" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$required} {$readonly}/>
				{/if}
				{if $rField.type == 'richtext' || $rField.type == 'text' }
					<textarea style="height:{$field.height|default:200px};{$field.style|default:''}" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" {$extraHTMLAttributes} {$required} {$readonly}>{$element[$fieldName]|escape:html}</textArea>
				{/if}
				{if $rField.type == 'richtext'}
					{capture assign="richFields"}{$richFields}{
						"name":"input{$fieldName}",
						"height":"{$field.height|default:'200px'}",
						"displayMode" : "{$field.displayMode|default:'replace'}",
						"editorOptions":{if !empty($field.editorOptions)}{$field.editorOptions|json_encode}{else}{}{/if}

					},{/capture}
				{/if}
				{if $rField.type == 'bool'}
					{$yesString = "Yes"|_t}
					{$noString = "No"|_t}
					<select class="selectpicker show-tick form-control" style="{$field.style|default:''}"  id="input{$fieldName}" name="{$fieldName}" {$readonly}>
						<option value="true" {if $element[$fieldName]==true}selected{/if}>{$field.trueTxt|default:$yesString}</option>
						<option value="false" {if $element[$fieldName]==false}selected{/if}>{$field.falseTxt|default:$noString}</option>
					</select>
				{/if}
				{if $rField.type == 'date'}
					<input style="{$field.style|default:''}" type="date" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$required} {$readonly}/>
				{/if}
				{if $rField.type == 'time'}
					<input style="{$field.style|default:''}" type="time" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$required} {$readonly}/>
				{/if}
				{if $rField.type == 'datetime'}
					<input style="{$field.style|default:''}" type="datetime-local" id="input{$fieldName}" class="form-control" placeholder="{$placeHolder}" name="{$fieldName}" value="{$element[$fieldName]|escape:html}" {if isset($rField.max)}max="{$rField.max}"{/if} {if isset($rField.min)}min="{$rField.min}"{/if}  {if isset($rField.step)}step="{$rField.step}"{/if} {$extraHTMLAttributes} {$required} {$readonly}/>
				{/if}
				{if $rField.type == 'singleReference'}
					{include file="formFields/singleReference.tpl"}
				{/if}
				{if $rField.type == 'multiReference'}
					{include file="formFields/multiReference.tpl"}
				{/if}
				{if $rField.type == 'sortableReferences'}
					{include file="formFields/sortableReferences.tpl"}
				{/if}
				{if $rField.type == 'stringArray'}
					{include file="formFields/stringArray.tpl"}
				{/if}
				{if $rField.type == 'multi'}
					{include file="formFields/multi.tpl"}
				{/if}
				{if $rField.type|substr:0:3 == 'gps'}
					{include file="formFields/gps.tpl"}
					{$includeLocationPicker=true}
				{/if}
                {if $rField.type == 'files'}
                    {include file="formFields/files.tpl"}
                {/if}
				{if $rField.type == 'table'}
					{include file="formFields/tableView.tpl"}
				{/if}
				{if $rField.type == 'editableTable'}
					{include file="formFields/tableEdit.tpl"}
				{/if}
			{/if}
		</div>
		{/foreach}
	{/foreach}
	<div class="col-sm-12 text-right mcms-buttons-container">
		<div class="col-sm-12 text-center alert alert-danger" id="errorBox" style="display:none;"></div>
		<button class="btn btn-lg btn-primary" type="submit" id="mCMSSaveBtn">{'Save'|_t}</button>
		<a class="btn btn-lg btn-danger" href="{$returnLocation}">{'Back'|_t}</a>
		<br/>
		<br/>
		<br/>
	</div>
</form>

{if $fileFields!='[null'}
{include file="fileinput-themes.tpl"}
<script>
	var fileFields = {$fileFields|cat:"]"};
	fileFields.forEach(function(e){
		if(e==null) return;
		if(e.options==null) e.options = {};
		if(e.currentValue!=null) {
			var file = e.currentValue;
			var showZoom = false;
			var preview = '<span class="file-other-icon"><i class="glyphicon glyphicon-file" style="margin-top:20px;margin-left:10px;"></i></span>';
			var url = 'uploads/'+file._id.$oid+'/'+(file.filename||e.previewMode||"file");
			switch(e.previewMode){
				case 'image':
					preview = url;
					e.options.initialPreviewAsData = true;
					showZoom = true;
					break;
				case 'pdf':
					preview = '<object style="width:100%;height:100%;" data="'+url+'" type="application/pdf"></object>';
					//e.options.initialPreviewAsData = true;
					showZoom = true;
					break;
				case 'mp4':
					preview = '<video style="width:auto;height:100%;"  controls ><source src="'+url+'" ></video>';
					showZoom = true;
					break;
				default: break;
			}

			if(e.options.showDownload === undefined) e.options.showDownload = true;
			if(e.options.showRemove === undefined) e.options.showRemove = true;

			e.options.initialPreview = [ preview ];
			e.options.initialPreviewConfig = [{
				caption: file.filename,
				showDrag: false,
				showDownload: e.options.showDownload,
				showRemove: e.options.showRemove,
				showZoom: showZoom,
				size: file.length,
				url: "allow.html",
				key: 1,
				downloadUrl: url
			}];
			e.options.showRemove = false;
			e.options.showUpload = false;
			e.options.showClose = false;
		}
		if(e.options.showUpload==undefined) e.options.showUpload=false;
		$("[id='"+e.id+"']").fileinput(e.options);
		$("[id='"+e.id+"']").on("filepredelete", function(a) {
			$("[id='"+e.id+"_delete']").val(1);
			return false;
		});
		$("[id='"+e.id+"_delete']").val(0);
	});
</script>
{/if}

{if $richFields!='['}
<script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
<script>
	var richFields = {$richFields|cat:'null]'};
	richFields.forEach(function(e){
		if(e==null) return;
		var options = e.editorOptions;
		if(options.height == null) options.height = e.height;
		if(options.toolbarStartupExpanded == null) options.toolbarStartupExpanded = true;
		if(options.toolbarCanCollapse == null) options.toolbarCanCollapse = true;
		if(options.disableNativeSpellChecker == null) options.disableNativeSpellChecker = false;

		options.toolbarGroups = [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
			{ name: 'editing', groups: [ 'find', 'selection' ] },
			{ name: 'links' },
			{ name: 'insert', options : ['image'] },
			// { name: 'forms' },
			{ name: 'tools' },
			{ name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
			//{ name: 'others' },
			'/',
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
			{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
			{ name: 'styles' },
			{ name: 'colors' },
		];
		options.removeButtons = 'Flash,Save,Preview,NewPage';

		CKEDITOR[e.displayMode]( e.name , options );
	});
</script>
{/if}

<script>
	$('#mCMSForm').ajaxForm(function(res) {
		$('#mCMSSaveBtn').prop('disabled', true);
		var message = '';
		try{
			if(res.status == 'ok'){
				window.location = res.redirect;
				return;
			}
			if(typeof res.message !== 'undefined') message = res.message;
			else message = {_jst('Response is not a valid JSON.')};
		}catch(e){
			message = {_jst('Response is not a valid JSON.')};
		}
		$('#mCMSSaveBtn').prop('disabled', false);
		$('#errorBox').html('<strong>'+{'Error'|_jst}+':</strong> '+message);
		$('#errorBox').show();
	});

	function onMCMSFormSubmit(form){
		$('#errorBox').hide();
		var onSubmit = function(form){
			{$m.ui.onSubmit|default:'return true;'}
		};
		if(!onSubmit(form)) return false;
		if ( typeof(CKEDITOR) !== 'undefined' ) {
			for ( instance in CKEDITOR.instances ){
				CKEDITOR.instances[instance].updateElement();
			}
		}
		return true;
	}

	var MongoCMSFormInfo = {
		'action' : '{$action|lower}',
		'collection' : '{$cName}',
		'id' : '{$element._id|default:""}'
	};

</script>
{if $includeLocationPicker|default:false}
<script type="text/javascript" src='//maps.google.com/maps/api/js?sensor=false&libraries=places&key={constant('GMAPS_KEY')}'></script>
<script src="js{$cachePrefix}/locationpicker.jquery.min.js"></script>
<script>locationPickers.forEach(displayLocationPicker);</script>
{/if}

{include file="footer.tpl"}