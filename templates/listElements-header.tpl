<h1 class="mcms-main-title {if (!empty($m.navigation.hideHeaderMenu))}hideHeaderMenu{/if}">{$m.ui.pluralLabel|capitalize}</h1>

<script>
	{include file="js/list-elements.js"}
</script>

<style>.btn-sm, .btn-md{ margin-bottom: 4px; }</style>
<div class="starter-template">
	<div class="col-xs-12 text-right listing-global-actions">
		{foreach $m.navigation.enableExport|default:[] as $exportMode}
		<a class="btn btn-md btn-success"
			href="./{MongoCMS::getExportingURL($cName,$exportMode)}"><span class="glyphicon glyphicon-download-alt"></span> {'Export to %s'|_t:($exportMode|upper)}</a>
		{/foreach}
		{if !empty($m.navigation.enablePrinting)}
		<a class="btn btn-md btn-success"
			href="./{MongoCMS::getPrintingURL($cName)}" onClick="return printOnIFrame(this);"><span class="glyphicon glyphicon-print"></span> {'Print'|_t}</a>
		{/if}
		{if empty($m.navigation.disableCreate)}
		<a class="btn btn-md btn-success" title="{'New %s'|_t:$m.ui.label}"
			href="./{MongoCMS::getEditionURL($cName,'new')}"><span class="glyphicon glyphicon-plus"></span> {$m.ui.label}</a>
		{/if}
		{if !empty($m.navigation.globalActions) }
		{foreach $m.navigation.globalActions as $action=>$button}
			{if !empty($button.onDisplay) && !call_user_func_array($button.onDisplay,[$action]) }
				{continue}
			{/if}
			{$btnTitle=$button.title|default:$button.label|default:$action}
			{$icon=''}
			{if !empty($button.icon)}
				{if strpos($button.icon, 'glyphicon')!==false}
					{$icon = '<span class="glyphicon '|cat:$button.icon|cat:'"></span>'}
				{elseif strpos($button.icon, '.png')!==false}
					{$icon = '<img class="image-icon" src="./img/'|cat:$button.icon|cat:'" />'}
				{else}
					{$icon = '<span class="material-icons">'|cat:$button.icon|cat:'</span>'}
				{/if}
				{if empty($button.label)}{$button.label=' '}{/if}
			{/if}
			{$addParameters=$button.addParameters|default:false}
			{$parameters=''}
			{if ($addParameters)}
				{$parameters=MongoCMS::getParameters()}
				{if (!empty($parameters))}{$parameters='&'|cat:$parameters}{/if}
			{/if}
			<a class="btn btn-md {$button.class|default:''}" href="{$button.url|default:'#'}?action={$action}&amp;collection={$cName}{$parameters}" {if !empty($button.onClick)}onClick="{$button.onClick}"{/if}
			{if !empty($button.confirmText)}onClick="return confirm('{$button.confirmText|replace:"'":"\\'"}');"{/if}
			title="{$btnTitle}">{$icon} {$button.label|default:$action|capitalize}</a>
		{/foreach}
		{/if}
	</div>
</div>
{if !empty($m.ui.listingOptions.filtersTemplate)}
{include file=$m.ui.listingOptions.filtersTemplate}
{/if}