{include file="header.tpl" title=$m.ui.pluralLabel section=$cName}
{include file="listElements-header.tpl"}

<style>
TD.actionButtons A { margin-right:4px; }
{$colCount=-1}
{foreach $m.ui.listingFields as $label=>$lField}
	{$colCount=$colCount+1}
	{if empty($lField) || empty($lField.style)}{continue}{/if}
	TD.mCMS-tdstyle-{$colCount} {
		{$lField.style}
	}
{/foreach}
</style>

<div class="col-xs-12 table-responsive">
	<table id="elementsTable" class="table table-striped table-bordered" >
	<thead>
		<tr>
			{foreach $m.ui.listingFields as $label=>$lField}
				{if empty($lField)}{continue}{/if}
				<th>{$lField.columnName|default:$label}</th>
			{/foreach}
			{if empty($m.navigation.hideActions)}
			<th class="text-center" style="width:140px;min-width:140px;">
				{$m.navigation.actionsLabel|default:('Actions'|_t)}
			</th>
			{/if}
		</tr>
	</thead>
	<tbody>
	</tbody>
	</table>
	<br/>
	<br/>
</div>

<script>
function addButtons(elem){
	var id = elem['_id'];
	var html = elem.buttons.join('');
	return html;
}

{if isset($m.ui.listingOptions.dataTables) }
$(document).ready(function() {
	{$columnDefs = $m.ui.listingOptions.dataTables.columnDefs|default:[]}
	{if empty($m.navigation.hideActions)}
	{$columnDefs[] = ["orderable"=>false,"className"=>"text-center actionButtons", "targets"=>-1 ]}
	{/if}
	{$colCount=-1}
	{foreach $m.ui.listingFields as $label=>$lField}
		{$colCount = $colCount+1}
		{if empty($lField) || empty($lField.style)}{continue}{/if}
		{$columnDefs[] = ["className"=>"mCMS-tdstyle-{$colCount}", "targets" => $colCount]}
	{/foreach}
	function getSearchFromHash(){
		if(! window.location.hash.includes('&q=')) return '';
		var from = 3+window.location.hash.indexOf('&q=');
		var q = window.location.hash.substr(from);
		return decodeURIComponent(q);
	}

	function getPageFromHash(){
		if(! window.location.hash.indexOf('#p=') === 0) return 0;
		if(! window.location.hash.includes('&l=')) return 0;
		if(! window.location.hash.includes('&q=')) return 0;
		if(! window.location.hash.includes('&o=')) return 0;
		var from = 3;
		var to = window.location.hash.indexOf('&l=');
		return parseInt(window.location.hash.substr(from, to-from));
	}

	var _default_page_len = parseInt({$m.ui.listingOptions.pagination|default:50});
	function getPageLenFromHash(){
		if(! window.location.hash.indexOf('#p=') === 0) return _default_page_len;
		if(! window.location.hash.includes('&l=')) return _default_page_len;
		if(! window.location.hash.includes('&q=')) return _default_page_len;
		if(! window.location.hash.includes('&o=')) return _default_page_len;
		var from = window.location.hash.indexOf('&l=')+3;
		var to = window.location.hash.indexOf('&o=');
		return parseInt(window.location.hash.substr(from, to-from));
	}

	var _default_page_sort = {$m.ui.listingOptions.dataTables.order|default:null|json_encode};
	function getPageSortFromHash(){
		if(! window.location.hash.indexOf('#p=') === 0) return _default_page_sort;
		if(! window.location.hash.includes('&l=')) return _default_page_sort;
		if(! window.location.hash.includes('&q=')) return _default_page_sort;
		if(! window.location.hash.includes('&o=')) return _default_page_sort;
		var from = window.location.hash.indexOf('&o=')+3;
		var to = window.location.hash.indexOf('&q=');
		var aux = window.location.hash.substr(from, to-from).split(',',2);
		return [[parseInt(aux[0]),aux[1]]];
	}

	function generateHash() {
		if(_MCMS_elementsTable==null){
			return '#p=0&l='+_default_page_len+'&o='+_default_page_sort[0]+'&q=';
		}
		return '#p='+_MCMS_elementsTable.page.info().page+
			'&l='+_MCMS_elementsTable.page.len()+
			'&o='+_MCMS_elementsTable.order()[0]+
			'&q='+$('#elementsTable, input[type=search]').val();
	}

	function updateHash(){
		if(_hash_semaphore) return;
		_hash_semaphore = true;
		var hash = generateHash();
		if(hash == _default_hash) hash = '';
		if(hash != window.location.hash){
			if(hash == '') {
				history.pushState({}, document.title, window.location.href.split('#')[0]);
			} else {
				window.location.hash = hash;
			}
		}
		_hash_semaphore = false;
	}

	var _default_hash = generateHash();
	var _hash_semaphore = false;
	var _MCMS_elementsTable = $('#elementsTable').DataTable({
		"columnDefs": {$columnDefs|json_encode},
		"search": {
			"caseInsensitive": true,
			"smart": true,
			"search" : getSearchFromHash()
		},
		"order" : getPageSortFromHash(),
		"pageLength": getPageLenFromHash(),
		"displayStart": getPageFromHash() * getPageLenFromHash(),
		"processing": true,
        "serverSide": true,
        "searching": {$m.ui.listingOptions.dataTables.searching|default:true|json_encode},
		"bLengthChange": {$m.ui.listingOptions.dataTables.showEntriesDropdown|default:true|json_encode},
		"ajax": {
			"url" : {$m.ui.listingOptions.dataTables.ajax.url|json_encode},
			"data" : function(d){ d.mode = 'ajax'; },
			"dataSrc" : function (json){
				if(json.status != null && json.status == 'error'){
					bootbox.alert('<strong>'+{'Error'|_jst}+':</strong> '+json.message);
					return false;
				}
				var data = json.data;
				{if empty($m.navigation.hideActions)}
				for (i=0; i<data.length; i++){
					data[i][data[i]['cols']] = addButtons(data[i]);
				}
				{/if}
				return data;
			}
		},
		{if L10N::$currentLang == 'es'}
		"language": { "url": "js{$cachePrefix}/dataTables.spanish.json" }
		{/if}
	});

	$(window).on('hashchange', function(e){
		if(_hash_semaphore) return;
		_hash_semaphore = true;
		var search = getSearchFromHash();
		var len = getPageLenFromHash();
		var page = getPageFromHash();
		var order = getPageSortFromHash();
		var chages = 0;

		var cOrder = _MCMS_elementsTable.order();
		if(cOrder[0][0]!=order[0][0] || cOrder[0][1]!=order[0][1]){
			_MCMS_elementsTable.order(order); chages++;
		}
		if($('#elementsTable, input[type=search]').val() != search){
			$('#elementsTable, input[type=search]').val(search);
			_MCMS_elementsTable.search(search); chages++;
		}
		if(_MCMS_elementsTable.page.len() != len){
			_MCMS_elementsTable.page.len(len); chages++;
		}
		if(_MCMS_elementsTable.page.info().page != page){
			_MCMS_elementsTable.page(page).draw('page'); chages++;
		}
		if(chages>0) _MCMS_elementsTable.draw();
		_hash_semaphore = false;
	});

	var _MCMS_elementsTable_serachInited = false;
	_MCMS_elementsTable.on('page',updateHash);
	_MCMS_elementsTable.on('length',updateHash);
	_MCMS_elementsTable.on('order',updateHash);
	_MCMS_elementsTable.on('search',function(){
		if(_MCMS_elementsTable_serachInited) return;
		_MCMS_elementsTable_serachInited = true;
		$('#elementsTable, input[type=search]').on('focusout',(a)=>{
			var prevSearch = getSearchFromHash();
			var curSearch = $('#elementsTable, input[type=search]').val();
			if(prevSearch != curSearch) updateHash()
		});
	});

	updateHash();
} );
{/if}
</script>

{include file="footer.tpl" ajaxBlockUI=$m.navigation.ajaxBlocksUI|default:true}