{include file="header_popup.tpl" title="API Explorer"}
<div class="col-xs-12" style="margin-bottom: 20px;">
	<h1>{$smarty.const.APP_NAME} :: API Explorer</h1>
</div>

<div class="col-xs-12 table-responsive">
	<table id="elementsTable" class="table table-striped table-bordered" >
	<thead>
		<tr>
			<th>Method</th>
			<th>Endpoint</th>
			<th>Params</th>
			<th  class="text-center" style="width:140px;min-width:140px;">{'Actions'|_t}</th>
		</tr>
	</thead>
	<tbody>
		{foreach $endpoints as $endpointID => $endpoint }
		<tr>
			<td class="{if $endpoint.method=='GET'}text-info{else}text-danger{/if}"><b>{$endpoint.method}</b></td>
			<td>
				{$endpoint.endpoint}
				{if !empty($endpoint.comment)}
					<br/>
					<span style="color:#999;">{$endpoint.comment}</span>
				{/if}
			</td>
			<td>
				{if $endpoint.sampleJson!==false}
					<b style="color:#250;">Json Payload</b>
					<ul style="list-style-position: inside;">
						{foreach $endpoint.jsonDoc as $field => $value}
						<li><span class="text-info">{$field}</span>: {$value.type}</li>
						{/foreach}
					</ul>
				{/if}
				{if $endpoint.sampleParams!==false}
					<b style="color:#ab9832;">HTTP Params</b><br/>
					<span class="text-info">Example:</span>
					{$endpoint.sampleParams}
				{/if}
				{if $endpoint.sampleParams===$endpoint.sampleJson}
					<b style="color:#999;">No parameters</b>
				{/if}
			</td>
			<td class="text-center">
				<a style="margin-top:3px;" target="_blank" class="btn btn-warning" href="{$endpoint.url}">{'Demo'|_t}</a>
				{if $endpoint.sampleJson!==false}
				<a style="margin-top:3px;" target="_blank" class="btn btn-danger" href="{$endpoint.url}&debug">{'Debug'|_t}</a>
				<a style="margin-top:3px;" class="btn btn-success" href="#" onclick="return sampleJson({$endpointID});">{'Json Sample'|_t}</a>
				{/if}
			</td>
		</tr>
		{/foreach}
	</tbody>
	</table>
	<br/>
	<br/>
</div>

<style>
   div.modal-dialog { width: 80% !important; }
</style>

<script>
$(document).ready(function() {
	$('#elementsTable').DataTable({
		columnDefs: { orderable: false, targets: -2 },
		pageLength: 50,
		searching: true,
		{if L10N::$currentLang == 'es'}
		language: { url: "js{$cachePrefix}/dataTables.spanish.json" }
		{/if}
	});
} );

var endpoints = {$endpoints|json_encode};
function sampleJson(endpointID){
	bootbox.dialog({
		title: 'Json Sample: '+endpoints[endpointID].endpoint,
		message: '<pre>'+JSON.stringify(JSON.parse(endpoints[endpointID].sampleJson),null,4)+'</pre>',
		class: 'bootbox-dialog-80'
	});
	return false;
}
</script>
{include file="footer_popup.tpl"}