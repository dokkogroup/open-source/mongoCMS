{include file="header.tpl" title="Login" section="login"}

<div class="container">
	<form method="post" action="login{if !empty($smarty.get.r)}?r={$smarty.get.r|urlencode}{/if}">
		<div class="text-center">
            {if defined('LOGIN_LOGO')}
				<img src="{$smarty.const.LOGIN_LOGO}" style="max-width: 80%;"/>
            {elseif defined('PANEL_LOGO')}
				<img src="{$smarty.const.PANEL_LOGO}" style="max-width: 80%;"/>
            {else}
				<img src="img{$cachePrefix}/logo-panel.png" style="max-width: 80%;"/>
            {/if}
			<h2>{'Login'|_t}</h2>
		</div>
		<label for="inputEmail" class="sr-only">{'EMail'|_t}</label>
		<input type="email" id="inputEmail" class="form-control" placeholder="{'EMail'|_t}" name="email" required autofocus>
		<label for="inputPassword" class="sr-only">{'Password'|_t}</label>
		<input type="password" id="inputPassword" class="form-control" placeholder="{'Password'|_t}" name="password" required>
		<br/>
		{if !empty($error)}<p class="text-danger bg-danger text-center"><b>{'Error'|_t}:</b> {$error}</p>{/if}
		{if isset($smarty.get.updated)}<p class="text-success bg-success text-center"><b>{'Your password has been updated'|_t}</b></p>{/if}
		{if isset($smarty.get.mailSent)}<p class="text-warning bg-warning text-center"><b>{'A message has been sent to your email address with instructions to reset your password'|_t}</b></p>{/if}
		<button class="btn btn-lg btn-primary btn-block" type="submit">{'Login'|_t}</button>
	</form>
	{if defined('ALLOW_PASSWORD_RECOVER')}
	<div class="text-right" style="margin-top:5px;">
		{'Forgot your password?'|_t} [<a href="login?recover">{'Password Recover'|_t|lower}</a>]
	</div>
	{/if}
	{if defined('ALLOW_USER_REGISTRATION')}
	<div class="text-right" style="margin-top:5px;">
		{'Don\'t have an account?'|_t} [<a href="register">{'Create new account'|_t|lower}</a>]
	</div>
	{/if}

	{if defined('DOKKO_LOGIN_APP_NAME')}
	<br/>
	<a href="dokko-login" class="btn btn-lg btn-danger btn-block">{'Login with DokkoGroup'|_t}</a>
	{/if}
</div> <!-- /container -->

{include file="footer.tpl"}