# Initial setup:

1. Copy files in base directores to your project root.
2. Edit MONGODB.php file (in your project root).
3. Edit common.php file (in your project admin folder -change project name and sessionv ar as you wish-).
4. Create your metadata files and folders on your project root folder: -you can use the metadata-samples folder-)

# License

The MIT License (MIT) - [LICENSE.md](LICENSE.md)

Copyright &copy; 2017 DokkoGroup (http://www.dokkogroup.com)
