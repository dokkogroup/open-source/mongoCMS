<?php

class MongoCMS {

	///////////////////////////////////////////////////////////////////////////////

	public static $metadata = null;
	public static $db = null;
	public static $collections = [];
	private static $metadataFile = null;
	private static $fieldOptionsCache = [];

	///////////////////////////////////////////////////////////////////////////////

	public static function ID($id){
		return MongoID($id);
	}

	///////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////

	public static function getByID($collection, $id, $returnSoftDeleted=true){
		if($id=='new') return null;
		$softDeleteField = null;
		if(is_string($collection)) {
			if(!$returnSoftDeleted){
				$cMetadata = self::getCollectionMetadata($collection);
				if(!empty($cMetadata['navigation']['softDeleteField'])){
					$softDeleteField = $cMetadata['navigation']['softDeleteField'];
				}
			}
			$collection = self::getCollection($collection);
		}
		$res = $collection->findOne(['_id'=>self::ID($id)]);
		if($softDeleteField!=null && !empty($res[$softDeleteField])){
			throw new Exception(_t('The requested element is marked as deleted.'), 1);
		}
		return $res;
	}

	public static function getByCustomField($collection,$customField,$value,$customFieldType='string',$returnSoftDeleted=true){
		$softDeleteField = null;
		if(is_string($collection)) {
			if(!$returnSoftDeleted){
				$cMetadata = self::getCollectionMetadata($collection);
				if(!empty($cMetadata['navigation']['softDeleteField'])){
					$softDeleteField = $cMetadata['navigation']['softDeleteField'];
				}
			}
			$collection = self::getCollection($collection);
		}
		$res = $collection->findOne([$customField=>self::castType($value,$customFieldType)]);
		if($softDeleteField!=null && !empty($res[$softDeleteField])){
			throw new Exception(_t('The requested element is marked as deleted.'), 1);
		}
		return $res;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function removeByID($collection, $id){
		if(is_string($collection)) $collection = self::getCollection($collection);
		return $collection->deleteOne(['_id'=>self::ID($id)]);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function updateByID($collection, $id, $data){
		if(is_string($collection)) $collection = self::getCollection($collection);
		return $collection->updateOne(['_id'=>self::ID($id)], ['$set'=>$data]);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function setDatabase($mongoDB){
		self::$db = $mongoDB;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function loadMetadata($file, $profileField = 'role') {
		// Check if there's a session opened
		if(!defined('DISABLE_SESSION') && !empty($_COOKIE[session_name()])){
			Denko::sessionStart();
			// Check if there's a metadata file for the user Logged IN
			if(!empty($_SESSION[SESSION_VAR]['role']) && is_string($_SESSION[SESSION_VAR]['role'])){
				$customMetadataFile = substr($file,0,-5).'-'.$_SESSION[SESSION_VAR][$profileField].'.json';
				if(file_exists($customMetadataFile)) $file = $customMetadataFile;
			}
		}
		// Load the metadata
		@$json = file_get_contents($file);
		if(empty($json)) throw new Exception(_t('Couldn\'t read metadata file: %s',$file), 1);
		@$json=json_decode($json,true);
		if($json == null)	throw new Exception(_t('Couldn\'t parse metadata file %s: %s',$file,json_last_error_msg()), 1);
		
		$lang=!empty(L10N::$currentLang)?'.'.L10N::$currentLang:'';
		$langFile=substr($file,0,-5).$lang.'.json';
		if (!empty($lang) && file_exists($langFile)){
			@$langJson = file_get_contents( $langFile);
			if(empty($langJson)) throw new Exception(_t('Couldn\'t read metadata file: %s',$langFile), 1);
			@$langJson=json_decode($langJson,true);
			if($langJson == null)	throw new Exception(_t('Couldn\'t parse metadata file %s: %s',$langFile,json_last_error_msg()), 1);
			$json=array_replace_recursive($json,$langJson);
		}
		
		self::$metadata = $json;
		
		self::$metadataFile = $file;

		if(defined('DISABLE_SESSION') || empty($_COOKIE[session_name()]) || empty($_SESSION[SESSION_VAR])) return;

		$myRoles = isset($_SESSION[SESSION_VAR]['role'])?$_SESSION[SESSION_VAR]['role']:null;
		if(is_string($myRoles)) $myRoles = [ $myRoles ];
		foreach (self::$metadata as $key => $value) {
			if(isset($value['enabledFor'])) {
				$disabled = true;
				foreach($myRoles as $role){
					if(!in_array($role, $value['enabledFor'])) continue;
					$disabled = false;
					break;
				}
				self::$metadata[$key]['disabled'] = $disabled;
			}
			if(empty(self::$metadata[$key]['disabled'])){
				self::$metadata[$key]['disabled'] = false;
			}
			self::$metadata[$key]['disabled'] = self::evalDisplayCond($_SESSION[SESSION_VAR], self::$metadata[$key]['disabled']);
		}
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getAttrByPath($object, $path){
		$path = str_replace('*', '.', $path);
		$aux = explode('.',$path);
		foreach($aux as $attr){
			if( $attr === '' || $attr === null ) continue;
			if(!isset($object[$attr])) return null;
			$object = $object[$attr];
		}
		return $object;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getFieldInfoByPath($metadata, $path){
		$path = str_replace('*', '.', $path);
		$path = str_replace('?', '*', $path);
		$path=preg_replace('/\.\d+\./i','.*.' , $path);
		if(isset($metadata['fields'][$path])){
			return $metadata['fields'][$path];
		}
		$aux = explode('.',$path);
		while(!empty($aux)){
			$top = array_pop($aux);
			array_push($aux, '*');
			$path = implode('.', $aux);
			if(isset($metadata['fields'][$path])){
				return $metadata['fields'][$path];
			}
			array_pop($aux);
			$path = implode('.', $aux);
			if(isset($metadata['fields'][$path]) && !empty($metadata['fields'][$path]['multilang'])){
				$langs = self::getFieldLanguages($metadata, $path);
				if(!empty($langs[$top])) return $metadata['fields'][$path];
			}
		}
		return null;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getFieldLanguages($metadata, $field){
		$info = self::getFieldInfoByPath($metadata, $field);
		if(empty($info['multilang'])) return null;
		if(is_array($info['multilang'])) return $info['multilang'];
		return MULTILANG_CONTENT;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getFieldOptions($cName, $fieldName, $translations=null){
		$fieldName=preg_replace('/\*\%_ROW_NUMBER_\%\*/i','*?*' , $fieldName);
		$cacheKey = $cName.'::'.$fieldName;

		if(!isset(self::$fieldOptionsCache[$cacheKey])){
			$field = self::getFieldInfoByPath(self::getCollectionMetadata($cName,true),$fieldName);
			if($field['type']!='multi') {
				throw new Exception(_t('The field %s on collection %s is not multi options',$fieldName, $cName), 1);
			}
			if(!empty($field['optionsCallback'])){
				$call = explode('::',$field['optionsCallback'],2);
				$options = (count($call)==1)?$call[0]():$call[0]::{$call[1]}();
			}elseif(isset($field['options'])){
				$options = $field['options'];
			}else{
				throw new Exception("The realField has no options", 1);
			}
			if(count($options)>0 && array_keys($options) === range(0, count($options) - 1)){
				$newOptions = [];
				foreach($options as $opt) $newOptions[$opt] = $opt;
				$options = $newOptions;
			}
			self::$fieldOptionsCache[$cacheKey] = $options;
		}else{
			$options = self::$fieldOptionsCache[$cacheKey];
		}

		// APPLY TRANSLATIONS
		if(isset($translations)) {
			foreach($translations as $k => $v ){
				if(isset($options[$k])) $options[$k] = $v;
			}
		}

		return $options;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getCollectionRealName($name){
		if(!isset(self::$metadata[$name]['collection'])) return $name;
		return self::$metadata[$name]['collection'];
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getCollection($name) {
		if(self::$db == null) throw new Exception(_t('Can\'t get collection if you didn\'t set the database. Call MongoCMS::setDatabase(...) first!'), 1);
		$name = self::getCollectionRealName($name);
		if(!isset(self::$collections[$name])) self::$collections[$name] = self::$db->$name;
		return self::$collections[$name];
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getCollectionFields($name, $allowDisabled = false) {
		if(self::$metadata == null) throw new Exception(_t('Can\'t get collection fields if you didn\'t set the metadata. Call MongoCMS::loadMetadata(...) first!'), 1);
		$metadata = self::getCollectionMetadata($name, $allowDisabled);
		if(!isset($metadata['fields'])) throw new Exception(_t('Can\'t find metadata for collection %s',$name), 1);
		return $metadata['fields'];
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function evalDisplayCond($element, $cond) {
		if(is_bool($cond)) return $cond;
		if(empty($cond)) return true;
		foreach($cond as $op => $params) {
			if($op == 'or'){
				$res = false;
				foreach($params as $cond){
					if($res) continue;
					$res = $res || self::evalDisplayCond($element, $cond);
				}
				if(!$res) return false;
				continue;
			}

			if( $op == 'and' ){
				foreach($params as $cond){
					if(!self::evalDisplayCond($element, $cond)) return false;
				}
				continue;
			}

			if( $op == 'not' ){
				return !self::evalDisplayCond($element, $params);
			}

			if( $op == 'empty' ){
				$attrVal = self::getAttrByPath($element, $params);
				return empty($attrVal) || count($attrVal)==0;
			}

			foreach($params as $k => $v) {
				$attrVal = self::getAttrByPath($element, $k);
				switch ($op) {
					case 'eq':
						if($attrVal != $v) return false;
						break;
					case 'ne':
						if($attrVal == $v) return false;
						break;
					case 'gt':
						if($attrVal <= $v) return false;
						break;
					case 'gte':
						if($attrVal < $v) return false;
						break;
					case 'lt':
						if($attrVal >= $v) return false;
						break;
					case 'lte':
						if($attrVal > $v) return false;
						break;
					case 'in':
						if(in_array($attrVal, $v)) return false;
						break;
					case 'nin':
						if(!in_array($attrVal, $v)) return false;
						break;
					default: break;
				}
			}
		}
		return true;
	}

	///////////////////////////////////////////////////////////////////////////////

	private static function loadCollectionMetadata(&$metadata, $fromFile, $name=null){
		if(empty($metadata['file'])) return;
		$path = $metadata['file'];
		unset($metadata['file']);
		$lang=!empty(L10N::$currentLang)?'.'.L10N::$currentLang:'';
		$langPath='';
		if($path == '@' && $name == null){
			throw new Exception(_t('You can\'t use @ as value for "file" in %s. Only main metadata file can use @ as "file" value.', $fromFile), 1);
		}
		if($path == '@'){
			$path = dirname($fromFile).'/mongoCMS-metadata/'.$name.'.json';
			$langPath = dirname($fromFile).'/mongoCMS-metadata/'.$name.$lang.'.json';
		}else{
			$path = dirname($fromFile).'/'.$path;
			$langPath =  dirname($fromFile).'/'.substr($path,0,-5).$lang.'.json';
		}
		if(!file_exists($path)){
			throw new Exception(_t('Can\'t find metadata file (%s) for collection %s',$path,$name?:''), 1);
		}
		$json = null;
		$json = json_decode(file_get_contents($path),true);
		if ( $json === null ){
			throw new Exception(_t('Couldn\'t parse metadata file %s: %s', $path, json_last_error_msg()), 1);
		}
		if(!empty($lang) && file_exists($langPath)){
			$langJson = json_decode(file_get_contents($langPath),true);
			if ( $langJson === null ){
				throw new Exception(_t('Couldn\'t parse metadata file %s: %s', $langPath, json_last_error_msg()), 1);
			}
			$json = array_replace_recursive($json, $langJson);
		}
		$metadata = array_replace_recursive($json, $metadata);
		self::loadCollectionMetadata($metadata, $path, null);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function ensureArray(&$data){
		if($data instanceof MongoDB\Model\BSONArray) return $data;
		if(!is_array($data)) $data = [$data];
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getCollectionMetadata($name, $allowDisabled=false) {
		if(self::$metadata == null) throw new Exception(_t('Can\'t get collection fields if you didn\'t set the metadata. Call MongoCMS::loadMetadata(...) first!'), 1);
		if(!isset(self::$metadata[$name])) throw new Exception(_t('Can\'t find metadata for collection %s', $name), 1);
		if(!$allowDisabled && !empty(self::$metadata[$name]['disabled'])) throw new Exception(_t('Collection %s is disabled in metadata file', $name), 1);

		if(empty(self::$metadata[$name]['MongoCMS::Initialized'])){
			self::loadCollectionMetadata(self::$metadata[$name], self::$metadataFile, $name);
			if(!empty(self::$metadata[$name]['navigation']['listFilters'])){
				self::processMetadataFilters(self::$metadata[$name]['navigation']['listFilters']);
			}
			if(!empty(self::$metadata[$name]['ui']['fields'])) {
				self::processMetadataFilters(self::$metadata[$name]['ui']['fields']);
			}
			if(!empty(self::$metadata[$name]['fields'])) {
				self::processMetadataFilters(self::$metadata[$name]['fields']);
			}

			if(isset(self::$metadata[$name]['ui']['listingOptions']['descriptiveText'])){
				self::ensureArray(self::$metadata[$name]['ui']['listingOptions']['descriptiveText']);
			}
			if(isset(self::$metadata[$name]['ui']['listingFields'])){
				foreach(self::$metadata[$name]['ui']['listingFields'] as $k=>$v){
					if(isset($v['fields']) && !is_array($v['fields'])){
						self::$metadata[$name]['ui']['listingFields'][$k]['fields'] = [ $v['fields'] => null ];
					}
				}
			}
			if(isset(self::$metadata[$name]['ui']['exportingFields'])){
				foreach(self::$metadata[$name]['ui']['exportingFields'] as $k=>$v){
					if(isset($v['fields']) && !is_array($v['fields'])){
						self::$metadata[$name]['ui']['exportingFields'][$k]['fields'] = [ $v['fields'] => null ];
					}
				}
			}
			self::$metadata[$name]['MongoCMS::Initialized'] = true;
		}

		return self::$metadata[$name];
	}

	///////////////////////////////////////////////////////////////////////////////

	private static function castType($var, $type){
		switch ($type) {
			case 'id': return self::ID($var);
			case 'array': return (array)$var;
			case 'int': return (integer)$var;
			case 'integer': return (integer)$var;
			case 'float': return (float)$var;
			case 'string': return (string)$var;
			case 'bool': return (boolean)$var;
			default: return $var;
		}
	}

	///////////////////////////////////////////////////////////////////////////////

	private static function processMetadataFilters(&$f){
		if(is_array($f)){
			foreach($f as $k => $v) self::processMetadataFilters($f[$k]);
			return;
		}
		if(!is_string($f)) return;
		$parts=explode('.', $f, 2);
		$castTo = null;
		if(substr($parts[0],0,1)=='('){
			$auxCast = explode(')', $parts[0], 2);
			$parts[0] = $auxCast[1];
			$castTo = substr($auxCast[0], 1);
		}

		switch ($castTo) {
			case 'id': $defaultRet = null; break;
			case 'array': $defaultRet = []; break;
			case 'int': $defaultRet = 0; $castTo='integer'; break;
			case 'float': $defaultRet = 0.0; break;
			case 'string': $defaultRet = ''; break;
			case 'bool': $defaultRet = false; $castTo='boolean'; break;
			default: $defaultRet = null; break;
		}

		switch ($parts[0]) {
			case '$session':
				if(!isset($_SESSION[SESSION_VAR][$parts[1]])) $f = $defaultRet;
				else $f = self::castType($_SESSION[SESSION_VAR][$parts[1]], $castTo);
				break;
			case '#$get':
				if(empty($_GET[$parts[1]])) {
					$f = ['$exists'=>true];
					break;
				}
			case '@$get':
				if(isset($_GET[$parts[1]]) && $_GET[$parts[1]]=='.') {
					$f = ['$exists'=>true];
					break;
				}
				if(isset($_GET[$parts[1]]) && $_GET[$parts[1]]=='!') {
					$f = ['$exists'=>false];
					break;
				}
				if(isset($_GET[$parts[1]]) && $_GET[$parts[1]]=='-') {
					$f = $defaultRet;
					break;
				}
			case '$get':
				if(!isset($_GET[$parts[1]])) $f = $defaultRet;
				else $f = self::castType($_GET[$parts[1]], $castTo);
				break;
			// THIS CAN BE DEPRECATED SOON IN FAVOR OF (id)$get...
			case '@ID:$get':
				if(isset($_GET[$parts[1]]) && $_GET[$parts[1]]=='.') {
					$f = ['$exists'=>true];
					break;
				}
				if(isset($_GET[$parts[1]]) && $_GET[$parts[1]]=='!') {
					$f = ['$exists'=>false];
					break;
				}
			case 'ID:$get':
				if(!isset($_GET[$parts[1]]) || $_GET[$parts[1]]=='-') {
					$f = $defaultRet;
					break;
				}
				$f = self::castType($_GET[$parts[1]],'id');
				break;
			default:
				break;
		}
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getTextFromDescriptionFields($element, $descFields, $collectionName, $key=null){
		$cMetadata = self::getCollectionMetadata($collectionName, true);
		$desc = '';
		if(is_string($descFields)){
			$descFields = [$descFields => null];
		}
		foreach($descFields as $k=>$field){
			if(is_string($k)){
				if($k=='$key') {
					$string = $key;
					$desc .= self::applyModifiers($string,$field,$element);
				} elseif($k[0]!='@') {
					$string = self::getAttrByPath($element,$k);
					$desc .= self::applyModifiers($string,$field,$element);
				}else{
					$k = substr($k,1);
                    if(isset($element[$k])){
                        $string = self::getDescriptiveText($element[$k], $cMetadata['fields'][$k]['collection']);
                        $desc .= self::applyModifiers($string, $field, $element);
                    }
				}
				continue;
			}
			if($desc!='') $desc .= ' - ';
			if($field[0]!='@') {
				$desc .= self::getAttrByPath($element,$field);
			} else {
				$field = substr($field,1);
                if(isset($element[$field])){
                    $desc .= self::getDescriptiveText($element[$field], $cMetadata['fields'][$field]['collection']);
                }
			}
		}
		return $desc;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getDescriptiveText($element, $collectionName) {
		$cMetadata = self::getCollectionMetadata($collectionName, true);
		if(is_string($element) || $element instanceof MongoDB\BSON\ObjectID) {
			$element = self::getById($collectionName, $element);
		}
		if(empty($cMetadata['ui']['listingOptions']['descriptiveText'])) {
			return $element['_id'];
		}

		$descFields = $cMetadata['ui']['listingOptions']['descriptiveText'];

		$desc = self::getTextFromDescriptionFields($element, $descFields, $collectionName);
		$desc = str_replace("'","\'",$desc);
		$desc = str_replace('"',"\'",$desc);
		$desc = str_replace("\n"," ",$desc);
		$desc = str_replace("\r","",$desc);
		$desc = str_replace("\t"," ",$desc);
		return $desc;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function formatField($data, $format, $name, $cName){
		if(!is_array($format)) return $data;
		if(isset($format['trim']) && $format['trim']==true && !is_bool($data)) $data=trim($data);
		if(isset($format['nullable']) && $format['nullable']==true && empty($data) && !is_bool($data) && $data!==0 && $data!=='0') return null;

		if(empty($format['type'])) {
			throw new Exception(_t('Missing data type for field <b>%s</b>',$name), 1);
		}

		switch ($format['type']) {
			case 'int':
				if($data!==0 && $data!=='0' && empty($data)) throw new Exception(_t('Empty data for %s','Integer'), 1);
				return (int)$data;
			case 'float':
			case 'gpsLat':
			case 'gpsLon':
				if($data!==0 && $data!=='0' && empty($data)) throw new Exception(_t('Empty data for %s',$format['type']), 1);
				return (float)$data;
			case 'bool':
				if(is_bool($data)) return $data;
				if(is_numeric($data)) return $data != 0;
				if(is_string($data)){
					$data = strtolower($data);
					return ( $data == "true" || $data == "yes");
				}
				throw new Exception(_t('Invalid data for %s','Bool'), 1);
			case 'multi':
				$validOptions = self::getFieldOptions($cName,$name);
				if(empty($format['multiple'])){
					if (isset($validOptions[$data])){
						if (empty($format['numericKeys']))	return $data;	
						return (int)$data;
					} 
					throw new Exception(_t('Invalid data for %s','Multioptions'), 1);
				}
				
				if(empty($data)) return [];
				if(!is_array($data)) throw new Exception(_t('Invalid data for %s','Multioptions'), 1);
				foreach ($data as $value) {
					if (!isset($validOptions[$value])) throw new Exception(_t('Invalid data for %s','Multioptions'), 1);
				}
				if (empty($format['numericKeys'])) return $data;
				$datacast=[];
				foreach ($data as $value) {
					$datacast[]=(int)$value;
				}
				return $datacast;	
			case 'password':
				return password_hash($data, PASSWORD_BCRYPT);
			case 'singleReference':
				if($data == "null" || empty($data) ) {
					if(isset($format['nullable']) && $format['nullable']===false){
						throw new Exception(_t('Empty data for %s',$name));
					}
					return null;
				}
				if (!empty($format['customField'])){
					 $customFieldType=(!empty($format['customFieldType']))?$format['customFieldType']:'string';
					 return self::castType($data,$customFieldType);
				}
				return self::ID($data);
			case 'multiReference':
				if( empty($data) ) $data = [];
				$aux = [];
				foreach($data as $mongoId) {
					if(empty($mongoId)) continue;
					$aux[] = self::ID($mongoId);
				}
				if( empty($aux) ) {
					if(isset($format['nullable']) && $format['nullable']===false){
						throw new Exception(_t('Empty data for %s',$name));
					}
				}
				return $aux;
			case 'sortableReferences':
				if( empty($data) ) return null;
				$aux = [];
				foreach(json_decode($data,true) as $itemData){
					$itemData['id'] = self::ID($itemData['id']);
					$aux[] = $itemData;
				}
				return $aux;
			case 'stringArray':
				if( empty($data) ) return [];
				$aux = [];
				$arrData = explode("\n", $data);
				foreach($arrData as $str){
					$str = trim($str);
					if(empty($str)) continue;
					$aux[] = $str;
				}
				return $aux;
			case 'email':
				return mb_strtolower($data);
            case 'files':
                $files = json_decode($data, true);
                foreach ($files as $k => $v) $files[$k] = self::ID($v);
                return $files;
			case 'date':
				if( DateTime::createFromFormat("Y-m-d", $data) === false) {
					throw new Exception(_t('Invalid date format on "%s" (please use YYYY-MM-DD)',$data), 1);
				}
				return $data;
			case 'time':
				if( DateTime::createFromFormat("H:i", $data) === false) {
					throw new Exception(_t('Invalid time format on "%s" (please use HH:MM)',$data), 1);
				}
				return $data;
			case 'datetime':
				if( DateTime::createFromFormat("Y-m-d\TH:i", $data) === false) {
					throw new Exception(_t('Invalid date/time format on "%s" (please use YYYY-MM-DDTHH:MM)',$data), 1);
				}
				return $data;
            case 'string':
			case 'text':
			case 'richtext':
			case 'gpsPair':
				return $data;
			case 'gps':
				throw new Exception(_t('GPS field <b>%s</b> should never be formated since it\'s composed. Please check your metadata specification!',$name), 1);
			case 'object': return json_decode($data);
			default:
				throw new Exception(_t('Unknown data type <b>%s</b> on field <b>%s</b>',$format['type'],$name), 1);
		}
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function checkCallbackFunction($cMetadata, $callback){
		if(!empty($cMetadata['navigation'][$callback]) && !is_callable($cMetadata['navigation'][$callback])) {
			throw new Exception(_t('%s function %s doesn\'t exists',$callback,$cMetadata['navigation'][$callback]), 1);
		}
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function executeCallbackFunction($cMetadata, $callback, $params) {
		if(!empty($cMetadata['navigation'][$callback])) {
			return call_user_func_array($cMetadata['navigation'][$callback],$params);
		}
		return null;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function executeOnFilterCallback($callback, &$listFilters, &$options) {
		if(empty($callback)) return;
		if(!is_callable($callback)) {
			throw new Exception(_t('%s function %s doesn\'t exists','onFilter',$callback), 1);
		}
		return call_user_func_array($callback,[&$listFilters, &$options]);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function checkDelete($cName, $sorteable=null, $redirect=null){
		if(!isset($_GET['action']) || $_GET['action']!='delete') return;
		if(defined("SESSION_VAR") &&
			!empty($_SESSION[SESSION_VAR]['_id']) &&
			$_SESSION[SESSION_VAR]['_id']==$_GET['id']){
			throw new Exception(_t('You can\'t delete your own user.'), 1);
		}

		$oldF = null;
		$filesToDelete = [];

		if(!is_string($cName)) throw new Exception("Error: invalid collection name", 1);

		$cMetadata = self::getCollectionMetadata($cName);
		if(!empty($cMetadata['navigation']['disableDelete'])) {
			throw new Exception(_t('Deletion is disabled for collection %s (check disableDelete in metadata file)',$cName), 1);
		}
		if($sorteable === null) {
			$sorteable = isset($cMetadata['hasPosition'])?$cMetadata['hasPosition']:false;
		}
		foreach($cMetadata['fields'] as $fieldName=>$field){
			if(!empty($field['type']) && $field['type']=='file') {
				if($oldF==null) $oldF = self::getById($cName,$_GET['id']);
				if(!empty($oldF[$fieldName])) $filesToDelete[] = $oldF[$fieldName];
			}
		}
		if($redirect == null) $redirect = self::getListingURL($cName);
		$collection = self::getCollection($cName);

		if($sorteable === null) $sorteable = false;
		if($oldF==null) $oldF = self::getById($collection,$_GET['id']);

		$softDeleteField = null;
		if(!empty($cMetadata['navigation']['softDeleteField'])){
			$softDeleteField = $cMetadata['navigation']['softDeleteField'];
		}

		self::checkCallbackFunction($cMetadata,'onDeleted');
		self::checkCallbackFunction($cMetadata,'onDelete');

		self::executeCallbackFunction($cMetadata,'onDelete',[&$oldF]);

		if($softDeleteField == null){
			$res = self::removeById($collection, $_GET['id']);
		}else{
			$res = self::updateById($collection, $_GET['id'],[ $softDeleteField => true ]);
			$filesToDelete = [];
		}
		self::executeCallbackFunction($cMetadata,'onDeleted',[&$oldF, &$res]);

		if($res){
			foreach($filesToDelete as $f) {
				try {
					self::deleteFile($f);
				} catch(MongoDB\GridFS\Exception\FileNotFoundException $e) { }
			}
			if($sorteable){
				$collection->updateMany(
					['order' => ['$gt' => $oldF['order']]],
					['$inc' => ['order'=> -1]]);
			}
			echo json_encode(['status'=>'ok','redirect'=>null]);
		}else{
			echo json_encode(['status'=>'error','message'=>self::$db->lastError()]);
		}
		exit;
	}

	public static function getCollectionMaxPos($cName){
		global $mongoDB;
		$cMetadata = self::getCollectionMetadata($cName);
		if(empty($cMetadata['hasPosition'])) return null;
		if(!empty($cMetadata['navigation']['softDeleteField'])){
			$filter[$softDeleteField] = ['$ne'=>true];
		}

		$collection = self::getCollection($cName);
		$filter = [];
		$max = $collection->findOne(
			$filter,
			[ 'sort'=>['order'=>-1],'limit'=>1]
		);

		$minPosition = self::getMinPosition($cName);

		if($max==null || !isset($max['order'])) return $minPosition-1;
		return $max['order'];
	}


	///////////////////////////////////////////////////////////////////////////////

	public static function checkRestore($cName, $sorteable=null, $redirect=null){
		if(!isset($_GET['action']) || $_GET['action']!='restore') return;

		$oldF = null;
		if(!is_string($cName)) throw new Exception("Error: invalid collection name", 1);

		$cMetadata = self::getCollectionMetadata($cName);

		if(empty($cMetadata['navigation']['softDeleteField'])) {
			throw new Exception(_t("Error: Collection %s doesn't have soft deletion enabled (check softDeleteField in metadata file)",$cName), 1);
		}

		if(!empty($cMetadata['navigation']['disableRestore'])) {
			throw new Exception(_t('Restauration is disabled for collection %s (check disableRestore in metadata file)',$cName), 1);
		}

		if($redirect == null) $redirect = self::getListingURL($cName);
		$collection = self::getCollection($cName);

		if($oldF==null) $oldF = self::getById($collection,$_GET['id']);

		$softDeleteField = $cMetadata['navigation']['softDeleteField'];

		self::checkCallbackFunction($cMetadata,'onRestored');
		self::checkCallbackFunction($cMetadata,'onRestore');

		self::executeCallbackFunction($cMetadata,'onRestore',[&$oldF]);

		$update = [ $softDeleteField => false ];
		$max = self::getCollectionMaxPos($cName);
		if($max !== null) $update['order'] = $max+1;
		$res = self::updateById($collection, $_GET['id'],$update);

		self::executeCallbackFunction($cMetadata,'onRestored',[&$oldF, &$res]);

		if($res){
			echo json_encode(['status'=>'ok','redirect'=>null]);
		}else{
			echo json_encode(['status'=>'error','message'=>self::$db->lastError()]);
		}
		exit;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function joinObjectAttributes(&$f, $arrK, $v){
		if(count($arrK)==0) return;
		$first = array_shift($arrK);
		if(is_numeric($first) && (int)$first == $first) $first = (int)$first;
		if(!is_array($f)) $f = [];
		if(count($arrK)==0) {
			$f[$first] = $v;
			return;
		}
		self::joinObjectAttributes($f[$first], $arrK, $v);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function checkInsertUpdate($collection, $sorteable = null, $redirect = null, $fields = null){
		if(count($_POST)==0) return;
		if(is_string($collection)) {
			$cName = $collection;
			if($fields == null) $fields = self::getCollectionFields($collection);
			$cMetadata = self::getCollectionMetadata($collection);
			if($_GET['id']!='new' && !empty($cMetadata['navigation']['disableEdit'])) {
				throw new Exception(_t('Editing is disabled for collection %s (check disableEdit in metadata file)',$collection), 1);
			}elseif($_GET['id']=='new' && !empty($cMetadata['navigation']['disableCreate'])){
				throw new Exception(_t('Creation has been disabled for %s (check disableCreate in metadata file)',$collection), 1);
			}
			if($sorteable === null) {
				$sorteable = isset($cMetadata['hasPosition'])?$cMetadata['hasPosition']:false;
			}
			if($redirect == null) $redirect = self::getListingURL($collection);
			$collection = self::getCollection($collection);
		}else{
			$cName = $collection->getNamespace();
			$cName = substr($cName,strpos($cName,'.')+1);
		}

		if($fields == null) throw new Exception(_t('There are no fields to insert or update'), 1);
		if($sorteable === null) $sorteable = false;

		$f = [];
		foreach($_POST as $k => $v){
			if(substr($k, -7)=='_delete') continue;
			if($k=='to_delete_tableitems') continue;

			$k = str_replace('*', '.', $k);
			$v = self::getFieldInfoByPath($cMetadata, $k);
			if($v==null) continue;
			
			$fields[$k] = $v;
		}
		foreach($_FILES as $k => $v){
			$k = str_replace('*', '.', $k);
			$v = self::getFieldInfoByPath($cMetadata, $k);
			if($v==null) continue;
			$fields[$k] = $v;
		}
		foreach($fields as $k => $v){
			if(strpos($k, '*')!==false) unset($fields[$k]);
		}
		foreach($fields as $k => $v){
			$reqKey = str_replace('.','*',$k);
			if(isset($v['type']) && $v['type']=='password' && empty($_POST[$reqKey]) && $_GET['id']!='new'){
				$fields[$k]['readonly']=true;
				continue;
			}
			if(isset($v['readonly']) && $v['readonly']==true && $_GET['id']!='new') continue;
			if(isset($v['type']) && $v['type']=='file'){
				if(!isset($_FILES[$reqKey])) $_FILES[$reqKey] = ['error' => 4];
				if(!empty($fields[$k]['readonly']) && $_GET['id']!='new') continue;

				if($_FILES[$reqKey]['error']===0){
					$f[$k] = self::saveFile($_FILES[$reqKey]['tmp_name'], $_FILES[$reqKey]['name'], $_FILES[$reqKey]['type']);
					continue;
				}
				if($_FILES[$reqKey]['error']===4 && $_GET['id']!='new') {
					if(empty($v['nullable']) || $_POST[$reqKey.'_delete']==0){
						$fields[$k]['readonly'] = true;
					}
					continue;
				}
				if($_FILES[$reqKey]['error']===4 && !empty($v['nullable'])) {
					$f[$k] = null;
					continue;
				}
				throw new Exception(_t('Missing file %s',$k), 1);
			}
			if(!isset($_POST[$reqKey])) continue;
			$f[$k] = self::formatField($_POST[$reqKey], $v, $k, $cName);
		}

		if($_GET['id']=='new'){
			if($sorteable){
				$max = self::getCollectionMaxPos($cName);
				$f['order'] = $max + 1;
			}
			foreach($f as $k => $v){
				if(strpos($k, '.')===false) continue;
				$parts = explode('.',$k);
				self::joinObjectAttributes($f, $parts, $v);
				unset($f[$k]);
			}
			foreach($fields as $k => $v){
				if(empty($v['readonly'])) continue;
				if(empty($v['default'])) continue;
				if(isset($v['type']) && $v['type']=='gps') continue;
				if(!empty($v['defaultFormula'])) continue;
				$f[$k]=self::formatField($v['default'], $v, $k, $cName);
			}
			$f['aud_ins_date'] = Denko::curTimestamp();
			$f['aud_upd_date'] = null;

			self::checkCallbackFunction($cMetadata,'onInserted');
			self::checkCallbackFunction($cMetadata,'onInsert');

			self::executeCallbackFunction($cMetadata,'onInsert',[null, &$f]);
			$res = $collection->insertOne($f);
			self::executeCallbackFunction($cMetadata,'onInserted',[null, &$f, &$res]);
		}else{
			$oldF = self::getById($collection,$_GET['id']);;
			if($sorteable) $f['order']=$oldF['order'];

			foreach($fields as $k => $v){
				if($k=='aud_ins_date'){
					$f[$k]=$oldF[$k];
					continue;
				}
				if(empty($v['readonly'])) {
					if($v['type']=='file' && isset($oldF[$k])){
						try {
							self::deleteFile($oldF[$k]);
							if(empty($f[$k])) $f[$k] = null;
						} catch(MongoDB\GridFS\Exception\FileNotFoundException $e) { }
					}
					continue;
				}
				if(isset($f[$k])) unset($f[$k]);
			}

			foreach($f as $k => $v){
				if(strpos($k, '.')===false) continue;
				$parts = explode('.',$k);
				if(isset($f[$parts[0]]) || empty($oldF[$parts[0]])){
					self::joinObjectAttributes($f, $parts, $v);
					unset($f[$k]);
				}
			}

			$f['aud_upd_date'] = Denko::curTimestamp();

			self::checkCallbackFunction($cMetadata,'onUpdated');
			self::checkCallbackFunction($cMetadata,'onUpdate');
			self::executeCallbackFunction($cMetadata,'onUpdate',[&$oldF, &$f]);

			if(is_string($collection)) $collection = self::getCollection($collection);
			$unset=[];
			$pull=[];
			$to_delete=[];
			if (!empty($_POST['to_delete_tableitems'])){
				$to_delete=explode(',',implode(',',$_POST['to_delete_tableitems']));
			}
			foreach($to_delete as $item){
				if (empty($item)) continue;
				$data=explode('|',$item);
				$unset[$data[0].'.'.$data[1]]=1;
				$pull[$data[0]]=null;
			}
			$query['$set']=$f;
			if (count($unset)>0) { 
				$query['$unset']=$unset;
			}
			$res=$collection->updateOne(['_id'=>self::ID($_GET['id'])], $query);
		
			if (count($pull)>0) {
				$collection->updateOne(['_id'=>self::ID($_GET['id'])], ['$pull'=>$pull]);
			}
			self::executeCallbackFunction($cMetadata,'onUpdated',[&$oldF, &$f, &$res]);
		}

		if($res){
			echo json_encode(['status'=>'ok','redirect'=>$redirect]);
		}else{
			echo json_encode(['status'=>'error','message'=>self::$db->lastError()]);
		}
		exit;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getListingURL($cName){
		$cMetadata = self::getCollectionMetadata($cName);
		if(empty($cMetadata['navigation']['listingURL'])) return 'list-'.$cName;
		return $cMetadata['navigation']['listingURL'];
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getExportingURL($cName,$mode){
		$cMetadata = self::getCollectionMetadata($cName);
		$appendParameters = $_GET;
		unset($appendParameters['cn']);
		unset($appendParameters['controller']);
		unset($appendParameters['export']);
		if(empty($cMetadata['navigation']['exportingURL'])) return 'list-'.$cName.'?export='.$mode.'&'.http_build_query($appendParameters);
		return str_replace('%MODE%', $mode, $cMetadata['navigation']['exportingURL']);
	}
	///////////////////////////////////////////////////////////////////////////////

	public static function getPrintingURL($cName){
		$cMetadata = self::getCollectionMetadata($cName);
		$appendParameters = $_GET;
		unset($appendParameters['cn']);
		unset($appendParameters['controller']);
		unset($appendParameters['export']);
		if(empty($cMetadata['navigation']['printingURL'])) return 'list-'.$cName.'?export=print&'.http_build_query($appendParameters);
		return str_replace('%MODE%', $mode, $cMetadata['navigation']['printingURL']);
	}
	///////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////

	public static function getParameters(){
		$appendParameters = $_GET;
		unset($appendParameters['cn']);
		unset($appendParameters['controller']);
		unset($appendParameters['export']);
		return http_build_query($appendParameters);
	}
	///////////////////////////////////////////////////////////////////////////////

	public static function getViewURL($cName, $id){
		$cMetadata = self::getCollectionMetadata($cName);
		if(empty($cMetadata['navigation']['viewURL'])) return 'view-'.$cName.'@'.$id;
		return str_replace('%ID%', $id, $cMetadata['navigation']['viewURL']);
	}

	///////////////////////////////////////////////////////////////////////////////
	public static function getEditionURL($cName, $id){
		$cMetadata = self::getCollectionMetadata($cName);
		if(empty($cMetadata['navigation']['editionURL'])) return 'edit-'.$cName.'@'.$id;
		return str_replace('%ID%', $id, $cMetadata['navigation']['editionURL']);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getDeletionURL($cName, $id){
		$cMetadata = self::getCollectionMetadata($cName);
		if(empty($cMetadata['navigation']['deletionURL'])) return 'delete-'.$cName.'@'.$id;
		return str_replace('%ID%', $id, $cMetadata['navigation']['deletionURL']);
	}
	///////////////////////////////////////////////////////////////////////////////

	public static function getDeletionWarningURL($cName, $id){
		$cMetadata = self::getCollectionMetadata($cName);
		if(empty($cMetadata['navigation']['deletionWarningURL'])) return 'list-'.$cName.'?deleteWarning='.$id.'&mode=ajax';
		return str_replace('%ID%', $id, $cMetadata['navigation']['deletionWarningURL']);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getRestaurationURL($cName, $id){
		$cMetadata = self::getCollectionMetadata($cName);
		if(empty($cMetadata['navigation']['restaurationURL'])) return 'restore-'.$cName.'@'.$id;
		return str_replace('%ID%', $id, $cMetadata['navigation']['restaurationURL']);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function newSmarty() {
		$s = new Smarty();
		$cachePrefix = '';
		if(file_exists('../LAST_PULL')){
			$cachePrefix='-'.trim(file_get_contents('../LAST_PULL'));
		}
		$s->addTemplateDir(__DIR__.'/../templates/');
		$s->addPluginsDir(__DIR__.'/../smarty-plugins/');
		$s->assign('cachePrefix',$cachePrefix);
		return $s;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function dateToUTC($timestamp) {
		return new MongoDB\BSON\UTCDateTime((strtotime($timestamp)-(3*3600))*1000);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getMinPosition($cName) {
		$cMetadata = self::getCollectionMetadata($cName);
		if(isset($cMetadata['minPosition'])){
			return $cMetadata['minPosition'];
		}
		return 1;
	}

	public static function saveFile($file, $name, $contentType, $attributes = null) {
		if(self::$db == null) throw new Exception(_t('Can\'t get bucket if you didn\'t set the database. Call MongoCMS::setDatabase(...) first!'), 1);
		$bucket = self::$db->selectGridFSBucket();
		$fStream = fopen($file,'r');
		if(empty($attributes)) $attributes = [];
		$attributes['contentType'] = $contentType;
		$id = $bucket->uploadFromStream($name, $fStream, $attributes);
		fclose($fStream);
		return $id;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getFile($id) {
		if(self::$db == null) throw new Exception(_t('Can\'t get bucket if you didn\'t set the database. Call MongoCMS::setDatabase(...) first!'), 1);
		if(empty($id)) return null;
		$bucket = self::$db->selectGridFSBucket();
		$f = $bucket->findOne(['_id'=>self::ID($id)]);
		if($f==null) return null;
		return $f;
	}

    ///////////////////////////////////////////////////////////////////////////////

    public static function getFiles($ids) {
        if(self::$db == null) throw new Exception(_t('Can\'t get bucket if you didn\'t set the database. Call MongoCMS::setDatabase(...) first!'), 1);
        if(empty($ids) || count($ids)==0) return null;
        $bucket = self::$db->selectGridFSBucket();
        $result=[];
        foreach ($ids as $id){
            $f = $bucket->findOne(['_id'=>self::ID($id)]);
            if($f==null) continue;
            $result[]=$f;
        }
        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////



	public static function getFileStream($id) {
		if(self::$db == null) throw new Exception(_t('Can\'t get bucket if you didn\'t set the database. Call MongoCMS::setDatabase(...) first!'), 1);
		if(empty($id)) return null;
		$bucket = self::$db->selectGridFSBucket();
		$s = $bucket->openDownloadStream(self::ID($id));
		if($s==null) return null;
		return $s;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function deleteFile($id) {
		if(self::$db == null) throw new Exception(_t('Can\'t get bucket if you didn\'t set the database. Call MongoCMS::setDatabase(...) first!'), 1);
		if(empty($id)) return;
		$bucket = self::$db->selectGridFSBucket();
		$bucket->delete(self::ID($id));
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getSessionUser($attr=null){
		if(!defined("SESSION_VAR") || empty($_SESSION[SESSION_VAR]['_id'])) return null;
		$user = $_SESSION[SESSION_VAR];
		if(empty($attr)) return $user;

		if($attr == 'displayRole') {
			if(defined("ON_USER_ROLE_DISPLAY")){
				$res = call_user_func_array(ON_USER_ROLE_DISPLAY,[$user]);
				if($res!=null) return $res;
			}
			$role = $user['role'];
			if(!is_string($role)){
				$role = (array)$role;
				if(count($role)==1){
					$role=$role[0];
				}else{
					return _t('%s user roles combined',count($role));
				}
			}
			$options = self::getFieldOptions('users','role');
			$cMetadata = self::getCollectionMetadata('users', true);
			if(empty($options[$role])) return $role;
			return $options[$role];
		}
		return $user[$attr];
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function applyModifiers($string, $modifiers, $element=null){
		if(empty($modifiers)) return $string;
		self::ensureArray($modifiers);
		$prefix = '';
		$suffix = '';
		$originalString = $string;

		foreach($modifiers as $modifier){

			// INTERNAL MODIFIERS WITHOUT PARAMETERS
			////////////////////////////////////////

			switch (trim($modifier)) {
				case 'bold': $prefix .= '<b>'; $suffix = '</b>'.$suffix; continue 2;
				case 'italic': $prefix .= '<i>'; $suffix = '</i>'.$suffix; continue 2;
				case 'br': $suffix.='<br/>'; continue 2;
				case '_self': $string = $element; continue 2;
				case 'list':
					if(empty($string)) {
						$string='';
						continue 2;
					}
					$aux = '<ul style="margin-left:20px;">';
					foreach($string as $item){
						$aux.="\n".'<li>'.$item.'</li>';
					}
					$aux.="\n</ul>";
					$string = $aux;
				continue 2;
				default: break;
			}

			// INTERNAL MODIFIERS WITH ONE PARAMETER
			////////////////////////////////////////

			$aux = explode(":",$modifier,2);
			$f = trim($aux[0]);

			switch ($f) {
				case 'settype':
					settype($string,$aux[1]);
					continue 2;
				case 'implode':
					$string = implode($aux[1],(array)$string);
					continue 2;
				case 'emptyCut':
					if(empty($string)) return $aux[1];
					continue 2;
				case 'prefix': $prefix = $aux[1].$prefix; continue 2;
				case 'suffix': $suffix .= $aux[1]; continue 2;
				case 'concat': $string .= $aux[1]; continue 2;
				case 'objectReference':
					if(empty($string)) continue 2;
					$string = self::getById($aux[1], $string);
					continue 2;
				case 'reference':
					if(empty($string)) continue 2;
					$string = self::getDescriptiveText($string, $aux[1]);
					continue 2;
				case 'fileUrl':
					$f = self::getFile($string);
					if($f==null){
						$string = '';
					}else{
						$string = 'uploads/'.$f['_id'].'/'.(empty($f['filename'])?'image':$f['filename']);
					}
					continue 2;
				case 'img':
					if(empty($string)) $string = 'img/placeholder.png';
					$string = '<img src="'.$string.'" '.$aux[1].' />';
					continue 2;
				case 'default': if($string=='') $string=$aux[1]; continue 2;
				case 'format': $string=sprintf($aux[1],$string); continue 2;
				case 'color': $string='<span style="color:'.$aux[1].';">'.$string.'</span>'; continue 2;
				case 'dateFormat':
					$dateParts = explode('.',$string,2);
					if(count($dateParts)==2){
						$aux[1] = str_replace('u', $dateParts[1], $aux[1]);
					}
					$string = date($aux[1],$dateParts[0]);
					continue 2;
				case 'field':
					if(isset($string[$aux[1]])){
						$string=$string[$aux[1]];
					}else{
						$string=null;
					}
					continue 2;
				case 'dump':
					ob_start();
					Denko::print_r($string,5,[],'mongoCMS_dump');
					$string = ob_get_contents();
					ob_end_clean();
					continue 2;
				case 'style': $prefix .= '<span style="'.$aux[1].'">'; $suffix = '</span>'.$suffix; continue 2;
				default: break;
			}

			// INTERNAL MODIFIERS WITH MORE PARAMETERS
			//////////////////////////////////////////

			if(!empty($aux[1])){
				$p = str_replace(':', "\1", $aux[1]);
				$p = str_replace("\\\1", ":", $p);
				$p = explode("\1",$p);
			}else{
				$p = array();
			}
			$pCount = count($p);

			switch ($f) {
				case 'a':
					if($pCount!=2) throw new Exception(_t('Modifier "%s" requires 2 parameters','a'), 1);
					$replaces = array('%%', '@@');
					$replacements = array($originalString, $string);
					$string = '<a href="'.$originalString.'" '
							  .str_replace($replaces, $replacements,$p[1]).' >'
							  .str_replace($replaces, $replacements,$p[0]).'</a>';
					continue 2;
				case 'bool':
					if($pCount!=2) throw new Exception(_t('Modifier "%s" requires 2 parameters','bool'), 1);
					$string = ($string==true)?$p[0]:$p[1];
					continue 2;
				case 'replace':
					if($pCount!=2) throw new Exception(_t('Modifier "%s" requires 2 parameters','replace'), 1);
					$string = str_replace($p[0], $p[1], $string);
					continue 2;
				case 'truncate':
					if($pCount===1) $p[1]='';
					if($pCount!=2) throw new Exception(_t('Modifier "%s" requires 1 or 2 parameters','truncate'), 1);
					if(strlen($string)<=$p[0]) continue 2;
					if(strlen($p[1])>$p[0]){
						$string = mb_substr($p[1], 0, $p[0]);
					} else {
						$string = mb_substr($string, 0, $p[0]-strlen($p[1])).$p[1];
					}
					continue 2;
				case 'multiReference':
					if(empty($string)) continue 2;
					$newString = [];
					foreach($string as $item){
						$newString[]= self::getDescriptiveText($item, $p[0]);
					}
					if(isset($p[1])){
						$string = implode($p[1], $newString);
					}else{
						$string = $newString;
					}
					continue 2;
				case 'linkReference':
					if(empty($string)) continue 2;
					$auxDesc = self::getDescriptiveText($string, $p[0]);
					if($p[1]=='view') $string = self::getViewURL($p[0], $string);
					if($p[1]=='edit') $string = self::getEditionURL($p[0], $string);
					$string = $auxDesc.' - <a href="./'.$string.'">['._t(Denko::capitalize($p[1])).']</a>';
					continue 2;
				case 'ifField':
					if($pCount!=4) throw new Exception(_t('Modifier "%s" requires 4 parameters','ifField'), 1);
					$auxFieldVal = $element[$p[0]];
					$auxVal = $p[2];
					if($p[1]=='==' && $auxVal != $auxFieldVal) continue 2;
					if($p[1]=='!=' && $auxVal == $auxFieldVal) continue 2;
					if($p[1]=='>' && $auxVal <= $auxFieldVal) continue 2;
					if($p[1]=='>=' && $auxVal < $auxFieldVal) continue 2;
					if($p[1]=='<' && $auxVal >= $auxFieldVal) continue 2;
					if($p[1]=='<=' && $auxVal > $auxFieldVal) continue 2;
					$string = $p[3];
					continue 2;
				case 'multiOptions':
					if(empty($string)) continue 2;
					$fieldOptions = self::getFieldOptions($p[0], $p[1]);
					$values = $string;
					self::ensureArray($values);
					$string = '';
					if(empty($p[3])) $p[3] = '';
					if(empty($p[2])) $p[2] = '%s';
					if(empty($p[4])) $p[4] = '<br/>';
					foreach($values as $str){
						if(isset($fieldOptions[$str])) {
							$newString = $fieldOptions[$str];
						} else {
							$newString = $p[3];
						}
						if($string!='') $string.=$p[4];
						$string .= sprintf($p[2],$newString,$str);
					}
					continue 2;
			}

			// EXTERNAL MODIFIERS...
			////////////////////////////////////////
			$f = str_replace('.','::',$f);
			$paramPosition = explode('@',$f);
			$f = array_shift($paramPosition);
			if(empty($paramPosition)) $paramPosition[]=0;
			foreach($paramPosition as $key => $pos) {
				if($pos==='') continue;
				array_splice($p, $pos, 0, [$key==0?$string:$element]);
			}
			if(!is_callable($f)){
				throw new Exception(_t('Function %s doesn\'t exists',$f), 1);
			}
			$string = call_user_func_array($f, $p);
		}
		return $prefix.$string.$suffix;
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function displayEditBtn($cMetadata, $element){
		if(!empty($cMetadata['navigation']['disableEdit'])) return false;
		if(empty($cMetadata['navigation']['onEditBtnDisplay'])) return true;
		return call_user_func_array($cMetadata['navigation']['onEditBtnDisplay'],[$element, 'edit']);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function displayViewBtn($cMetadata, $element){
		if(!empty($cMetadata['navigation']['disableView'])) return false;
		if(empty($cMetadata['navigation']['onViewBtnDisplay'])) return true;
		return call_user_func_array($cMetadata['navigation']['onViewBtnDisplay'],[$element, 'view']);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function displayDeleteBtn($cMetadata, $element){
		if(!empty($cMetadata['navigation']['disableDelete'])) return false;
		if(!empty($cMetadata['navigation']['softDeleteField'])) {
			if(!empty($element[$cMetadata['navigation']['softDeleteField']])) return false;
		}
		if((string)$_SESSION[SESSION_VAR]['_id'] == (string)$element['_id']) return false;
		if(empty($cMetadata['navigation']['onDeleteBtnDisplay'])) return true;
		return call_user_func_array($cMetadata['navigation']['onDeleteBtnDisplay'],[$element, 'delete']);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function displayRestoreBtn($cMetadata, $element){
		if(!empty($cMetadata['navigation']['disableRestore'])) return false;
		if(empty($cMetadata['navigation']['softDeleteField'])) return false;
		if(empty($element[$cMetadata['navigation']['softDeleteField']])) return false;
		if(empty($cMetadata['navigation']['onRestoreBtnDisplay'])) return true;
		return call_user_func_array($cMetadata['navigation']['onRestoreBtnDisplay'],[$element, 'restore']);
	}

	///////////////////////////////////////////////////////////////////////////////

	public static function getReferenceSelectorFilters($cMetadata, $field) {
		$filters = [];
		$options = [];
		if(!empty($field['filter'])) $filters = $field['filter'];
		if(!empty($cMetadata['navigation']['softDeleteField'])){
			$filters[$cMetadata['navigation']['softDeleteField']] = ['$ne'=>true];
		}
		if(!empty($field['sort'])) $options['sort'] = $field['sort'];
		return [ 'filters'=>$filters, 'options'=>$options ];
	}
}