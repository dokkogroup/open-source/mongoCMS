<?php
require_once 'common.php';

///////////////////////////////////////////////////////////////////////////////

function getFileVersion($file){
	$data = file_get_contents($file);
	$aux = explode('#MU::VERSION::', $data,2);
	if(count($aux)!=2) return -1;
	$aux = explode("\n",$aux[1],2);
	$version = trim($aux[0]);
	if(is_numeric($version) && (int)$version == $version) return (int)$version;
	return -1;
}

///////////////////////////////////////////////////////////////////////////////

MongoCMS\checkLogged();

if(file_exists('initDB.php')) {
	throw new Exception("initDB is deprecated. Please update to megaUpdater for MongoDB!", 1);
}

if(file_exists('../megaUpdater/mongoUpdate.php')) {
	$projectVersion = getFileVersion('../megaUpdater/mongoUpdate.php');
	$mongoCMSVersion = getFileVersion('../mongoCMS/base/megaUpdater/mongoUpdate.php');
	if($projectVersion<$mongoCMSVersion){
		throw new Exception("MegaUpdater for MongoCMS has a new version. Please update megaUpdater (mongoUpdate.php file) to continue.", 1);
	}
}

foreach(MongoCMS::$metadata as $k=>$v){
	if(!empty($v['disabled'])) continue;
	Denko::redirect(MongoCMS::getListingURL($k));
	exit;
}
