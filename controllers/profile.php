<?php
if(count($_POST)>0) define('isAjaxRequest',true);
require_once 'common.php';

MongoCMS\checkLogged();

if(!defined('MY_PROFILE_METADATA')){
	throw new Exception(_t('There\'s no profile metadata specification. Please define MY_PROFILE_METADATA to enable my profile section.'), 1);
}

$user = $_SESSION[SESSION_VAR];

if(isset($_GET['reload'])){
	$_SESSION[SESSION_VAR] = $mongoDB->users->findOne(['_id'=>$user['_id']]);
	if(defined('ON_USER_LOGGED_IN')){
		if(!is_callable(ON_USER_LOGGED_IN)) throw new Exception(_t('%s function %s doesn\'t exists','ON_USER_REGISTERED',ON_USER_LOGGED_IN, 1));
		call_user_func_array(ON_USER_LOGGED_IN,[&$user]);
	}
	Denko::redirect('./profile#updated');
	exit;
}

$cName = 'myProfile';
//Denko::print_r(MongoCMS::$metadata); exit;
MongoCMS::$metadata['myProfile'] = [ 'file' => MY_PROFILE_METADATA ];
$cMetadata = MongoCMS::getCollectionMetadata($cName);

$_GET['id'] = (string)$user['_id'];
MongoCMS::checkInsertUpdate($cName);

if(!empty($cMetadata['navigation']['disableEdit'])) {
	throw new Exception(_t('Editing is disabled for collection %s (check disableEdit in metadata file)',$cName), 1);
}

MongoCMS::$metadata['myProfile']['disabled']=true;

$smarty = MongoCMS::newSmarty();
$smarty->assign('element', $user);
$smarty->assign('m',$cMetadata);
$smarty->assign('returnLocation', './');
$smarty->assign('cName', $cName);
$smarty->display('profile.tpl');
