<?php
if(!defined('isAjaxRequest') && !empty($_GET['mode']) && $_GET['mode']=='ajax') define('isAjaxRequest',true);
require_once 'common.php';

MongoCMS\checkLogged();

if(empty($_GET['cn'])){
	echo "fcn"; exit;
}
$cName = $_GET['cn'];
$cMetadata = MongoCMS::getCollectionMetadata($cName);
$exportMode = false;

if(!empty($_GET['deleteWarning'])){
	$element = MongoCMS::getById($cName, $_GET['deleteWarning']);
	$text = MongoCMS::getTextFromDescriptionFields($element, $cMetadata['navigation']['deleteWarning'], $cName);
	echo json_encode(['status'=>'ok', 'text'=>$text]);
	exit;
} else if(!empty($_GET['export']) && $_GET['export']=='csv'){
	$usingAjax = false;
	$exportMode = 'csv';
} else if(!empty($_GET['export']) && $_GET['export']=='xls'){
	$usingAjax = false;
	$exportMode = 'xls';
} else if(!empty($_GET['export']) && $_GET['export']=='print'){
	$usingAjax = false;
	$tpl = 'listElements-print.tpl';
} else if(empty($cMetadata['ui']['listingOptions']['dataTables']['ajax'])){
	$usingAjax = false;
	$tpl = 'listElements.tpl';
}else{
	$usingAjax = true;
	$tpl = 'listElements-ajax.tpl';
}

if(!empty($cMetadata['ui']['listingTemplate'])){
	$tpl = $cMetadata['ui']['listingTemplate'];
}
$listFilters = [];

if(isset($cMetadata['navigation']['listFilters'])){
	$listFilters = $cMetadata['navigation']['listFilters'];
}

if(!empty($cMetadata['navigation']['softDeleteField'])){
	$listFilters[$cMetadata['navigation']['softDeleteField']] = [ '$ne' => true ];
}

$options = [];
if(isset($cMetadata['navigation']['sort'])){
	$options['sort']=$cMetadata['navigation']['sort'];
}

if(isset($cMetadata['navigation']['onFilter'])){
	MongoCMS::checkCallbackFunction($cMetadata,'onFilter');
	MongoCMS::executeCallbackFunction($cMetadata,'onFilter',[&$listFilters, &$options]);
}

function getResultsCount($listFilters){
	global $cName, $cMetadata;
	if(!empty($cMetadata['navigation']['computedBy'])) {
		throw new Exception("Computed listings cannot be displayed using AJAX", 1);
	}
	return MongoCMS::getCollection($cName)->count($listFilters);
}

function getResults($listFilters,$options){
	global $cName, $cMetadata;
	if(!empty($cMetadata['navigation']['computedBy'])) {
		$cMetadata['navigation']['disableDelete']=true;
		$cMetadata['navigation']['disableCreate']=true;
		$cMetadata['navigation']['disableView']=true;
		$cMetadata['navigation']['hideActions']=true;
		MongoCMS::checkCallbackFunction($cMetadata,'computedBy');
		return MongoCMS::executeCallbackFunction($cMetadata,'computedBy',[&$listFilters, &$options]);
	}
	return MongoCMS::getCollection($cName)->find($listFilters,$options);
}


if(!empty($_GET['mode']) && $_GET['mode']=='ajax'){
	$llff = $cMetadata['ui']['listingFields'];

	$options['limit'] = (int)$_GET['length'];
	$options['skip'] = (int)$_GET['start'];
	if($options['limit']>1000) $options['limit'] = 1000;
	$sortingColumn = (int)$_GET['order'][0]['column'];
	$sortBy = null;
	foreach($llff as $lf) {
		if(empty($lf) || empty($lf['fields'])) continue;
		if($sortingColumn == 0) {
			$sortBy = $lf;
			break;
		}
		$sortingColumn--;
	}

	foreach( $sortBy['fields'] as $fieldName => $modifiers ){
		$options['sort'][ $fieldName ] = ($_GET['order'][0]['dir']=='desc')?-1:1;
	}

	

	if(strlen($_GET['search']['value'])>1){

		if(!empty($cMetadata['ui']['listingOptions']['dataTables']['ajax']['searchFields'])){
			$regexParts = explode(' ', trim($_GET['search']['value']));
			$regex = '';
			foreach ($regexParts as $k => $regexPart) {
				if(empty($regexPart)) unset($regexParts[$k]);
				else $regexParts[$k] = preg_quote($regexPart);
			}
			if(count($regexParts)>0){
				$regex = '('.implode('|', $regexParts).')';
				$listFilters['$or'] = [];
				foreach($cMetadata['ui']['listingOptions']['dataTables']['ajax']['searchFields'] as $searchField) {
					$listFilters['$or'][] = [ $searchField => [ '$regex' => $regex, '$options' => 'i' ] ];
				}
			}
		} else {
			$textFilter = [ '$search' => $_GET['search']['value'] ];
			if(!MongoCMS\validMongoId($_GET['search']['value'])){
				$listFilters['$text'] = $textFilter;
			}else{
				$listFilters['$or'] = [
					[ '$text' => $textFilter ],
					[ '_id' => MongoID($_GET['search']['value']) ]
				];
			}
			$score = [ '$meta' => 'textScore' ];
			$options['projection']['score'] = $score;
			$options['sort']['score'] = $score;
		}
	}

	$list = getResults($listFilters,$options);
	$cant =  getResultsCount($listFilters);

	$res = [];
	$res['_']=$_GET['_'];
	$res['recordsTotal']=$cant;
	$res['recordsFiltered']=$cant;
	$res['data']=[];

	foreach($list as $key => $element){
		$item = [];
		$elementDescription = MongoCMS::getDescriptiveText($element,$cName);
		foreach($llff as $lf) {
			if(empty($lf) || empty($lf['fields'])) continue;
			$data = MongoCMS::getTextFromDescriptionFields($element,$lf['fields'],$cName, $key);
			if(!empty($lf['options']['setPosition'])){
				$data = '<span style="cursor:pointer" onclick="changeOrder(\''.$element['_id'].'\',\''.$cName.'\','.$element['order'].',\''.$elementDescription.'\');">'.$data.'</span>';
			}
			if(!empty($lf['options']['positionArrows'])){
				$data.='<br/>
				<a href="order?cn='.$cName.'&amp;id='.$element['_id'].'&amp;dec">↑</a>
				<a href="order?cn='.$cName.'&amp;id='.$element['_id'].'&amp;inc">↓</a>';
			}
			$item[] = $data;
		}
		$item['cols'] = count($item);
		$item['_id'] = (string)$element['_id'];
		$item['description'] = $elementDescription;
		$item['buttons'] = MongoCMS\getCustomActionButtons($element, $cMetadata, $cName, $elementDescription);
 		$res['data'][] = $item;
	}

	echo json_encode($res);
	exit;
}

if($exportMode == 'csv'){
	ini_set('memory_limit','4G');
	ini_set('max_execution_time',120);
	require_once '../commons/data2csv.php';
	$exportData = [];
	$list = getResults($listFilters,$options);

	if(!empty($cMetadata['ui']['exportingFields'])){
		$exportMetadata = 'exportingFields';
	}else{
		$exportMetadata = 'listingFields';
	}

	foreach ($list as $key => $element ) {
		$row = [];
		foreach($cMetadata['ui'][$exportMetadata] as $label=>$lField){
			if(empty($lField)) continue;
			$row[utf8_decode($label)] = MongoCMS::getTextFromDescriptionFields($element,$lField['fields'],$cName, $key);
		}
		$exportData[] = $row;
	}
	
	header('Content-Disposition: download; filename="'.$cName.'.csv"');
	Denko::arrayUtf8Decode($exportData);
	dataToCsv($exportData,false,true);
	exit;
}
else if($exportMode == 'xls'){
	ini_set('memory_limit','3G');
	ini_set('max_execution_time',120);
	require_once '../commons/data2xls.php';
	$exportData = [];
	$list = getResults($listFilters,$options);

	if(!empty($cMetadata['ui']['exportingFields'])){
		$exportMetadata = 'exportingFields';
	}else{
		$exportMetadata = 'listingFields';
	}

	foreach ($list as $key => $element ) {
		$row = [];
		foreach($cMetadata['ui'][$exportMetadata] as $label=>$lField){
			if(empty($lField)) continue;
			$row[utf8_decode($label)] = MongoCMS::getTextFromDescriptionFields($element,$lField['fields'],$cName, $key);
		}
		$exportData[] = $row;
	}

	header('Content-Disposition: download; filename="'.$cName.'.xls"');
	Denko::arrayUtf8Decode($exportData);
	dataToXls($exportData,false,true);
	exit;
}

$smarty = MongoCMS::newSmarty();

if(!$usingAjax || (!empty($_GET['export']) && $_GET['export']=='print')){
	$list = getResults($listFilters,$options);
	$smarty->assign('elemArr', $list);
}

$smarty->assign('m', $cMetadata);
$smarty->assign('cName', $cName);
$smarty->display($tpl);
