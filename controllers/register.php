<?php
require_once 'common.php';

Denko::noCache();
$ipConfigs = MongoCMS\getConfig('ip_access_filter');
if(!empty($ipConfigs)) Denko::accessFilter($ipConfigs);


if(!defined('ALLOW_USER_REGISTRATION') || ALLOW_USER_REGISTRATION == false ){
	throw new Exception(_t('User registration is not enabled. Please define ALLOW_USER_REGISTRATION to enable this feature.'), 1);
}

Denko::sessionStart();

$smarty = MongoCMS::newSmarty();

if(!empty($_POST['email'])){
	$_POST['email'] = mb_strtolower($_POST['email']);
	$user = $mongoDB->users->findOne(['email'=>$_POST['email']]);
	if(!empty($user)){
		$smarty->assign('error',_t('This user already exists...'));
	}elseif(empty($_POST['password1']) || empty($_POST['password2']) || $_POST['password1']!=$_POST['password2'] || strlen($_POST['password1'])<6){
		$smarty->assign('error',_t('Your password should have at least 6 characters...'));
	}elseif(!MongoCMS\recaptchaCheck()){
		$smarty->assign('error',_t('Please confirm your\'re not a robot...'));
	}elseif(!MongoCMS\validEmailCheck($_POST['email'])){
		$smarty->assign('error',_t('Please enter a valid email address...'));
	}else{
		$activationToken = time().':'.sprintf("%08x", rand());
		$user = [
			'email' => $_POST['email'],
			'name' => !empty($_POST['name'])?$_POST['name']:$_POST['email'],
			'pendingPassword' => password_hash($_POST['password1'], PASSWORD_BCRYPT),
			'role' => USER_REGISTRATION_PROFILE,
			'activationToken' => $activationToken
		];
		if(defined('ON_USER_REGISTER') && !is_callable(ON_USER_REGISTER)) {
			throw new Exception(_t('%s function %s doesn\'t exists','ON_USER_REGISTER',ON_USER_REGISTER, 1));
		}
		if(defined('ON_USER_REGISTERED') && !is_callable(ON_USER_REGISTERED)) {
			throw new Exception(_t('%s function %s doesn\'t exists','ON_USER_REGISTERED',ON_USER_REGISTERED, 1));
		}
		if(defined('ON_USER_REGISTER')){
			call_user_func_array(ON_USER_REGISTER,[&$user]);
		}
		$res = $mongoDB->users->insertOne($user);
		if(defined('ON_USER_REGISTERED')) {
			call_user_func_array(ON_USER_REGISTERED,[&$user, &$res]);
		}
		$user = $mongoDB->users->findOne(['email'=>$_POST['email']]);
		$smarty->assign('user',$user);
		$smarty->assign('activationURL', Denko::getBaseHREF().'register?at='.$activationToken);
		$body = $smarty->fetch('emails/register.tpl');
		$subject = APP_NAME.' - '._t('Email verification');
		require_once(__DIR__.'/../../commons/mailer.php');
		sendEMail($user['email'],$subject,$body);
		Denko::redirect('register?mailSent');
		exit;
	}
}

if(!empty($_GET['at'])) {
	$user = $mongoDB->users->findOne(['activationToken'=>$_GET['at']]);
	if(empty($user)) {
		Denko::redirect('.');
	}else{
		$mongoDB->users->updateOne(['_id'=>$user['_id']], [ '$set' => [
			'activationToken' => null,
			'password' => $user['pendingPassword'],
			'pendingPassword' => null
		]]);
		$_SESSION[SESSION_VAR] = $user;
		Denko::redirect('.');
		exit;
	}
}

MongoCMS\checkNotLogged();

$smarty->display('register.tpl');