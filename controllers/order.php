<?php
require_once 'common.php';

MongoCMS\checkLogged();

///////////////////////////////////////////////////////////////////////////////

if(empty($_GET['id'])){
    echo "fid"; exit;
}

if(empty($_GET['cn'])){
	echo "fcn"; exit;
}
$cName = $_GET['cn'];

$cMetadata = MongoCMS::getCollectionMetadata($cName);

if(!isset($cMetadata['hasPosition']) || $cMetadata['hasPosition'] == false ){
	throw new Exception(_t('The collection %s doesn\'t has ordering enabled. Check hasPosition on your metadata file.', $cName), 1);
}

$id = $_GET['id'];
$collection = $mongoDB->$cName;

///////////////////////////////////////////////////////////////////////////////

function setPos($collection, $pos, $newPos, $id, $maxPos, $min){
	if(!is_numeric($newPos) || $newPos==$pos) return false;
	if(empty($pos)) $maxPos++;
	if($newPos < $min || $newPos > $maxPos ) return false;

	if(!empty($pos) || $pos===0){
		if($newPos > $pos){
			$res = $collection->updateMany(
				['order' => ['$gte' => $pos+1,'$lte'=>$newPos]],
				['$inc' => ['order'=> -1]]);
		}else{
			$res = $collection->updateMany(
				['order' => ['$gte' => $newPos,'$lte'=>$pos-1]],
				['$inc' => ['order'=> +1]]);
		}
		if(!$res) return $res;
	}
	$res = $collection->updateOne(
		['_id'=>MongoCMS::ID($id)],
		['$set'=>['order'=>$newPos]]);
	return $res;
}

///////////////////////////////////////////////////////////////////////////////

$c = MongoCMS::getByID($collection,$id);
$pos = isset($c['order'])?$c['order']:null;
$max = $collection->findOne([],['sort'=>['order'=>-1],'limit'=>1]);
if(empty($max)) $max=['order'=>0];
$min = MongoCMS::getMinPosition($cName);

$newPos = 0;

if(isset($_GET['inc'])) $newPos = $pos + 1;
if(isset($_GET['dec'])) $newPos = $pos - 1;
if(isset($_GET['set'])) $newPos = $_GET['set'] * 1;

$res = setPos($collection, $pos, $newPos, $id, $max['order'], $min);

if(!$res && $res!==false){
	echo "ERROR! ";
	Denko::print_r($mongoDB->lastError());
	exit;
}

Denko::redirect(MongoCMS::getListingURL($cName));
