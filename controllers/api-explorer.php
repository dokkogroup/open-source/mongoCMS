<?php
require_once 'common.php';
MongoCMS\checkLogged();

$file = __DIR__.'/../../ws/SAMPLES.txt';
$lines = explode("\n",file_get_contents($file));

$endpoints = [];
foreach($lines as $line){
	$line = trim($line);
	if(empty($line)) continue;
	$parts = explode(' #', $line, 2);
	$url = trim($parts[0]);
	if(count($parts)==2){
		$comment = trim($parts[1]);
	}else{
		$comment = '';
	}

	$params = false;
	$json = false;
	$parts = explode('?json=', $url, 2);
	if(count($parts)==2){
		$endpoint = $parts[0];
		$json=$parts[1];
		$url=$endpoint.'?json='.urlencode($json);
	}else{
		$json=false;
		$parts = explode('/', $url, 3);
		if(count($parts)<3){
			$endpoint = $url;
		}else{
			$endpoint = '/'.$parts[1];
			$params = '/'.$parts[2];
		}
	}

	if(strtolower(substr($endpoint, 1,3))=='get'){
		$method = 'GET';
	}else{
		$method = 'POST';
	}

	$jsonDoc = [];
	if($json!==false){
		$jsonData = json_decode($json,true);
		foreach($jsonData as $field => $value){
			$type = gettype($value);
			if($type=='array'){
				$keys = array_keys($value);
				foreach($keys as $key){
					if(is_integer($key)) continue;
					$type = 'object';
					break;
				}
				$value = json_encode($value);
			}
			if($type=='double') $type='float';

			$jsonDoc[$field] = [
				'sampleValue' => $value,
				'type' => $type
			];
		}
	}

	$endpoints[]=[
		'url' => 'ws'.$url,
		'comment' => $comment,
		'method' => $method,
		'endpoint' => $endpoint,
		'sampleJson' => $json,
		'sampleParams' => $params,
		'jsonDoc' => $jsonDoc
	];
}

$smarty = MongoCMS::newSmarty();
$smarty->assign('endpoints', $endpoints);
$smarty->display('api-explorer.tpl');
