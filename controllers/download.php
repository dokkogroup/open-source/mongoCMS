<?php
define('DISABLE_SESSION', true);
require_once 'common.php';

if(empty($_GET['id'])){
	echo "fid"; exit;
}

if(empty($_GET['name'])){
	echo "fname"; exit;
}

if(empty($_GET['mode'])){
	echo "fmode"; exit;
}

$f = MongoCMS::getFile($_GET['id']);
if($f==null) {
	header('HTTP/1.0 404 Not Found', true, 404);
	exit;
}
$s = MongoCMS::getFileStream($_GET['id']);

if(!empty($f['contentType'])) header('Content-Type: '.$f['contentType']);
header('Content-Disposition: '.$_GET['mode'].'; filename="'.$_GET['name'].'"');
header('Cache-Control: max-age=2592000, public');
echo stream_get_contents($s);