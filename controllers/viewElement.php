<?php
require_once 'common.php';
MongoCMS\checkLogged();

if(empty($_GET['id'])){
	echo "fid"; exit;
}
if(empty($_GET['cn'])){
	echo "fcn"; exit;
}

$cName = $_GET['cn'];
$cMetadata = MongoCMS::getCollectionMetadata($cName);

if(!empty($cMetadata['navigation']['disableView'])) {
	throw new Exception(_t('Viewing is disabled for collection %s (check disableView in metadata file)',$cName), 1);
}

if(empty($cMetadata['ui']['viewFields'])){
	if(empty($cMetadata['ui']['fields'])) {
		throw new Exception(_t('Can\'t display element on collection %s. There\'s no ui.viewFields or ui.fields definition on metadata file',$cName));
	}
	$cMetadata['ui']['viewFields'] = $cMetadata['ui']['fields'];
}

$smarty = MongoCMS::newSmarty();
$smarty->assign('element', MongoCMS::getByID($cName,$_GET['id'],false));
$smarty->assign('m',$cMetadata);
$smarty->assign('cName', $cName);
$smarty->assign('action', 'View');
$smarty->assign('returnLocation', MongoCMS::getListingURL($cName));
$smarty->display('viewElement.tpl');
