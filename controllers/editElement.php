<?php
if(isset($_GET['action']) && ( $_GET['action'] == 'uploadFile' || $_GET['action']=='delete' || $_GET['action']=='restore')) define('isAjaxRequest',true);
if(!defined('isAjaxRequest') && count($_POST)>0) define('isAjaxRequest',true);

require_once 'common.php';

MongoCMS\checkLogged();

////////////////////////////////////////////////////////////////////////////////////////////
if(isset($_GET['action']) && $_GET['action'] == 'uploadFile'){
    if(!isset($_POST['file_id'])) { header("Error uploading file.", true, 500); exit; }
    if(empty($_FILES)) { header("Error uploading file.", true, 500); exit; }
    $file = array_pop($_FILES);
    if(!isset($file['error'][0])) { header("Error uploading file.", true, 500); exit; }
    if($file['error'][0]!==0) { header("Error uploading file (file uploaded width errors)", true, 500); exit; }
    $id = MongoCMS::saveFile($file['tmp_name'][0], $file['name'][0], $file['type'][0]);
    echo json_encode(['id'=>(string)$id]);
    exit;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

if(empty($_GET['id'])) throw new Exception(_t('Missing Element ID'), 1);
if(empty($_GET['cn'])) throw new Exception(_t('Missing Collection Name'), 1);

$cName = $_GET['cn'];
$cMetadata = MongoCMS::getCollectionMetadata($cName);

MongoCMS::checkDelete($cName);
MongoCMS::checkRestore($cName);
MongoCMS::checkInsertUpdate($cName);

if($_GET['id']!='new' && !empty($cMetadata['navigation']['disableEdit'])) {
	throw new Exception(_t('Editing is disabled for collection %s (check disableEdit in metadata file)',$cName), 1);
}elseif($_GET['id']=='new' && !empty($cMetadata['navigation']['disableCreate'])){
	throw new Exception(_t('Creation has been disabled for %s (check disableCreate in metadata file)',$collection), 1);
}

$smarty = MongoCMS::newSmarty();
$smarty->assign('element', MongoCMS::getByID($cName,$_GET['id'],false));
$smarty->assign('m',$cMetadata);
$smarty->assign('cName', $cName);
$smarty->assign('action', $_GET['id']=='new'?'New':'Edit');
$smarty->assign('returnLocation', MongoCMS::getListingURL($cName));
$smarty->display('editElement.tpl');
