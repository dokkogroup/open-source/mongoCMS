<?php
namespace MongoCMS;

if(defined('SESSION_DURATION')) {
	$maxSessionLifetime = max(SESSION_DURATION)+360;
	ini_set("session.cookie_lifetime",$maxSessionLifetime);
	ini_set("session.gc_maxlifetime",$maxSessionLifetime);
	session_set_cookie_params($maxSessionLifetime);
	session_save_path(__DIR__.'/../../session-data/');
}

if(defined('isAjaxRequest')) header('Content-Type: application/json');

///////////////////////////////////////////////////////////////////////////////

function handleException($ex) {
	if(defined('EXCEPTION_HANDLED')) return;

	if( PHP_SAPI === 'cli' ) echo "\n";
	error_log("Uncaught exception class=" . get_class($ex) . " message=" . $ex->getMessage() . " line=" . $ex->getLine());

	if (ob_get_level()) ob_end_clean(); # try to purge content sent so far
	$msg = $ex->getMessage();
	$exType = 'UNKNOWN_TYPE';
	if( PHP_SAPI === 'cli' ) { // if we're running from command-line
		$exType = 'CONSOLE_EXCEPTION';
		if(getenv('MONGOCMS_DEBUG')=='true'){
			print_r(['message'=>$ex->getMessage(), 'file'=>$ex->getFile(), 'line'=>$ex->getLine(), 'code'=>$ex->getCode(), 'trace'=>$ex->getTrace()]);
		}else{
			echo "\n -- You can set/export env variable MONGOCMS_DEBUG=true to get more info on the command line --";
		}
		if(!defined('ENABLE_EVENT_LOGS') || ENABLE_EVENT_LOGS==false){
			echo "\n -- You can enable mongoCMS's event logs by calling define(\"ENABLE_EVENT_LOGS\", true); on your PHP script --";
		}else{
			echo "\n -- You can check mongoCMS's event logs for more info --";
		}
		echo "\n";
	} elseif(defined('isAjaxRequest')) { // if we're running as ajax request
		$exType = 'AJAX_EXCEPTION';
		$exClass = get_class($ex);
		if(substr($exClass,0,7) == 'MongoDB' && substr($msg,0,6) == 'E11000' ){
			$aux = explode('index: ', $msg,2);
			$aux = explode(' dup key: ',$aux[1],2);
			$msg = _t('Can\'t save changes because the data <b>%s</b> is duplicated for index <b>"%s"</b>', substr($aux[1],3,-2), $aux[0]);
		}
		echo json_encode(['status'=>'error','message'=>$msg]);
	} elseif(class_exists('\MongoCMS')) { // if we're running as standard web request
		$exType = 'FATAL_EXCEPTION';
		header('HTTP/1.1 500 Internal Server Error');
		$smarty = \MongoCMS::newSmarty();
		$smarty->assign('ex',$ex);
		$smarty->assign('showTrace',\Denko::getHost()=='localhost');
		$errorTPL = $smarty->createTemplate('error.tpl');
		$smarty->display('eval:'.file_get_contents($errorTPL->source->filepath));
	}else{
		header('HTTP/1.1 500 Internal Server Error');
		echo "Exception: ".$ex->getMessage()."\n";
		echo "File: ".$ex->getFile()." / Line: ".$ex->getLine()."\n\n";
		if(\Denko::getHost()=='localhost'){
			echo "Stack Trace:\n";
			foreach($ex->getTrace() as $number => $trace){
				echo $number.")\n";
				foreach($trace as $k => $v){
					echo '  '.$k.': ';
					if(is_array($v)) $v = implode(' | ', $v);
					print_r($v);
					echo "\n";
				}
			}
		}
	}
	define('EXCEPTION_HANDLED',true);
	if($ex->getCode()!==0){
		try{
			@logEvent($exType,$msg,['message'=>$ex->getMessage(), 'file'=>$ex->getFile(), 'line'=>$ex->getLine(), 'code'=>$ex->getCode(), 'trace'=>$ex->getTrace()]);
		}catch(Exception $e){}
	}
}

///////////////////////////////////////////////////////////////////////////////

function shoutDownfunction(){
	$a = error_get_last();
	if(empty($a)) return;
	if($a['type']==E_NOTICE) return;
	if($a['type']==E_WARNING) return;
	if($a['type']==E_DEPRECATED) return;
	handleException(new \Exception($a['message'], 1));
}

///////////////////////////////////////////////////////////////////////////////

register_shutdown_function('MongoCMS\shoutDownfunction');
set_exception_handler('MongoCMS\handleException');

require_once __DIR__.'/../../denko/L10N.php';

///////////////////////////////////////////////////////////////////////////////

function checkLogged($roles = null){
	if(defined('SESSION_DURATION')) {
		\Denko::sessionStart();
		if(!empty($_SESSION[SESSION_VAR]['role'])){
			$lastLoginCheck = time();
			if(!empty($_SESSION[SESSION_VAR.':lastLoginCheck'])){
				$lastLoginCheck = $_SESSION[SESSION_VAR.':lastLoginCheck'];
			}
			$myRoles = $_SESSION[SESSION_VAR]['role'];
			if(!is_array($myRoles)) $myRoles = [ $myRoles ];
			$duration = 0;
			foreach($myRoles as $role){
				if(!array_key_exists($role, SESSION_DURATION)) continue;
				if(SESSION_DURATION[$role]>$duration) $duration = SESSION_DURATION[$role];
			}
			if($duration==0) $duration = SESSION_DURATION['default'];
			if($lastLoginCheck + $duration < time()){
				\Denko::sessionDestroy();
			}
		}
	}
	\Denko::sessionStart();
	$_SESSION[SESSION_VAR.':lastLoginCheck'] = time();
	if(empty($_SESSION[SESSION_VAR.':lastSessionRenew'])) {
		$_SESSION[SESSION_VAR.':lastSessionRenew'] = time();
	}
	if(time() - $_SESSION[SESSION_VAR.':lastSessionRenew'] > 300) {
		session_regenerate_id(true);
		session_write_close();
		session_start();
		$_SESSION[SESSION_VAR.':lastSessionRenew'] = time();
	}
	if(empty($_SESSION[SESSION_VAR])) {
		if(! defined('isAjaxRequest')) {
			if(empty($_SERVER['REQUEST_URI'])) Denko::redirect('login');
			$uri = $_SERVER['REQUEST_URI'];
			$aux = explode('/list-', $uri, 2);
			if(count($aux)==2) \Denko::redirect('login?r=./list-'.urlencode($aux[1]));
			$aux = explode('/edit-', $uri, 2);
			if(count($aux)==2) \Denko::redirect('login?r=./edit-'.urlencode($aux[1]));
			$aux = explode('/view-', $uri, 2);
			if(count($aux)==2) \Denko::redirect('login?r=./view-'.urlencode($aux[1]));
			if(substr($uri, -8) == '/profile') \Denko::redirect('login?r=./profile');
			if(substr($uri, -13) == '/api-explorer') \Denko::redirect('login?r=./api-explorer');
			\Denko::redirect('login');
		}
		throw new \Exception(\_t('Your session has expired. Please re-login.'), 0);
	}
	if($roles==null) return;
	if(!is_array($roles)) $roles = [ $roles ];
	$myRoles = $_SESSION[SESSION_VAR]['role'];
	if(is_string($myRoles)) $myRoles = [ $myRoles ];
	foreach($myRoles as $role){
		if(empty($role)) continue;
		if(in_array($role,$roles)) return;
	}
	throw new \Exception(\_t('You can\'t perform this action.'), 1);
}

///////////////////////////////////////////////////////////////////////////////

function checkNotLogged(){
	\Denko::sessionStart();
	if(!empty($_SESSION[SESSION_VAR])) \Denko::redirect('.');
}

///////////////////////////////////////////////////////////////////////////////

function getConfig($name, $returnFullObject=false){
	global $mongoDB, $configurationsCollection;
	if(!isset($configurationsCollection)) $configurationsCollection = $mongoDB->configurations;
	$object = $configurationsCollection->findOne(['name'=>$name]);
	if($object == null) throw new \Exception(_t('Error: Configuration "%s" is not defined.',$name), 1);
	if($returnFullObject) return $object;
	return $object['value'];
}

///////////////////////////////////////////////////////////////////////////////

function setConfig($name, $value){
	global $mongoDB, $configurationsCollection;
	$config = getConfig($name,true);
	$configurationsCollection->updateOne(['_id'=>$config['_id']],['$set' => ['value' => $value]]);
	return true;
}

///////////////////////////////////////////////////////////////////////////////

function recaptchaCheck() {
	if(!defined('RECAPTCHA_KEY')) return true;
	if(!defined('RECAPTCHA_SECRET')) {
		throw new \Exception(_t("Recaptcha secret not defined. Please define RECAPTCHA_SECRET to validate recaptcha"), 1);
	}
	# Verify captcha
	$post_data = http_build_query([
		'secret' => RECAPTCHA_SECRET,
		'response' => $_POST['g-recaptcha-response'],
		'remoteip' => $_SERVER['REMOTE_ADDR']
	]);
	$opts = ['http' => [
		'method'  => 'POST',
		'header'  => 'Content-type: application/x-www-form-urlencoded',
		'content' => $post_data
	]];

	$context  = stream_context_create($opts);
	$response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
	@$result = json_decode($response);
	if (empty($result) || empty($result->success)) return false;
	return true;
}

///////////////////////////////////////////////////////////////////////////////

function validEmailCheck($email) {
	if(!\Denko::hasEmailFormat($email)) return false;

	if(!defined('EMAIL_VALIDATION_URL')) return true;
	$url = sprintf(EMAIL_VALIDATION_URL,$email);
	@$json = file_get_contents($url);
	$res = null;
	@$res = json_decode($json,true);

	// IF CONNECTION FAILED
	if(empty($res)) {
		logEvent('EMAIL_VALIDATION','Got empty response or invalid json from email validation service',['json_last_error()'=>json_last_error()]);
		if(!FORCE_EMAIL_VALIDATION) return true;
		throw new Exception(_t('Can\'t connect to email validation service. Please try again later.'), 1);
	}

	// IF API KEY DEPLETED (NEED TO BUY MORE CHECKS)
	if($res['status']==119){
		logEvent('EMAIL_VALIDATION','Email validation account depleted (YOU NEED TO BUY MORE CHECKS)',$res);
		if(!FORCE_EMAIL_VALIDATION) return true;
		throw new Exception(_t('Can\'t connect to email validation service. Please try again later.'), 1);
	}

	if($res['status']==200) return true;
	logEvent('EMAIL_VALIDATION','Rejected '.$email.'. '.$res['info'],$res);
	return false;
}

///////////////////////////////////////////////////////////////////////////////

function logEvent($type, $description, $data){
	if(!defined('ENABLE_EVENT_LOGS') || ENABLE_EVENT_LOGS==false) return;
	global $mongoDB;
	$logData = [
		'type' => $type,
		'description' => $description,
		'data' => $data,
		'aud_ins_date' => \Denko::curTimestamp(),
		'sessionData' => null,
		'serverData' => null,
		'notes' => null
	];

	if( PHP_SAPI !== 'cli' ) { // if we're not running on command line
		if(empty($_SESSION)){
			$sessionData == [];
		} else {
			$sessionData = $_SESSION;
			if(isset($sessionData[SESSION_VAR]['password'])){
				unset($sessionData[SESSION_VAR]['password']);
			}
		}
		$serverData = [
			'requestURI' => $_SERVER['REQUEST_URI'],
			'httpReferer' => $_SERVER['HTTP_REFERER'],
			'requestScheme' => $_SERVER['REQUEST_SCHEME'],
			'host' => \Denko::getHost(),
			'clientIP' => \Denko::getIP()
		];
		$logData['sessionData'] = $sessionData;
		$logData['serverData'] = $serverData; 
	}

	@json_encode($logData);
	if(json_last_error()==5) {
		$logData['notes'] = 'MongoCMS Notice: Some data on this log has been automatically encoded to UTF8 to allow storage on MongoDB.';
		\Denko::arrayUtf8SafeEncode($logData);
	}
	$mongoDB->mCMSEventLogs->insertOne($logData);
}

///////////////////////////////////////////////////////////////////////////////

function beginPasswordRecovery($user, $isActivation){
	global $mongoDB;
	if($user==null) throw new \Exception(_t('Couldn\'t launch password recovery process.'), 1);

	$recoverToken = time().':'.sprintf("%08x", rand());
	$mongoDB->users->updateOne( [ '_id' => $user['_id'] ], [ '$set' => [
		'recoverToken' => $recoverToken,
		'activationToken' => null,
		'pendingPassword' => null
	] ] );

	$smarty = \MongoCMS::newSmarty();
	$smarty->assign('user',$user);
	$smarty->assign('recoveryURL', \Denko::getBaseHREF().'login?rt='.$recoverToken);

	if(!$isActivation){
		$subject = APP_NAME.' - '._t('Password Recover');
		$body = $smarty->fetch('emails/password-recover.tpl');
	} else {
		$subject = APP_NAME.' - '._t('Account Activation');
		$body = $smarty->fetch('emails/account-activation.tpl');
	}

	require_once(__DIR__.'/../../commons/mailer.php');
	\sendEMail($user['email'],$subject,$body);
}

///////////////////////////////////////////////////////////////////////////////

function getCustomActionButtons($element, $cMetadata, $cName, $elementDescription){
	$buttons = [];
	$customActions = [];
	if(!empty($cMetadata['navigation']['customActions'])){
		$customActions = $cMetadata['navigation']['customActions'];
	}
	foreach($customActions as $action=>$button){
		if($button === null) continue;
		$icon = '';
		if(!empty($button['icon'])){
			if(strpos($button['icon'], 'glyphicon')!==false){
				$icon = '<span class="glyphicon '.$button['icon'].'"></span>';
			}elseif(strpos($button['icon'], '.png')!==false){
				$icon = '<img class="image-icon" src="./img/'.$button['icon'].'" />';
			}else{
				$icon = '<span class="material-icons">'.$button['icon'].'</span>';
			}
		}
		if(!empty($button['onDisplay'])){
			if(!call_user_func_array($button['onDisplay'],[$element, $action])) continue;
		}
		$btnURL = '';
		if(!isset($button['url']) || is_string($button['url'])){
			$btnURL = isset($button['url'])?$button['url']:'#';
			$auxQP = [];
			if(!empty($button['qsa'])) {
				foreach($button['qsa'] as $qp) {
					if(isset($_GET[$qp])) $auxQP[$qp] = $_GET[$qp];
				}
			}
			$auxQP['action']=$action;
			$auxQP['id']=(string)$element['_id'];
			$auxQP['collection']=$cName;
			if(!empty($button['parameters'])){
				foreach($button['parameters'] as $pName){
					$auxQP[$pName] = (string)$element[$pName];
				}
			}
			$qsGlue = '?';
			foreach($auxQP as $k=>$v) {
				$btnURL.=$qsGlue.$k.'='.urlencode($v);
				$qsGlue = '&amp;';
			}
		}else{
			foreach($button['url'] as $fieldName=>$modifiers){
				$btnURL .= \MongoCMS::applyModifiers($element[$fieldName],$modifiers,$element);
			}
		}
		$onClick = 'return true;';
		if(!empty($button['confirmText'])){
			$onClick='return confirm(`'.str_replace('`','\\`',$button['confirmText']).'`);';
		}elseif (isset($button['onClick'])) {
			$onClick = $button['onClick'];
		}

		if(!isset($button['label']) && !empty($icon)) $button['label'] = '';

		$btnTitle = (isset($button['title'])?$button['title']:'');
		$btnTarget = (isset($button['target'])?$button['target']:'');
		$btnLabel = (isset($button['label'])?$button['label']:$action);
		$btnLabel = mb_convert_case($btnLabel, MB_CASE_TITLE, 'UTF-8');
		$customBtn = '<a class="btn btn-sm '.(isset($button['class'])?$button['class']:'').'" href="'.$btnURL.'" target="'.$btnTarget.'" onClick="'.$onClick.'" title="'.$btnTitle.'">'.$icon.' '.$btnLabel.'</a>';
		$buttons[]=$customBtn;
	}

	if(\MongoCMS::displayViewBtn($cMetadata, $element)){
		$buttons[] = '<a class="btn btn-sm btn-warning" href="./'.\MongoCMS::getViewURL($cName,$element['_id']).'" title="'._t('View').'"><span class="glyphicon glyphicon-eye-open"></span></a>';
	}

	if(\MongoCMS::displayEditBtn($cMetadata, $element)){
		$buttons[] = '<a class="btn btn-sm btn-info" href="./'.\MongoCMS::getEditionURL($cName,$element['_id']).'" title="'._t('Edit').'"><span class="glyphicon glyphicon-pencil"></span></a>';
	}

	if(\MongoCMS::displayDeleteBtn($cMetadata, $element)){
		$buttons[] = '<button type="button" class="btn btn-sm btn-danger" onClick="deleteElement(\''.$element['_id'].'\',\''.$elementDescription.'\');" title="'._t('Delete').'"><span class="glyphicon glyphicon-trash"></span></button>';
	}

	if(\MongoCMS::displayRestoreBtn($cMetadata, $element)){
		$buttons[] = '<button type="button" class="btn btn-sm btn-default" onClick="restoreElement(\''.$element['_id'].'\',\''.$elementDescription.'\');" title="'._t('Restore').'"><span class="material-icons" style="width:15px;margin-top:-2px;margin-left:-2px;margin-right:1px;font-size:1.6em">restore_page</span></button>';
	}

	return $buttons;
}

///////////////////////////////////////////////////////////////////////////////

function validMongoId($string){
	return preg_match('/^[a-f\d]{24}$/i', $string);
}