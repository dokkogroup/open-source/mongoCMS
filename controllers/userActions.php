<?php
define('isAjaxRequest',true);
require_once 'common.php';

if(!defined('ROLES_THAT_CAN_FORCE_PASSWORD_RECOVERY')) {
	$ROLES_THAT_CAN_FORCE_PASSWORD_RECOVERY = ['admin'];
}else{
	$ROLES_THAT_CAN_FORCE_PASSWORD_RECOVERY = ROLES_THAT_CAN_FORCE_PASSWORD_RECOVERY;
}
if(!defined('ROLES_THAT_CAN_SWITCH_USER')){
	$ROLES_THAT_CAN_SWITCH_USER = ['admin'];
}else{
	$ROLES_THAT_CAN_SWITCH_USER = ROLES_THAT_CAN_SWITCH_USER;
}

if ( empty($_GET['id']) || empty($_GET['collection']) || empty($_GET['action']) ){
	throw new Exception("Missing parameters", 1);
}

if ( !in_array($_GET['action'],['passwordRecovery','switchUser']) ) {
	throw new Exception("Invalid action", 1);
}

MongoCMS\checkLogged();
$u = MongoCMS::getByID('users', $_GET['id']);

if($u == null) throw new Exception("Can't find user");

switch ($_GET['action']) {
	case 'passwordRecovery':
		MongoCMS\checkLogged($ROLES_THAT_CAN_FORCE_PASSWORD_RECOVERY);
		MongoCMS\beginPasswordRecovery($u,true);
		echo json_encode(['status'=>'ok','redirect'=>null]);
		break;
	case 'switchUser':
		MongoCMS\checkLogged($ROLES_THAT_CAN_SWITCH_USER);
		if(!defined('ALLOW_SWITCH_USER')) throw new Exception("Can't switch user. Please enable ALLOW_SWITCH_USER to allow this action.", 1);
		$_SESSION[SESSION_VAR] = $u;
		if(defined('ON_USER_LOGGED_IN')){
			if(!is_callable(ON_USER_LOGGED_IN)) throw new Exception(_t('%s function %s doesn\'t exists','ON_USER_REGISTERED',ON_USER_LOGGED_IN, 1));
			call_user_func_array(ON_USER_LOGGED_IN,[&$u]);
		}
		echo json_encode(['status'=>'ok','redirect'=>'.']);
		break;
	default:
		# code...
		break;
}
