<?php
require_once 'common.php';

Denko::noCache();
try{
	$ipConfigs = MongoCMS\getConfig('ip_access_filter');
}catch(Exception $e){
	$ipConfigs = null;
}

if(!empty($ipConfigs)) Denko::accessFilter($ipConfigs);

Denko::sessionStart();

if(isset($_GET['logout'])){
	if(defined('FORCE_DOKKO_LOGIN') && FORCE_DOKKO_LOGIN) Denko::redirect('dokko-login?logout');
	$_SESSION[SESSION_VAR] = null;
	Denko::redirect('login');
}

MongoCMS\checkNotLogged();

if(defined('FORCE_DOKKO_LOGIN') && FORCE_DOKKO_LOGIN) {
	$_SESSION[SESSION_VAR.':after-dokko-login-redirect'] = empty($_GET['r'])?'.':$_GET['r'];
	Denko::redirect('dokko-login');
}

$smarty = MongoCMS::newSmarty();

if(!empty($_POST['email'])){
	$_POST['email'] = mb_strtolower($_POST['email']);
	// Valido, si no hay usuarios, creo al admin:
	$aux = $mongoDB->users->findOne();
	if(empty($aux)) $mongoDB->users->insertOne([
		'email'=>'fbricker@gmail.com',
		'password'=>password_hash('12345678', PASSWORD_BCRYPT),
		'name'=>'Federico Bricker',
		'role'=>'admin',
		'aud_ins_date'=>Denko::curTimestamp(),
		'aud_upd_date'=>null
	]);

	$user = $mongoDB->users->findOne(['email'=>$_POST['email']]);

	if(isset($_GET['recover'])){
		if(empty($user)) {
			$smarty->assign('error',_t('Invalid username...'));
		}elseif(!MongoCMS\recaptchaCheck()){
			$smarty->assign('error',_t('Please confirm your\'re not a robot...'));
		} else {
			MongoCMS\beginPasswordRecovery($user, false);
			Denko::redirect('login?mailSent');
			exit;
		}
	}else{
		if(!isset($_POST['password']) || !$user || !password_verify($_POST['password'],$user['password'])){
			$smarty->assign('error',_t('Invalid username or password...'));
		} else {
			$_SESSION[SESSION_VAR] = $user;
			if(defined('ON_USER_LOGGED_IN')){
				if(!is_callable(ON_USER_LOGGED_IN)) throw new Exception(_t('%s function %s doesn\'t exists','ON_USER_REGISTERED',ON_USER_LOGGED_IN, 1));
				call_user_func_array(ON_USER_LOGGED_IN,[&$user]);
			}
			if(!empty($_GET['r'])) Denko::redirect($_GET['r']);
			Denko::redirect('.');
		}
	}
}

if(!empty($_GET['rt'])) {
	$user = $mongoDB->users->findOne(['recoverToken'=>$_GET['rt']]);
	$aux = explode(':', $_GET['rt'],2);
	if(empty($user) || time()-$aux[0]<0 || time()-$aux[0]>86400){
		$smarty->assign('error',_t('This password recovery link has expired...'));
	}elseif(empty($_POST)){
		$smarty->assign('user',$user);
		$smarty->display('new-password.tpl');
		exit;
	}elseif(!empty($_POST['password1']) && !empty($_POST['password2']) && $_POST['password1']==$_POST['password2'] && strlen($_POST['password1'])>=6) {
		$mongoDB->users->updateOne(['_id'=>$user['_id']], [ '$set' => [
			'password' => password_hash($_POST['password1'], PASSWORD_BCRYPT),
			'recoverToken' => null,
			'activationToken' => null,
			'pendingPassword' => null
		]]);
		Denko::redirect('login?updated');
		exit;
	}
}

if(isset($_GET['recover'])) {
	if(!defined('ALLOW_PASSWORD_RECOVER') || ALLOW_PASSWORD_RECOVER == false ){
		throw new Exception(_t('Password recover is not enabled. Please define ALLOW_PASSWORD_RECOVER to enable this feature.'), 1);
	}
	$smarty->display('password-recover.tpl');
	exit;
}

$smarty->display('login.tpl');