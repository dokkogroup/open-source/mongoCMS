<?php
require_once 'common.php';
require_once __DIR__.'/../../commons/dkLogin.php';

if(!defined('DOKKO_LOGIN_APP_NAME')) Denko::redirect('login');
if(empty($_GET['k'])) {
	$url=explode('?',Denko::getRequestScheme()."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
	$url=urlencode($url[0]);
	if(isset($_GET['logout'])){
		$_SESSION[SESSION_VAR] = null;
		dklLoginRedirect('ask',DOKKO_LOGIN_APP_NAME.'&url='.$url);
		exit;
	}
	dklLoginRedirect('c',DOKKO_LOGIN_APP_NAME.'&url='.$url);
}

$redirectTo = null;
if(!empty($_SESSION[SESSION_VAR.':after-dokko-login-redirect'])){
	$redirectTo = $_SESSION[SESSION_VAR.':after-dokko-login-redirect'];
	unset($_SESSION[SESSION_VAR.':after-dokko-login-redirect']);
}
$_SESSION[SESSION_VAR] = null;

$login=dokkoLogin(DOKKO_LOGIN_APP_NAME);

$user = $mongoDB->users->findOne(['email'=>$login['email']]);

if($user == null){
	$user = [];
	$user['email'] = $login['email'];
	$user['password'] = 'From DokkoLogin';
	$user['name'] = $login['name'].' '.$login['lastname'];
	$user['role'] = $login['access_level'];
	$mongoDB->users->insertOne($user);
	$user = $mongoDB->users->findOne(['email'=>$login['email']]);
}else{
	$user['name'] = $login['name'].' '.$login['lastname'];
	$user['role'] = $login['access_level'];
	$mongoDB->users->updateOne(['_id'=>$user['_id']],['$set'=>$user]);
}

$_SESSION[SESSION_VAR] = $user;
if($redirectTo!=null) Denko::redirect($redirectTo);
if(empty($login['redirect']) || $login['redirect']=='#') Denko::redirect('.');
Denko::redirect($login['redirect']);
