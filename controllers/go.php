<?php
require_once 'common.php';

\Denko::sessionStart();

if(empty($_SESSION[SESSION_VAR])){
	Denko::redirect('./login?r=./go-'.$_GET['role'].'@0@'.$_GET['location']);
}

$roles = $_SESSION[SESSION_VAR]['role'];
if(!is_array($roles)) $roles = [ $roles ];

if(!in_array($_GET['role'], $roles)) {
	if($_GET['retry']=='0') Denko::redirect('./');
	$_SESSION[SESSION_VAR] = null;
	Denko::redirect('./login?r=./go-'.$_GET['role'].'@0@'.$_GET['location']);
}

Denko::redirect('./'.$_GET['location']);